<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml"
              pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties"
                              Target="docProps/custom.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:body>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblInd w:w="-301" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0"
                                       w:noHBand="0" w:noVBand="0"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="1541"/>
                            <w:gridCol w:w="376"/>
                            <w:gridCol w:w="886"/>
                            <w:gridCol w:w="486"/>
                            <w:gridCol w:w="595"/>
                            <w:gridCol w:w="360"/>
                            <w:gridCol w:w="909"/>
                            <w:gridCol w:w="656"/>
                            <w:gridCol w:w="239"/>
                            <w:gridCol w:w="601"/>
                            <w:gridCol w:w="1622"/>
                            <w:gridCol w:w="1919"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="030E7D1A" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="374"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1917" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="32891875" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Name</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> (</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>中/英文姓名)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2327" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="507C104D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="0092125A">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00124E7E">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.crewName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1804" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="428A44F8" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">Rank </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">( </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>职务</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> )</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2223" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="4D84C69E" w14:textId="2257A776" w:rsidR="00FE27F6"
                                     w:rsidRDefault="004F0FBB">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004F0FBB">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.crewPost}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="79C48425" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="70D57B43" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="374"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1917" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="5DE1F315" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">D.O.B </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">( </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>出生</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>日期</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> )</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2327" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="40376604" w14:textId="5DF5858F" w:rsidR="00FE27F6"
                                     w:rsidRDefault="004F0FBB">
                                    <w:r w:rsidRPr="007F7FDD">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${(crewInfo.birthday?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1804" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="28517BD3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>P.O.B</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> 出生地 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2223" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="5908155C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="0092125A">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.birthPlace}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge/>
                                </w:tcPr>
                                <w:p w14:paraId="26283DBE" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="4FBC5E54" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="181"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1917" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="48E69FFE" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">ID No. </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>身份证号码</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2327" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="5294A76B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="0092125A">
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.documentNumber}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1804" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="2BE78D0A" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Phone</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>家庭电话</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> )</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2223" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="6D1A71F3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6" w:rsidP="00255BEC">
                                    <w:pPr>
                                        <w:ind w:firstLineChars="200" w:firstLine="420"/>
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge/>
                                </w:tcPr>
                                <w:p w14:paraId="2A9275C2" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="5AA9A500" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="374"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1917" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="087BA7F7" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Degree</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>学历</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="0AE77458" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="6C66FDA0" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2327" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="461CE307" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="0092125A">
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.education}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1804" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="5D3E5FC7" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Mobil(手机)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2223" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="3CED4623" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="0092125A">
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:b/>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.phoneNumber}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge/>
                                </w:tcPr>
                                <w:p w14:paraId="75A87551" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="03D4B1FD" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="374"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1917" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="392BE584" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Address</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">( </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>家庭</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>地址</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6354" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                </w:tcPr>
                                <w:p w14:paraId="6410BDBC" w14:textId="00A8D37D" w:rsidR="00FE27F6"
                                     w:rsidRDefault="004F0FBB">
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.homeAddress}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge/>
                                </w:tcPr>
                                <w:p w14:paraId="714BCBC1" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="2E1560E7" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="374"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1917" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="11BCE897" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Kin/Name</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>亲属姓名</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1372" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="1733ACBE" w14:textId="4AFAF590" w:rsidR="00FE27F6"
                                     w:rsidRDefault="004F0FBB">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="004F0FBB">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.emergencyContact}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2520" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="3476BE3B" w14:textId="0132F736" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Relationship</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>关系</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="004F0FBB" w:rsidRPr="004F0FBB">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.relationship}wInfo.relationship}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2462" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="4A6CA2AB" w14:textId="595A21CD" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Tel电话：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="004F0FBB" w:rsidRPr="004F0FBB">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.relationshipPhone}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge/>
                                </w:tcPr>
                                <w:p w14:paraId="44A94DAB" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="54994BEF" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="374"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1541" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="05335836" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Weight</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>体重</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(kg)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">      </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">       </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">         </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1262" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="1DCBBE60" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:b/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1081" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="2C53298A" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>身高</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>c</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>m)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1269" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                </w:tcPr>
                                <w:p w14:paraId="0D6D639C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:b/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1496" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="38930E4C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Blood type</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>血型</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1622" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="7C983D0B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge/>
                                </w:tcPr>
                                <w:p w14:paraId="776ABB1D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="0F35E543" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="374"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2803" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="1802E82B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Education</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>毕业学校/时间/专业</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2350" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                </w:tcPr>
                                <w:p w14:paraId="42917304" w14:textId="54133EFD" w:rsidR="00FE27F6"
                                     w:rsidRDefault="004F0FBB">
                                    <w:r w:rsidRPr="008F0647">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.school}</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="007F7FDD">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${(crewInfo.graduationTime?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1496" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                </w:tcPr>
                                <w:p w14:paraId="60F63F89" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Shoe Size 鞋码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1622" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="61F057DD" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1919" w:type="dxa"/>
                                    <w:vMerge/>
                                </w:tcPr>
                                <w:p w14:paraId="5C67A51B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="21A8F6C3" w14:textId="77777777" w:rsidR="00FE27F6" w:rsidRDefault="00FE27F6">
                        <w:pPr>
                            <w:ind w:leftChars="-428" w:left="-899" w:firstLineChars="240" w:firstLine="360"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="15"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="15"/>
                            </w:rPr>
                            <w:t xml:space="preserve">CERTIFICATES </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="15"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="15"/>
                            </w:rPr>
                            <w:t>证 书 情 况</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="15"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblInd w:w="-294" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0"
                                       w:noHBand="0" w:noVBand="0"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="3762"/>
                            <w:gridCol w:w="2167"/>
                            <w:gridCol w:w="2326"/>
                            <w:gridCol w:w="1687"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="33C58A7D" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="635"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6A0F4BE3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Certificates Typ</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>e</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="67DF3461" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>证书名称</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="78E9632C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Number</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="2C8E8B71" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>证书号码</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="73883CA6" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Valid Period</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="4C8B9AE6" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>有效期</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="22C2474B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Place of Issue</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="1235C4A4" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>签发地点</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="455AFFB1" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="340"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="3C5EBF1E" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Seaman’</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>s</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> Book</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>海员证</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="19D6B658" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="5D1E1CA4" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:ind w:leftChars="-58" w:left="-1" w:rightChars="-51" w:right="-107"
                                               w:hangingChars="67" w:hanging="121"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="673DB523" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:firstLineChars="100" w:firstLine="180"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="2A12ACD8" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="340"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="4F100C6E" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Seaman’</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>s</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> Record</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Book</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>服务簿</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="35B9BDB7" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="31AFE295" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:ind w:leftChars="-58" w:left="-1" w:rightChars="-51" w:right="-107"
                                               w:hangingChars="67" w:hanging="121"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="43944472" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:firstLineChars="100" w:firstLine="180"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="2AD04848" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="340"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="297DB5BC" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Passport</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>护照</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>-</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="56B1A9EA" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="78D04DD2" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:ind w:leftChars="-58" w:left="-1" w:rightChars="-51" w:right="-107"
                                               w:hangingChars="67" w:hanging="121"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6A466CFE" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="16E9B744" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="340"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1371BD3D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Competency Certificate</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>适任证书</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="0D7580CA" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6FDA7EA0" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:leftChars="-58" w:left="-1" w:rightChars="-51" w:right="-107"
                                               w:hangingChars="67" w:hanging="121"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="2C38B7A3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="7845416F" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="340"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1F287762" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>GMDSS Certificate</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(GMDSS</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>证书</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6E78AF9C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="26F5D8AD" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:rightChars="-51" w:right="-107"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="408638FE" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="56716BE5" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="340"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="13EC4560" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>PNM/LIB/GMDSS Certificate</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>巴/利</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>G</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>证</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6A2E25B3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="07693C37" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:rightChars="-51" w:right="-107"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6B37CAF1" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="2FD84329" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="340"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3762" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="58452335" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Other License/Certificate</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>其它证书</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2167" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6F647B1F" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2326" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="7CD4506A" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:leftChars="-51" w:left="1" w:rightChars="-51" w:right="-107"
                                               w:hangingChars="60" w:hanging="108"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1687" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="781DF1F5" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="169538D0" w14:textId="77777777" w:rsidR="00FE27F6" w:rsidRDefault="00FE27F6">
                        <w:pPr>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="15"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="15"/>
                            </w:rPr>
                            <w:t xml:space="preserve">DETAILS OF COURSE / CERTIFICATES  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="15"/>
                            </w:rPr>
                            <w:t>专业培训证</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblInd w:w="-252" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0"
                                       w:noHBand="0" w:noVBand="0"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="6146"/>
                            <w:gridCol w:w="1305"/>
                            <w:gridCol w:w="2433"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="5CBC149E" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="75B150E3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Item</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> (</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>项目</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="2E78FFB0" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>证书编号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="72F5DA14" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Issue Date (</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>签发日期</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="4794B515" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="366FEC35" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="隶书" w:eastAsia="隶书" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:eastAsia="隶书"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Certificate of professional Training</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="隶书" w:eastAsia="隶书" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="隶书" w:eastAsia="隶书" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>专业培训合格证书</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="隶书" w:eastAsia="隶书" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="7EC57BED" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="793028A8" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="41320A26" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="2EF3D0CE" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Basic Safety Training</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>基本安全</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="5A9E3112" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="01CD6F43" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="0A1E3CEC" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="37BC1F1C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Survival Craft and Rescue Boats</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>救生艇、筏</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="470254B5" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="3F786DEF" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:widowControl w:val="0"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="75F73B46" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="4513EE12" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Advanced Fire Fighting</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>高级消防</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="28A0F531" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="41EB2BAC" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:tabs>
                                            <w:tab w:val="left" w:pos="270"/>
                                        </w:tabs>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="5F9BF888" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="28A9EBCF" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Medical First Aid</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>精通急救</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="19C835F3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="30CA6546" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:tabs>
                                            <w:tab w:val="left" w:pos="195"/>
                                        </w:tabs>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="293D066A" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="7D9A6BE5" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Radar Observer Automatic Radar Piloting</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>雷达两小证</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="282A1F03" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="72F48270" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="43ED9196" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="252BD093" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Ship Captain Medicare</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>船上医护</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1CED567F" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="581BF75D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6" w:rsidP="00AA08C0">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="044B802A" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="0BF7096D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Special Training Certificate</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="隶书" w:eastAsia="隶书" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>特殊专业培训合格证</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="隶书" w:eastAsia="隶书" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1F315D96" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="43E005CE" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="00B97814" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="5F8073F1" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Oil Tanker Familiarization/operations</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>油安/油操</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="5DECD697" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="61F14A3A" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="197D1E1E" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6640450B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:eastAsia="隶书"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Chemical Tanker Familiarizations/operations</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>(</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>化安/化操</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="4AA5B686" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="52581DD4" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="5DC07764" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="6146" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="6FF91F0A" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> S</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>hip</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>security</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>officer</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> (</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>船舶保安员</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>)</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1305" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="4328C109" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2433" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="0D826099" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:pStyle w:val="1"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Times New Roman"/>
                                            <w:b w:val="0"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="3BB11ECE" w14:textId="77777777" w:rsidR="00FE27F6" w:rsidRDefault="00FE27F6">
                        <w:pPr>
                            <w:ind w:firstLine="435"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                <w:sz w:val="18"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:sz w:val="18"/>
                            </w:rPr>
                            <w:t xml:space="preserve">Previous Sea Experience  </w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                <w:sz w:val="18"/>
                            </w:rPr>
                            <w:t xml:space="preserve">海上资历 </w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="0" w:type="auto"/>
                            <w:tblInd w:w="-252" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0"
                                       w:noHBand="0" w:noVBand="0"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="1494"/>
                            <w:gridCol w:w="666"/>
                            <w:gridCol w:w="1260"/>
                            <w:gridCol w:w="1260"/>
                            <w:gridCol w:w="1620"/>
                            <w:gridCol w:w="1260"/>
                            <w:gridCol w:w="1080"/>
                            <w:gridCol w:w="1260"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="26FC0FBD" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:val="1040"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1494" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="14F798C2" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Ship’s Name</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="79AC0EBC" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:ind w:firstLineChars="200" w:firstLine="360"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>船名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="666" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="58BE2E58" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Rank</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="1304BD1D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>职务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2520" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="37D7BE87" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Service Time</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="6AB51556" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>在船时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1620" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="40006B58" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>VSL Type/GRT</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="6B22E668" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>船舶类型/总吨</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="34EE2303" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>M/E Type/BHP</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="2EC1EE0D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>主机型号</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>马力</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1080" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="60D638E5" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Areas</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="155190AC" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>航区</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="7527C40A" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">Ship’s </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>Owner</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="11989E42" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>公司名称</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <#list experienceList as experience>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="5C0E01E3" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="307"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1494" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="2EF47D42" w14:textId="588826CE" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3">
                                    <w:r w:rsidRPr="006919B3">
                                        <w:t>${experience.vesselName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="666" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="74761E45" w14:textId="189468F0" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:sz w:val="16"/>
                                            <w:szCs w:val="16"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="006919B3">
                                        <w:rPr>
                                            <w:sz w:val="16"/>
                                            <w:szCs w:val="16"/>
                                        </w:rPr>
                                        <w:t>${experience.crewPostex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="72744057" w14:textId="258C8020" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3" w:rsidP="00AA08C0">
                                    <w:pPr>
                                        <w:tabs>
                                            <w:tab w:val="left" w:pos="465"/>
                                        </w:tabs>
                                    </w:pPr>
                                    <w:r w:rsidRPr="006919B3">
                                        <w:t>${(experience.startTime?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="70D52C6F" w14:textId="1B0C7A9E" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3" w:rsidP="006919B3">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="006919B3">
                                        <w:t>${(experience.endTime?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1620" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="3D168EA4" w14:textId="78A58A9F" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="006919B3">
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${experience.shipTypeex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="256B0E35" w14:textId="12266FD8" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="006919B3">
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${experience.hostModel}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1080" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="170CDEEC" w14:textId="12EA6EE0" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3" w:rsidP="00AA08C0">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:rightChars="-86" w:right="-181"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="006919B3">
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${experience.navigationAreaex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="63D0CB24" w14:textId="7F8D7E15" w:rsidR="00FE27F6"
                                     w:rsidRDefault="006919B3">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="006919B3">
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>${experience.companyName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="47022A0D" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="307"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1494" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="206B4B31" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="666" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="4F305597" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:sz w:val="16"/>
                                            <w:szCs w:val="16"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="0C5063F1" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:tabs>
                                            <w:tab w:val="left" w:pos="465"/>
                                        </w:tabs>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="0316BF1D" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1620" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1AAE759B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="31E28C62" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1080" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="5B306A9C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:left="2" w:rightChars="-86" w:right="-181" w:hangingChars="1"
                                               w:hanging="2"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="6B76C8CC" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="763FB8F7" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="307"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1494" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="39141CF3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="666" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="53A76327" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:ind w:rightChars="-89" w:right="-187"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="5793B6F1" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="58DE2B58" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1620" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5E31A91B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="32315E5B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="2AFD91F7" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="5CD58074" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="05ADC62C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="76984C44" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="4BB2B514" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>GGGTG</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="21D99B28" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1080" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="34220DAC" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:left="2" w:rightChars="-86" w:right="-181" w:hangingChars="1"
                                               w:hanging="2"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="1973458C" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="0A93127B" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="307"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1494" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="05A7C2E9" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="666" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="0FED1CC5" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:ind w:rightChars="-89" w:right="-187"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="167566EB" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="30FA0570" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1620" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="02AF58C8" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="1BEDA1E3" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1080" w:type="dxa"/>
                                </w:tcPr>
                                <w:p w14:paraId="066841E9" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:left="2" w:rightChars="-86" w:right="-181" w:hangingChars="1"
                                               w:hanging="2"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="68B533DA" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00FE27F6" w14:paraId="2E3087CD" w14:textId="77777777">
                            <w:trPr>
                                <w:cantSplit/>
                                <w:trHeight w:hRule="exact" w:val="307"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1494" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="4738BBD2" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="666" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="668A3789" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:ind w:rightChars="-89" w:right="-187"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="5F817136" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="03E11AC8" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6"/>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1620" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6DCD2F4A" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="739C1268" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="457B089B" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="1C0600A5" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="2D5387B8" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="788A6625" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="707DA481" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:wordWrap w:val="0"/>
                                        <w:jc w:val="center"/>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                        </w:rPr>
                                        <w:t>GGGTG</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="1F1CEDDD" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1080" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="33DDFAA6" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:ind w:left="2" w:rightChars="-86" w:right="-181" w:hangingChars="1"
                                               w:hanging="2"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1260" w:type="dxa"/>
                                    <w:tcBorders>
                                        <w:bottom w:val="dotted" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="65E57118" w14:textId="77777777" w:rsidR="00FE27F6"
                                     w:rsidRDefault="00FE27F6">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="16D69466" w14:textId="77777777" w:rsidR="00FE27F6" w:rsidRDefault="00FE27F6">
                        <w:pPr>
                            <w:spacing w:line="400" w:lineRule="exact"/>
                            <w:ind w:leftChars="300" w:left="630"/>
                            <w:rPr>
                                <w:sz w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:sectPr w:rsidR="00FE27F6">
                        <w:headerReference w:type="default" r:id="rId6"/>
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="567" w:right="1134" w:bottom="567" w:left="1134" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="720"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId8" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
                              Target="webSettings.xml"/>
                <Relationship Id="rId7"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
                <Relationship Id="rId6"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/header"
                              Target="header1.xml"/>
                <Relationship Id="rId5"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"
                              Target="endnotes.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"
                              Target="footnotes.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/footnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml">
        <pkg:xmlData>
            <w:footnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:footnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="13370BE5" w14:textId="77777777" w:rsidR="007E1277" w:rsidRDefault="007E1277">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:footnote>
                <w:footnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="63CAAD0F" w14:textId="77777777" w:rsidR="007E1277" w:rsidRDefault="007E1277">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:footnote>
            </w:footnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/endnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml">
        <pkg:xmlData>
            <w:endnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:endnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="3F358F40" w14:textId="77777777" w:rsidR="007E1277" w:rsidRDefault="007E1277">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:endnote>
                <w:endnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="21AF2E46" w14:textId="77777777" w:rsidR="007E1277" w:rsidRDefault="007E1277">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:endnote>
            </w:endnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/header1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml">
        <pkg:xmlData>
            <w:hdr xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                   xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                   xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                   xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                   xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                   xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                   xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                   xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                   xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                   xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                   xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                   xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                   xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                   xmlns:o="urn:schemas-microsoft-com:office:office"
                   xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                   xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                   xmlns:v="urn:schemas-microsoft-com:vml"
                   xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                   xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                   xmlns:w10="urn:schemas-microsoft-com:office:word"
                   xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                   xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                   xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                   xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                   xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                   xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                   xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                   xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                   xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                   mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:p w14:paraId="2594982B" w14:textId="77777777" w:rsidR="007F16FA" w:rsidRDefault="007F16FA">
                    <w:pPr>
                        <w:pStyle w:val="a3"/>
                        <w:pBdr>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:pBdr>
                        <w:rPr>
                            <w:b/>
                            <w:sz w:val="44"/>
                        </w:rPr>
                    </w:pPr>
                    <w:r>
                        <w:rPr>
                            <w:rFonts w:hint="eastAsia"/>
                            <w:b/>
                            <w:sz w:val="44"/>
                        </w:rPr>
                        <w:t xml:space="preserve">    </w:t>
                    </w:r>
                </w:p>
            </w:hdr>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题​​">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="CAEACE"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4472C4"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="等线 Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游ゴシック Light"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线 Light"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="等线" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main"
                                           name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}"
                                           vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"/>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main"
                        mc:Ignorable="w14 w15 w16se w16cid">
                <w:zoom w:percent="100"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:stylePaneFormatFilter w:val="3F01" w:allStyles="1" w:customStyles="0" w:latentStyles="0"
                                         w:stylesInUse="0" w:headingStyles="0" w:numberingStyles="0" w:tableStyles="0"
                                         w:directFormattingOnRuns="1" w:directFormattingOnParagraphs="1"
                                         w:directFormattingOnNumbering="1" w:directFormattingOnTables="1"
                                         w:clearFormatting="1" w:top3HeadingStyles="1" w:visibleStyles="0"
                                         w:alternateStyleNames="0"/>
                <w:doNotTrackMoves/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:doNotShadeFormData/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:doNotValidateAgainstSchema/>
                <w:doNotDemarcateInvalidXml/>
                <w:hdrShapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2049"/>
                </w:hdrShapeDefaults>
                <w:footnotePr>
                    <w:footnote w:id="-1"/>
                    <w:footnote w:id="0"/>
                </w:footnotePr>
                <w:endnotePr>
                    <w:endnote w:id="-1"/>
                    <w:endnote w:id="0"/>
                </w:endnotePr>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:useNormalStyleForList/>
                    <w:doNotUseIndentAsNumberingTabStop/>
                    <w:useAltKinsokuLineBreakRules/>
                    <w:allowSpaceOfSameStyleInTable/>
                    <w:doNotSuppressIndentation/>
                    <w:doNotAutofitConstrainedTables/>
                    <w:autofitToFirstFixedWidthCell/>
                    <w:displayHangulFixedWidth/>
                    <w:splitPgBreakAndParaMark/>
                    <w:doNotVertAlignCellWithSp/>
                    <w:doNotBreakConstrainedForcedTable/>
                    <w:doNotVertAlignInTxbx/>
                    <w:useAnsiKerningPairs/>
                    <w:cachedColBalance/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="11"/>
                    <w:compatSetting w:name="allowHyphenationAtTrackBottom"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="useWord2013TrackBottomHyphenation"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="00172A27"/>
                    <w:rsid w:val="00172A27"/>
                    <w:rsid w:val="00184145"/>
                    <w:rsid w:val="00255BEC"/>
                    <w:rsid w:val="00282C28"/>
                    <w:rsid w:val="00297DF3"/>
                    <w:rsid w:val="00377B99"/>
                    <w:rsid w:val="00380C70"/>
                    <w:rsid w:val="00383379"/>
                    <w:rsid w:val="004F0FBB"/>
                    <w:rsid w:val="0056530A"/>
                    <w:rsid w:val="006919B3"/>
                    <w:rsid w:val="007E1277"/>
                    <w:rsid w:val="007F16FA"/>
                    <w:rsid w:val="0092125A"/>
                    <w:rsid w:val="00AA08C0"/>
                    <w:rsid w:val="00B675FD"/>
                    <w:rsid w:val="00DB283A"/>
                    <w:rsid w:val="00E53BBE"/>
                    <w:rsid w:val="00EE0E98"/>
                    <w:rsid w:val="00FE27F6"/>
                    <w:rsid w:val="119D3230"/>
                    <w:rsid w:val="7E8F5259"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:doNotIncludeSubdocsInStats/>
                <w:doNotAutoCompressPictures/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2049"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="351BDFA2"/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{8922867A-DE8B-4D91-8F40-C0819CA90BF1}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                      xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                      xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                      xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                      xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                      mc:Ignorable="w14 w15 w16se w16cid">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman"
                                      w:cs="Times New Roman"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0"
                                w:defQFormat="0" w:count="377">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1"
                                    w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:uiPriority="59"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hashtag" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Unresolved Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link Error" w:semiHidden="1" w:unhideWhenUsed="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:kern w:val="2"/>
                        <w:sz w:val="21"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="1">
                    <w:name w:val="heading 1"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:keepNext/>
                        <w:widowControl/>
                        <w:outlineLvl w:val="0"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="隶书"/>
                        <w:b/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="2">
                    <w:name w:val="heading 2"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:keepNext/>
                        <w:ind w:leftChars="-51" w:left="1" w:rightChars="-41" w:right="-41" w:hangingChars="108"
                               w:hanging="108"/>
                        <w:outlineLvl w:val="1"/>
                    </w:pPr>
                    <w:rPr>
                        <w:b/>
                        <w:sz w:val="10"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="4">
                    <w:name w:val="heading 4"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:keepNext/>
                        <w:jc w:val="center"/>
                        <w:outlineLvl w:val="3"/>
                    </w:pPr>
                    <w:rPr>
                        <w:b/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a3">
                    <w:name w:val="header"/>
                    <w:basedOn w:val="a"/>
                    <w:pPr>
                        <w:pBdr>
                            <w:bottom w:val="single" w:sz="6" w:space="1" w:color="auto"/>
                        </w:pBdr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="center"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:styleId="a4">
                    <w:name w:val="Hyperlink"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman"
                                  w:cs="Times New Roman"/>
                        <w:color w:val="0000FF"/>
                        <w:u w:val="single"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a5">
                    <w:name w:val="footer"/>
                    <w:basedOn w:val="a"/>
                    <w:pPr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a6">
                    <w:name w:val="Balloon Text"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a7"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="004F0FBB"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a7">
                    <w:name w:val="批注框文本 字符"/>
                    <w:link w:val="a6"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:rsid w:val="004F0FBB"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman"
                                  w:cs="Times New Roman"/>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                           xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                           xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                           xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                           xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                           xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                           xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                           mc:Ignorable="w14 w15 w16se w16cid">
                <w:optimizeForBrowser/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                     xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                     xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                     xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                     xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                     xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                     mc:Ignorable="w14 w15 w16se w16cid">
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="隶书">
                    <w:altName w:val="宋体"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="00000001" w:usb1="080E0000" w:usb2="00000010" w:usb3="00000000" w:csb0="00040000"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋">
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线 Light">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线">
                    <w:altName w:val="DengXian"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml"
              pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:dcmitype="http://purl.org/dc/dcmitype/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title>大连格雷特船务代理有限公司</dc:title>
                <dc:subject/>
                <dc:creator>Administrator</dc:creator>
                <cp:keywords/>
                <dc:description>0411-82719617</dc:description>
                <cp:lastModifiedBy>夏 德航</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <dcterms:created xsi:type="dcterms:W3CDTF">2020-05-25T02:05:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2020-05-25T02:05:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>0</TotalTime>
                <Pages>1</Pages>
                <Words>255</Words>
                <Characters>1460</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <PresentationFormat/>
                <Lines>12</Lines>
                <Paragraphs>3</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company>微软系统</Company>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>1712</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/custom.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.custom-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/custom-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <property fmtid="{D5CDD505-2E9C-101B-9397-08002B2CF9AE}" pid="2" name="KSOProductBuildVer">
                    <vt:lpwstr>2052-10.1.0.5511</vt:lpwstr>
                </property>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>