<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml"
              pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties"
                              Target="docProps/custom.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:background w:color="FFFFFF"/>
                <w:body>
                    <w:p w14:paraId="1B72B25D" w14:textId="77777777" w:rsidR="008836A9" w:rsidRPr="008D3283"
                         w:rsidRDefault="0060264A" w:rsidP="008D3283">
                        <w:pPr>
                            <w:spacing w:line="300" w:lineRule="exact"/>
                            <w:ind w:firstLineChars="900" w:firstLine="2530"/>
                            <w:rPr>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>船员</w:t>
                        </w:r>
                        <w:r w:rsidR="00E6035D" w:rsidRPr="008D3283">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>应聘</w:t>
                        </w:r>
                        <w:r w:rsidR="00A77E84" w:rsidRPr="008D3283">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>登记</w:t>
                        </w:r>
                        <w:r w:rsidR="002A2629" w:rsidRPr="008D3283">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>评估</w:t>
                        </w:r>
                        <w:r w:rsidR="00A77E84" w:rsidRPr="008D3283">
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:b/>
                                <w:bCs/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>表</w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="10349" w:type="dxa"/>
                            <w:tblInd w:w="-885" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0"
                                       w:noHBand="0" w:noVBand="0"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="1139"/>
                            <w:gridCol w:w="563"/>
                            <w:gridCol w:w="229"/>
                            <w:gridCol w:w="196"/>
                            <w:gridCol w:w="801"/>
                            <w:gridCol w:w="186"/>
                            <w:gridCol w:w="289"/>
                            <w:gridCol w:w="815"/>
                            <w:gridCol w:w="495"/>
                            <w:gridCol w:w="382"/>
                            <w:gridCol w:w="195"/>
                            <w:gridCol w:w="98"/>
                            <w:gridCol w:w="45"/>
                            <w:gridCol w:w="940"/>
                            <w:gridCol w:w="84"/>
                            <w:gridCol w:w="206"/>
                            <w:gridCol w:w="839"/>
                            <w:gridCol w:w="12"/>
                            <w:gridCol w:w="848"/>
                            <w:gridCol w:w="286"/>
                            <w:gridCol w:w="1701"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="00930D40" w:rsidRPr="008A3633" w14:paraId="4AA42FC7" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1A315D4C" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="00930D40" w:rsidP="00C1600F">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>姓 名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1412" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="72D4F955" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="003728DF" w:rsidP="00C1600F">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00124E7E">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.crewName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1599" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6F6E3214" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="00930D40" w:rsidP="00C1600F">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>曾 用 名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1660" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7184215F" w14:textId="3C6DD685" w:rsidR="00930D40"
                                     w:rsidRDefault="00930D40" w:rsidP="00E21EC4">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1129" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7501404E" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="00930D40" w:rsidP="00C1600F">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>性 别</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="860" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4C0CE7D9" w14:textId="5CCB7D76" w:rsidR="00930D40"
                                     w:rsidRDefault="00F50CFC" w:rsidP="000A408A">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00124E7E">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${sex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1987" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="26273EE2" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="00930D40" w:rsidP="000A408A">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00930D40" w:rsidRPr="008A3633" w14:paraId="2ECC6934" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="17C9D0AB" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>英文姓名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1412" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3456EEC3" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="00930D40">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1599" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="061282FD" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>籍贯</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1660" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0F37D6B7" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00A34AB3" w:rsidP="00A34AB3">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00A34AB3">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.birthPlace}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1129" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6C34E3CC" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>民族</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="860" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="44140797" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1987" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vMerge/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4F73F6B5" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00930D40" w:rsidRPr="008A3633" w14:paraId="513DE447" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="72ECB57F" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>出生年月</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1412" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7DFBBF85" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="007F7FDD"
                                     w:rsidRDefault="00EC2E03" w:rsidP="007F7FDD">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="007F7FDD">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
<#--                                        <w:t>${crewInfo.birthday?date!}</w:t>-->
                                        <w:t>${(crewInfo.birthday?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1599" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3DEF5138" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>应聘职位</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3649" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="210BAD95" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="00965977">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1987" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vMerge/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="580EF79B" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00930D40" w:rsidRPr="008A3633" w14:paraId="4EC4CE83" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4E6B5202" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身份证号</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3011" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0635E4E1" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="003728DF" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.documentNumber}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1744" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5C9A5085" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">婚姻状况 </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1905" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="270FF4C8" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1987" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vMerge/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2A2E56CD" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00930D40" w:rsidRPr="008A3633" w14:paraId="01869530" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6F3361D6" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>联系方式（个人）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3011" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="213D3E50" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="003728DF" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:b/>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.phoneNumber}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1744" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="458C485F" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="00930D40" w:rsidP="00965977">
                                    <w:pPr>
                                        <w:ind w:firstLineChars="150" w:firstLine="315"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>家属电话</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="31C13F07" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1905" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5D9BB334" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1987" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vMerge/>
                                    <w:tcBorders>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3936AC8E" w14:textId="77777777" w:rsidR="00930D40" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00930D40" w:rsidP="008D3283">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00930D40" w:rsidRPr="008A3633" w14:paraId="7AD80BEC" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2315FE7F" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="00930D40" w:rsidP="00C1600F">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>住 址</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8647" w:type="dxa"/>
                                    <w:gridSpan w:val="19"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1DD1207D" w14:textId="77777777" w:rsidR="00930D40"
                                     w:rsidRDefault="003728DF" w:rsidP="00C1600F">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.homeAddress}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="7E6FA8EC" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4D4E613F" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>毕业院校</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2516" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="52F57D89" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008F0647">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.school}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1215" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2EDDCDD5" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>毕业时间</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2081" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0E2E2EC9" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="007F7FDD"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="007F7FDD">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${(crewInfo.graduationTime?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1134" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1CC596A9" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>政治面貌</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6331895D" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="007F7FDD">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.political}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="48FE9B72" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1CB05468" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>学历</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1226" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="33A2D833" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.education}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1290" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="094CDBC5" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>专 业</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3296" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6497D3BB" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.major}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1134" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2FA28B7F" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>语言能力</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="09CE4375" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="4FB6B532" w14:textId="77777777"
                              w:rsidTr="00CB0A2C">
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1702" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6A9E4A3D" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>体貌特征</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1226" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="55930098" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>身 高</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1290" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="038A996B" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1170" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="772C5B4F" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>体重</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2126" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7BB7E3CD" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1134" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="58AB2449" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>鞋码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="389BE541" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="095941C2" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="394"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2928" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2F521D0D" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">疾病情况说明 </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7421" w:type="dxa"/>
                                    <w:gridSpan w:val="16"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7C31460C" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="6A405DC4" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="240"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2928" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="403C6BA5" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>个人特长</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7421" w:type="dxa"/>
                                    <w:gridSpan w:val="16"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="68FA6D4F" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="5287F280" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="407"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="425A21E3" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>工作简历</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="16D2DAB5" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4151" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="321D120B" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:ind w:firstLineChars="300" w:firstLine="630"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>起止年月</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5059" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="204A03FE" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:ind w:firstLineChars="300" w:firstLine="630"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>公司名称及职务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <#list experienceList as experience>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="176BD57F" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="337"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="446F006D" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4151" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="71702CF6" w14:textId="1DBF102D" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="000A0DEA" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${(experience.startTime?string("yyyy-MM-dd"))!}:</w:t><w:t>${(experience.endTime?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5059" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7779D1A0" w14:textId="6D9562DB" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="0028784A" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.companyName}:</w:t><w:t>${experience.crewPostex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="77C30CA8" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="342"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1EEA320B" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4151" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="198E9FB9" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5059" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0F9239E0" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="left"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="5F42932E" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="377"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="10C97169" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4151" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1B107DAC" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="5059" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="62B1EF9F" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="21E0EC0F" w14:textId="77777777"
                              w:rsidTr="003B627E">
                            <w:trPr>
                                <w:trHeight w:val="456"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1815458C" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="007A276A"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:color w:val="333333"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>培训合格证情况</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0319CCAD" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>证书名称</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="0FCDC0E6" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="636DE5F0" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>签发日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0A1B0361" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>有效年月</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="495D55E3" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1CB4C168" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>签发机关</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="5D6C6163" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="1B6AE6B1" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4CCCADCD" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="63346DB5" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>基本安全培训</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6D61A998" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="56B41B30" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5683DABA" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="6E93AC2C" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6F20D4F6" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5996D834" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">救生艇筏 </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0AF12008" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5F667DEA" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="79F95DE2" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="4A996045" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3241049A" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="035A6217" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>高级消防</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3D71AD2D" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4D0D6041" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="15326D57" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="65F8E581" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="790F23B6" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="54D9E57E" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>精通急救</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4645EDA9" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="03BB52E7" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4468079D" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="7C8AADE1" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="18896D0B" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6B28F70A" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>保安意识</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1D9A35BE" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="400C7E49" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3A557C34" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="185504D8" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6F3D8C84" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2744A6DB" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>负有保安职责</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0FC1DFA0" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1BACBC38" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4CBD6192" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="5670E7F0" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="90"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="10A78E0A" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0700D89F" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>客船船员特殊培训</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="46830E13" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2CBFBF3A" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5317E31E" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="1A005399" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="90"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="447ECD8F" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4FA0CBF4" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="003E598C">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">船舶保安员 </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="66BD23C8" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0112AF30" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="44D77DC2" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="346B7840" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1993C48A" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>证书</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="54E79F33" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>情况</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="54D342CA" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:numPr>
                                            <w:ilvl w:val="0"/>
                                            <w:numId w:val="1"/>
                                        </w:numPr>
                                        <w:autoSpaceDN w:val="0"/>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="47B6225D" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>适任证书</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3151219A" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1082E64B" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="08C97A74" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="079808EA" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="19F10D5D" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4F9A61A8" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="007A276A"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>GMDSS</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t>证书</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="70A13EB3" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="74EF3C9B" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="18F4EF96" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="0530810B" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6E9AAE0E" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3F144001" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>服务簿</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="06E83604" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="156CEE27" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0EF91893" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="641BED69" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="30D33AF8" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="328A09DF" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>海员证</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="74A97851" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2479A022" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6900F09F" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="61B52CF3" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1E6FB355" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3E3F83B8" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>护照</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="25ACFA65" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="780EB248" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="66DA59D5" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="50780E88" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7E8A47AE" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="19ECE383" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>巴拿马证书</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2FE0D176" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5ED07089" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7D7A1B59" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="38589335" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="28D25225" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="27A62E02" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>健康证书</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2D3FD107" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0442CC45" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="11A31490" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="4F051E43" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="39142941" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2264" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="557F63A6" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>其他</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3260" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="131ECA9F" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1F4FFE25" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5AD596C0" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="1FA33288" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="158"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1266DA5E" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>海上</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="0ED3FC74" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>资历</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="7588995F" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="792" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1470CA4D" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>船名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1472" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="460D97F5" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>职务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1692" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="128B5D63" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>上/下船日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1568" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="078A481B" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>船舶航区</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5EFCDC91" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>船舶类型/总吨/</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="25A69FB0" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>马力</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0448E3D4" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008A3633">
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>船舶所属公司</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="7B58FB07" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <#list experienceList as experience>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="346544AA" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="482"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="52D64FF7" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="792" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="02C6984B" w14:textId="5218A29C" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="0028784A" w:rsidP="0028784A">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.vesselName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1472" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="17CD1565" w14:textId="6F8B2CD4" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="0028784A" w:rsidP="0028784A">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:lastRenderedPageBreak/>
                                        <w:t>${experience.crewPostex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1692" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="31BA7440" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1568" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="590A7A57" w14:textId="56FFE8DE" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="0028784A" w:rsidP="0028784A">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.navigationAreaex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="02043333" w14:textId="541BAFEE" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="0028784A" w:rsidP="0028784A">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.shipTypeex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="762CEBF6" w14:textId="3CCF385E" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="0028784A" w:rsidP="0028784A">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.shipownerName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        </#list>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="42D9A790" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="422"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2C0807C0" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="792" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4B1E09FF" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1472" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5A724CC8" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1692" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6DF889FE" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1568" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="72D2C058" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1A29DF8A" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="53717671" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="1A4AEDBD" w14:textId="77777777"
                              w:rsidTr="004856D3">
                            <w:trPr>
                                <w:trHeight w:val="437"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1139" w:type="dxa"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="32FE2956" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="792" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5B84A2E7" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1472" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0A6A6FE2" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1692" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3052DD29" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1568" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="75FB403D" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="42DC55E7" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="19D84E92" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="5C19EBD0" w14:textId="77777777"
                              w:rsidTr="00DC0E4B">
                            <w:trPr>
                                <w:trHeight w:val="437"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10349" w:type="dxa"/>
                                    <w:gridSpan w:val="21"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="478F3610" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">您是否有异体特征（纹身等） 是 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">      否</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="4B5D0D54" w14:textId="77777777"
                              w:rsidTr="0053351A">
                            <w:trPr>
                                <w:trHeight w:val="437"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10349" w:type="dxa"/>
                                    <w:gridSpan w:val="21"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="697868E5" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">您是否在国外有非法入境或非法务工等不良记录（特别是韩国、日本） 是 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">      否</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="4EBF75C7" w14:textId="77777777"
                              w:rsidTr="00D403A8">
                            <w:trPr>
                                <w:trHeight w:val="442"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10349" w:type="dxa"/>
                                    <w:gridSpan w:val="21"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4BD53E2F" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="008A3633"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>本人保证上述内容真实，如有不实，愿承担相关责任。 声明人： 日期：</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="48863ABF" w14:textId="77777777"
                              w:rsidTr="00D403A8">
                            <w:trPr>
                                <w:trHeight w:val="442"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="10349" w:type="dxa"/>
                                    <w:gridSpan w:val="21"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="221631EE" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="1550" w:firstLine="3255"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>以 下 内 容 由 公 司 填 写</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w:rsidRPr="008A3633" w14:paraId="6E0DAD12" w14:textId="77777777"
                              w:rsidTr="00017552">
                            <w:trPr>
                                <w:trHeight w:val="442"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2127" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="60888F92" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>最终确认的聘任职务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8222" w:type="dxa"/>
                                    <w:gridSpan w:val="17"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="71F8BA3D" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w14:paraId="17BA16A2" w14:textId="77777777" w:rsidTr="00017552">
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:trHeight w:val="1178"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2127" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1DB56F92" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>评估结果</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8222" w:type="dxa"/>
                                    <w:gridSpan w:val="17"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="5485FA65" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>针对拟聘船员考核结果为：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="32E3E409" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">        优秀□ 良好□ 合格□ 差□</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="6BB003F6" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="5A94D446" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="1AA06E74" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>安监、机务部/日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w14:paraId="6CC92F10" w14:textId="77777777" w:rsidTr="00017552">
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:trHeight w:val="1178"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2127" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="59030B0F" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>评估结果</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8222" w:type="dxa"/>
                                    <w:gridSpan w:val="17"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="64CC1FDB" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>针对拟聘船员考核结果为：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="05EEF37F" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">        优秀□ 良好□ 合格□ 差□</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="3B0007EF" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="0B3D2987" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="446EE161" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>船管部/日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w14:paraId="510270D9" w14:textId="77777777" w:rsidTr="00017552">
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:trHeight w:val="1178"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2127" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7D1F5712" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>评估结果</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8222" w:type="dxa"/>
                                    <w:gridSpan w:val="17"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="365C3ADC" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>针对拟聘船员考核结果为：</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="0B4AE45D" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">        优秀□ 良好□ 合格□ 差□</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="5327807A" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="2D78A71B" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="08AF8809" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>体系办/日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w14:paraId="0372354C" w14:textId="77777777" w:rsidTr="00017552">
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:trHeight w:val="1554"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2127" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="65B01BC8" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>船员管理部</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="3026B787" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>意见</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8222" w:type="dxa"/>
                                    <w:gridSpan w:val="17"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="585B70C9" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>优秀□ 良好□ 合格□ 差□</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="4702B872" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="001E25BB"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="4E6AB6B2" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>同意聘用□ 　　不同意聘用□</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="6EEF96DD" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="001E25BB"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="787B9212" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="379FAB0F" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>船员管理部/日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="00EC2E03" w14:paraId="3A92F55D" w14:textId="77777777" w:rsidTr="00017552">
                            <w:tblPrEx>
                                <w:tblBorders>
                                    <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                    <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                                </w:tblBorders>
                            </w:tblPrEx>
                            <w:trPr>
                                <w:trHeight w:val="1716"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2127" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4F7345BA" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>总经理</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="1446D55A" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>意见</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8222" w:type="dxa"/>
                                    <w:gridSpan w:val="17"/>
                                    <w:tcBorders>
                                        <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                        <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    </w:tcBorders>
                                </w:tcPr>
                                <w:p w14:paraId="7C739CEE" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>优秀□ 良好□ 合格□ 差□</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="7A586259" w14:textId="77777777" w:rsidR="00EC2E03" w:rsidRPr="001E25BB"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="200" w:firstLine="420"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="2BC1F555" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>同意聘用□ 　　不同意聘用□</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="4AED813D" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="200" w:firstLine="420"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="1483E651" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:line="300" w:lineRule="exact"/>
                                        <w:ind w:firstLineChars="200" w:firstLine="420"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="23276D25" w14:textId="77777777" w:rsidR="00EC2E03"
                                     w:rsidRDefault="00EC2E03" w:rsidP="00EC2E03">
                                    <w:pPr>
                                        <w:spacing w:before="156" w:line="300" w:lineRule="exact"/>
                                        <w:textAlignment w:val="baseline"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:hint="eastAsia"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>总经理/日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="32DBD138" w14:textId="77777777" w:rsidR="00A77E84" w:rsidRDefault="00C115C3"
                         w:rsidP="008D3283">
                        <w:pPr>
                            <w:spacing w:line="300" w:lineRule="exact"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>备注：应聘者应提供</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>5</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>寸全身彩照一张、身份证复印件一张、学历证明、其他专业资格证书（如英语</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>6</w:t>
                        </w:r>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>级等）、有资历的船员还应提船员资历证明（船员服务簿资历复印件）</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="35F6226B" w14:textId="77777777" w:rsidR="003E598C" w:rsidRDefault="003E598C"
                         w:rsidP="00504278">
                        <w:pPr>
                            <w:spacing w:line="500" w:lineRule="exact"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">      </w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="3E932AE1" w14:textId="77777777" w:rsidR="009513AC" w:rsidRDefault="009513AC"
                         w:rsidP="00504278">
                        <w:pPr>
                            <w:spacing w:line="500" w:lineRule="exact"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">    </w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="2EE0C292" w14:textId="77777777" w:rsidR="00C115C3" w:rsidRPr="00C115C3"
                         w:rsidRDefault="00C115C3" w:rsidP="00504278">
                        <w:pPr>
                            <w:spacing w:line="500" w:lineRule="exact"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">       </w:t>
                        </w:r>
                    </w:p>
                    <w:sectPr w:rsidR="00C115C3" w:rsidRPr="00C115C3" w:rsidSect="0008466E">
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="567" w:right="1797" w:bottom="1440" w:left="1797" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="720"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId8"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
                <Relationship Id="rId7"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"
                              Target="endnotes.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"
                              Target="numbering.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXml"
                              Target="../customXml/item1.xml"/>
                <Relationship Id="rId6"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"
                              Target="footnotes.xml"/>
                <Relationship Id="rId5"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
                              Target="webSettings.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/footnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml">
        <pkg:xmlData>
            <w:footnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:footnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="3C6735CD" w14:textId="77777777" w:rsidR="00396721" w:rsidRDefault="00396721"
                         w:rsidP="008836A9">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:footnote>
                <w:footnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="1CF04828" w14:textId="77777777" w:rsidR="00396721" w:rsidRDefault="00396721"
                         w:rsidP="008836A9">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:footnote>
            </w:footnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/endnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml">
        <pkg:xmlData>
            <w:endnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:endnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="1127C175" w14:textId="77777777" w:rsidR="00396721" w:rsidRDefault="00396721"
                         w:rsidP="008836A9">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:endnote>
                <w:endnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="429AC73E" w14:textId="77777777" w:rsidR="00396721" w:rsidRDefault="00396721"
                         w:rsidP="008836A9">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:endnote>
            </w:endnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题​​">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="CAEACE"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4472C4"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="等线 Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游ゴシック Light"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线 Light"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="等线" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main"
                                           name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}"
                                           vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"/>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main"
                        mc:Ignorable="w14 w15 w16se w16cid">
                <w:zoom w:percent="100"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:stylePaneFormatFilter w:val="3F01" w:allStyles="1" w:customStyles="0" w:latentStyles="0"
                                         w:stylesInUse="0" w:headingStyles="0" w:numberingStyles="0" w:tableStyles="0"
                                         w:directFormattingOnRuns="1" w:directFormattingOnParagraphs="1"
                                         w:directFormattingOnNumbering="1" w:directFormattingOnTables="1"
                                         w:clearFormatting="1" w:top3HeadingStyles="1" w:visibleStyles="0"
                                         w:alternateStyleNames="0"/>
                <w:doNotTrackMoves/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridHorizontalSpacing w:val="105"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:doNotValidateAgainstSchema/>
                <w:doNotDemarcateInvalidXml/>
                <w:hdrShapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2049" fillcolor="#9cbee0" strokecolor="#739cc3">
                        <v:fill color="#9cbee0" color2="#bbd5f0" type="gradient">
                            <o:fill v:ext="view" type="gradientUnscaled"/>
                        </v:fill>
                        <v:stroke color="#739cc3" weight="1.25pt"/>
                    </o:shapedefaults>
                </w:hdrShapeDefaults>
                <w:footnotePr>
                    <w:footnote w:id="-1"/>
                    <w:footnote w:id="0"/>
                </w:footnotePr>
                <w:endnotePr>
                    <w:endnote w:id="-1"/>
                    <w:endnote w:id="0"/>
                </w:endnotePr>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:useNormalStyleForList/>
                    <w:doNotUseIndentAsNumberingTabStop/>
                    <w:useAltKinsokuLineBreakRules/>
                    <w:allowSpaceOfSameStyleInTable/>
                    <w:doNotSuppressIndentation/>
                    <w:doNotAutofitConstrainedTables/>
                    <w:autofitToFirstFixedWidthCell/>
                    <w:displayHangulFixedWidth/>
                    <w:splitPgBreakAndParaMark/>
                    <w:doNotVertAlignCellWithSp/>
                    <w:doNotBreakConstrainedForcedTable/>
                    <w:doNotVertAlignInTxbx/>
                    <w:useAnsiKerningPairs/>
                    <w:cachedColBalance/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="11"/>
                    <w:compatSetting w:name="allowHyphenationAtTrackBottom"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="useWord2013TrackBottomHyphenation"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="00172A27"/>
                    <w:rsid w:val="000017FB"/>
                    <w:rsid w:val="00017552"/>
                    <w:rsid w:val="00084412"/>
                    <w:rsid w:val="0008466E"/>
                    <w:rsid w:val="00084DEA"/>
                    <w:rsid w:val="000A0DEA"/>
                    <w:rsid w:val="000A408A"/>
                    <w:rsid w:val="000A74A9"/>
                    <w:rsid w:val="000C3983"/>
                    <w:rsid w:val="000C5AC3"/>
                    <w:rsid w:val="000D422D"/>
                    <w:rsid w:val="000F1B21"/>
                    <w:rsid w:val="001146B7"/>
                    <w:rsid w:val="001468A2"/>
                    <w:rsid w:val="00172A27"/>
                    <w:rsid w:val="001A1D37"/>
                    <w:rsid w:val="001B146B"/>
                    <w:rsid w:val="001B3162"/>
                    <w:rsid w:val="001C112E"/>
                    <w:rsid w:val="001C2AC8"/>
                    <w:rsid w:val="001C69D9"/>
                    <w:rsid w:val="001E25BB"/>
                    <w:rsid w:val="0020597A"/>
                    <w:rsid w:val="002574C1"/>
                    <w:rsid w:val="00263A90"/>
                    <w:rsid w:val="002758B2"/>
                    <w:rsid w:val="0028784A"/>
                    <w:rsid w:val="002A2629"/>
                    <w:rsid w:val="0034481D"/>
                    <w:rsid w:val="003449C6"/>
                    <w:rsid w:val="00345685"/>
                    <w:rsid w:val="00350389"/>
                    <w:rsid w:val="003728DF"/>
                    <w:rsid w:val="00375D87"/>
                    <w:rsid w:val="00394150"/>
                    <w:rsid w:val="0039662D"/>
                    <w:rsid w:val="00396721"/>
                    <w:rsid w:val="003B429B"/>
                    <w:rsid w:val="003B627E"/>
                    <w:rsid w:val="003C16A5"/>
                    <w:rsid w:val="003D1D60"/>
                    <w:rsid w:val="003D1EF6"/>
                    <w:rsid w:val="003E598C"/>
                    <w:rsid w:val="0042059F"/>
                    <w:rsid w:val="0043274D"/>
                    <w:rsid w:val="0043719D"/>
                    <w:rsid w:val="004856D3"/>
                    <w:rsid w:val="00496312"/>
                    <w:rsid w:val="004C1946"/>
                    <w:rsid w:val="004E5015"/>
                    <w:rsid w:val="004F2885"/>
                    <w:rsid w:val="004F76AF"/>
                    <w:rsid w:val="00504278"/>
                    <w:rsid w:val="005256AD"/>
                    <w:rsid w:val="0053351A"/>
                    <w:rsid w:val="00571740"/>
                    <w:rsid w:val="005A41D2"/>
                    <w:rsid w:val="005F09D4"/>
                    <w:rsid w:val="0060264A"/>
                    <w:rsid w:val="00621123"/>
                    <w:rsid w:val="006229A7"/>
                    <w:rsid w:val="00641732"/>
                    <w:rsid w:val="00641C2B"/>
                    <w:rsid w:val="00671391"/>
                    <w:rsid w:val="006B0236"/>
                    <w:rsid w:val="006C406F"/>
                    <w:rsid w:val="006C7D02"/>
                    <w:rsid w:val="006E6A52"/>
                    <w:rsid w:val="006F5F2B"/>
                    <w:rsid w:val="007038EA"/>
                    <w:rsid w:val="00715855"/>
                    <w:rsid w:val="0073182F"/>
                    <w:rsid w:val="0074156C"/>
                    <w:rsid w:val="00742618"/>
                    <w:rsid w:val="00751F81"/>
                    <w:rsid w:val="00767CC5"/>
                    <w:rsid w:val="00780DB9"/>
                    <w:rsid w:val="0078569F"/>
                    <w:rsid w:val="00785932"/>
                    <w:rsid w:val="007A276A"/>
                    <w:rsid w:val="007C3CC0"/>
                    <w:rsid w:val="007D55C0"/>
                    <w:rsid w:val="007F7FDD"/>
                    <w:rsid w:val="00804E0A"/>
                    <w:rsid w:val="00807839"/>
                    <w:rsid w:val="00812DCC"/>
                    <w:rsid w:val="00845831"/>
                    <w:rsid w:val="00850F27"/>
                    <w:rsid w:val="00856130"/>
                    <w:rsid w:val="00867810"/>
                    <w:rsid w:val="008816CD"/>
                    <w:rsid w:val="008836A9"/>
                    <w:rsid w:val="0089691A"/>
                    <w:rsid w:val="008A3633"/>
                    <w:rsid w:val="008C4226"/>
                    <w:rsid w:val="008D3283"/>
                    <w:rsid w:val="008E01AF"/>
                    <w:rsid w:val="008E46E8"/>
                    <w:rsid w:val="0090345B"/>
                    <w:rsid w:val="00906D93"/>
                    <w:rsid w:val="00911314"/>
                    <w:rsid w:val="009267A3"/>
                    <w:rsid w:val="0092781D"/>
                    <w:rsid w:val="00930D40"/>
                    <w:rsid w:val="009513AC"/>
                    <w:rsid w:val="00965977"/>
                    <w:rsid w:val="009E28B9"/>
                    <w:rsid w:val="00A27BE8"/>
                    <w:rsid w:val="00A34AB3"/>
                    <w:rsid w:val="00A55315"/>
                    <w:rsid w:val="00A6627F"/>
                    <w:rsid w:val="00A73FF8"/>
                    <w:rsid w:val="00A77E84"/>
                    <w:rsid w:val="00A94747"/>
                    <w:rsid w:val="00AA142A"/>
                    <w:rsid w:val="00AE3B2A"/>
                    <w:rsid w:val="00AE4A7F"/>
                    <w:rsid w:val="00B15A87"/>
                    <w:rsid w:val="00B17361"/>
                    <w:rsid w:val="00B2125B"/>
                    <w:rsid w:val="00B40183"/>
                    <w:rsid w:val="00B41876"/>
                    <w:rsid w:val="00B45440"/>
                    <w:rsid w:val="00B54E45"/>
                    <w:rsid w:val="00B60910"/>
                    <w:rsid w:val="00B7613B"/>
                    <w:rsid w:val="00BB58FC"/>
                    <w:rsid w:val="00BD4694"/>
                    <w:rsid w:val="00BE01E5"/>
                    <w:rsid w:val="00BF2C3A"/>
                    <w:rsid w:val="00BF3C40"/>
                    <w:rsid w:val="00C06B57"/>
                    <w:rsid w:val="00C115C3"/>
                    <w:rsid w:val="00C1600F"/>
                    <w:rsid w:val="00C2481C"/>
                    <w:rsid w:val="00C25A53"/>
                    <w:rsid w:val="00C33816"/>
                    <w:rsid w:val="00C61FB2"/>
                    <w:rsid w:val="00C9731A"/>
                    <w:rsid w:val="00CB0A2C"/>
                    <w:rsid w:val="00CE09ED"/>
                    <w:rsid w:val="00D12465"/>
                    <w:rsid w:val="00D14BC5"/>
                    <w:rsid w:val="00D25C92"/>
                    <w:rsid w:val="00D403A8"/>
                    <w:rsid w:val="00D5695E"/>
                    <w:rsid w:val="00D65F7C"/>
                    <w:rsid w:val="00D674F0"/>
                    <w:rsid w:val="00D76A5C"/>
                    <w:rsid w:val="00DB3E2E"/>
                    <w:rsid w:val="00DC0E4B"/>
                    <w:rsid w:val="00DC1FC1"/>
                    <w:rsid w:val="00DD61C3"/>
                    <w:rsid w:val="00E0238F"/>
                    <w:rsid w:val="00E12AF9"/>
                    <w:rsid w:val="00E21EC4"/>
                    <w:rsid w:val="00E5478D"/>
                    <w:rsid w:val="00E6035D"/>
                    <w:rsid w:val="00E74394"/>
                    <w:rsid w:val="00E91B0C"/>
                    <w:rsid w:val="00EA261D"/>
                    <w:rsid w:val="00EC2E03"/>
                    <w:rsid w:val="00F025A2"/>
                    <w:rsid w:val="00F11FEA"/>
                    <w:rsid w:val="00F16A3F"/>
                    <w:rsid w:val="00F41320"/>
                    <w:rsid w:val="00F50CFC"/>
                    <w:rsid w:val="00F57D46"/>
                    <w:rsid w:val="00FE18F1"/>
                    <w:rsid w:val="00FE35CB"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:doNotIncludeSubdocsInStats/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2049" fillcolor="#9cbee0" strokecolor="#739cc3">
                        <v:fill color="#9cbee0" color2="#bbd5f0" type="gradient">
                            <o:fill v:ext="view" type="gradientUnscaled"/>
                        </v:fill>
                        <v:stroke color="#739cc3" weight="1.25pt"/>
                    </o:shapedefaults>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="559E1451"/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{EAC12E4C-EC40-467D-B735-FA61318C9D50}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/item1.xml" pkg:contentType="application/xml" pkg:padding="32">
        <pkg:xmlData pkg:originalXmlStandalone="no">
            <b:Sources SelectedStyle="\APA.XSL" StyleName="APA"
                       xmlns:b="http://schemas.openxmlformats.org/officeDocument/2006/bibliography"
                       xmlns="http://schemas.openxmlformats.org/officeDocument/2006/bibliography"/>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/itemProps1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.customXmlProperties+xml" pkg:padding="32">
        <pkg:xmlData pkg:originalXmlStandalone="no">
            <ds:datastoreItem ds:itemID="{A04DB8EB-33DD-4A10-9F83-4D4BAF180D63}"
                              xmlns:ds="http://schemas.openxmlformats.org/officeDocument/2006/customXml">
                <ds:schemaRefs>
                    <ds:schemaRef ds:uri="http://schemas.openxmlformats.org/officeDocument/2006/bibliography"/>
                </ds:schemaRefs>
            </ds:datastoreItem>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/numbering.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
        <pkg:xmlData>
            <w:numbering xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:abstractNum w:abstractNumId="0" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="00000004"/>
                    <w:multiLevelType w:val="singleLevel"/>
                    <w:tmpl w:val="00000004"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="3"/>
                        <w:numFmt w:val="decimal"/>
                        <w:suff w:val="nothing"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="0" w:firstLine="0"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="1" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="66517F23"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="00000000"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="2">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="3">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="4">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="5">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="6">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="7">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                    <w:lvl w:ilvl="8">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val=""/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                </w:abstractNum>
                <w:num w:numId="1">
                    <w:abstractNumId w:val="1"/>
                </w:num>
                <w:num w:numId="2">
                    <w:abstractNumId w:val="0"/>
                    <w:lvlOverride w:ilvl="0">
                        <w:startOverride w:val="3"/>
                    </w:lvlOverride>
                </w:num>
            </w:numbering>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                      xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                      xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                      xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                      xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                      mc:Ignorable="w14 w15 w16se w16cid">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:ascii="Times New Roman" w:eastAsia="宋体" w:hAnsi="Times New Roman"
                                      w:cs="Times New Roman"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0"
                                w:defQFormat="0" w:count="377">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:uiPriority="0"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:uiPriority="0"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hashtag" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Unresolved Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link Error" w:semiHidden="1" w:unhideWhenUsed="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:kern w:val="2"/>
                        <w:sz w:val="21"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a3">
                    <w:name w:val="header"/>
                    <w:basedOn w:val="a"/>
                    <w:pPr>
                        <w:pBdr>
                            <w:top w:val="none" w:sz="0" w:space="1" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="4" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="1" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="4" w:color="auto"/>
                        </w:pBdr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a4">
                    <w:name w:val="footer"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a5"/>
                    <w:uiPriority w:val="99"/>
                    <w:pPr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:lang w:val="x-none" w:eastAsia="x-none"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a5">
                    <w:name w:val="页脚 字符"/>
                    <w:link w:val="a4"/>
                    <w:uiPriority w:val="99"/>
                    <w:rsid w:val="00DC1FC1"/>
                    <w:rPr>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a6">
                    <w:name w:val="Balloon Text"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a7"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="000A408A"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a7">
                    <w:name w:val="批注框文本 字符"/>
                    <w:link w:val="a6"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:rsid w:val="000A408A"/>
                    <w:rPr>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                           xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                           xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                           xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                           xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                           xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                           xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                           mc:Ignorable="w14 w15 w16se w16cid">
                <w:encoding w:val="x-cp20936"/>
                <w:optimizeForBrowser/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                     xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                     xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                     xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                     xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                     xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                     mc:Ignorable="w14 w15 w16se w16cid">
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋">
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线 Light">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线">
                    <w:altName w:val="DengXian"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml"
              pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:dcmitype="http://purl.org/dc/dcmitype/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title>“中华泰山”邮轮应聘人员登记表</dc:title>
                <dc:subject/>
                <dc:creator>Administrator</dc:creator>
                <cp:keywords/>
                <dc:description/>
                <cp:lastModifiedBy>夏 德航</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <cp:lastPrinted>2015-08-20T06:18:00Z</cp:lastPrinted>
                <dcterms:created xsi:type="dcterms:W3CDTF">2020-05-25T01:36:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2020-05-25T01:36:00Z</dcterms:modified>
                <cp:category/>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>0</TotalTime>
                <Pages>2</Pages>
                <Words>198</Words>
                <Characters>1132</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <PresentationFormat/>
                <Lines>9</Lines>
                <Paragraphs>2</Paragraphs>
                <Slides>0</Slides>
                <Notes>0</Notes>
                <HiddenSlides>0</HiddenSlides>
                <MMClips>0</MMClips>
                <ScaleCrop>false</ScaleCrop>
                <Manager/>
                <Company/>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>1328</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/custom.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.custom-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/custom-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <property fmtid="{D5CDD505-2E9C-101B-9397-08002B2CF9AE}" pid="2" name="KSOProductBuildVer">
                    <vt:lpwstr>2052-9.1.0.4405</vt:lpwstr>
                </property>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/customXml/_rels/item1.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXmlProps"
                              Target="itemProps1.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>