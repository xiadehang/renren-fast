<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml"
              pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:body>
                    <w:p w14:paraId="7B35899B" w14:textId="77777777" w:rsidR="007E622E" w:rsidRDefault="00180AB9"
                         w:rsidP="00180AB9">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="黑体"/>
                                <w:b/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="00E16958">
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="黑体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>社会船员</w:t>
                        </w:r>
                        <w:r w:rsidR="00C55E56" w:rsidRPr="00E16958">
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="黑体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>面试</w:t>
                        </w:r>
                        <w:r w:rsidR="00C411D3">
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="黑体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>及信息登记</w:t>
                        </w:r>
                        <w:r w:rsidRPr="00E16958">
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="黑体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="32"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>表</w:t>
                        </w:r>
                    </w:p>
                    <w:p w14:paraId="4FECFD7D" w14:textId="77777777" w:rsidR="007C7115" w:rsidRPr="00E16958"
                         w:rsidRDefault="00E85BAA" w:rsidP="00E85BAA">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:ind w:right="964"/>
                            <w:jc w:val="left"/>
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="黑体"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="黑体" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="24"/>
                                <w:szCs w:val="24"/>
                            </w:rPr>
                            <w:t xml:space="preserve">工号：      </w:t>
                        </w:r>
                    </w:p>
                    <w:tbl>
                        <w:tblPr>
                            <w:tblW w:w="9633" w:type="dxa"/>
                            <w:tblBorders>
                                <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            </w:tblBorders>
                            <w:tblLayout w:type="fixed"/>
                            <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0"
                                       w:noHBand="0" w:noVBand="1"/>
                        </w:tblPr>
                        <w:tblGrid>
                            <w:gridCol w:w="956"/>
                            <w:gridCol w:w="425"/>
                            <w:gridCol w:w="142"/>
                            <w:gridCol w:w="142"/>
                            <w:gridCol w:w="142"/>
                            <w:gridCol w:w="284"/>
                            <w:gridCol w:w="143"/>
                            <w:gridCol w:w="424"/>
                            <w:gridCol w:w="325"/>
                            <w:gridCol w:w="130"/>
                            <w:gridCol w:w="112"/>
                            <w:gridCol w:w="325"/>
                            <w:gridCol w:w="441"/>
                            <w:gridCol w:w="316"/>
                            <w:gridCol w:w="479"/>
                            <w:gridCol w:w="571"/>
                            <w:gridCol w:w="421"/>
                            <w:gridCol w:w="872"/>
                            <w:gridCol w:w="546"/>
                            <w:gridCol w:w="142"/>
                            <w:gridCol w:w="141"/>
                            <w:gridCol w:w="284"/>
                            <w:gridCol w:w="219"/>
                            <w:gridCol w:w="631"/>
                            <w:gridCol w:w="1020"/>
                        </w:tblGrid>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="56543D00" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="553"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="956" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="51C579C1" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EB5D5F">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>姓名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="851" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1FDCB6AF" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="00131C01" w:rsidP="003430AD">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00124E7E">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.crewName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="851" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="71591BAE" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>性别</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="892" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0287E6B0" w14:textId="25344644" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="00B34540" w:rsidP="00494316">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00124E7E">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${sex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="757" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="32A9A6CF" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="007E622E">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EB5D5F">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>民族</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="479" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="317127DF" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="992" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5D136A3B" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>出生地</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1560" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4CDF3991" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00C4310E"
                                     w:rsidRDefault="001E62B0" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.birthPlace}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1275" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2AB04B3D" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>拟派部门</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1020" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="48433BA6" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="420B4E3F" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="553"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1665" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="78B4FFD7" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>户口簿地址</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2642" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="52C7C8F3" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00C4310E"
                                     w:rsidRDefault="001E62B0" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.homeAddress}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1471" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5574F734" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>户口性质</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3855" w:type="dxa"/>
                                    <w:gridSpan w:val="8"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="033C796F" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00AE4A5C">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">农业户 □    </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>非</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>农业户 □</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="27F8F4BD" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="553"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1665" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="46C5A633" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>身份证号码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2642" w:type="dxa"/>
                                    <w:gridSpan w:val="10"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2785BECE" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="001C5461"
                                     w:rsidRDefault="001E62B0" w:rsidP="001734F7">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="001E62B0">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.documentNumber}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="50F5BB3A" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00C2467E"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00C2467E">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>现住址</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>及邮编</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="4276" w:type="dxa"/>
                                    <w:gridSpan w:val="9"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7E1C8A77" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="00494316" w:rsidP="001734F7">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                            <w:sz w:val="20"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00494316">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                            <w:sz w:val="20"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.homeAddress}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="6118296F" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="668"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1381" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="14D1B3BB" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00EB5D5F">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>出生</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1732" w:type="dxa"/>
                                    <w:gridSpan w:val="8"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4BE8C1D5" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="001C5461"
                                     w:rsidRDefault="00494316" w:rsidP="001734F7">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="007F7FDD">
                                        <w:rPr>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>${(crewInfo.birthday?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1194" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3B60EB5C" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00C2467E"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00C2467E">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>婚姻情况</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1050" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1032003E" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="003430AD">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1293" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="693FF980" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00C2467E"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>政治面貌</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2983" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="56539F6E" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="001C5461"
                                     w:rsidRDefault="0080232F" w:rsidP="007062B2">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>V群众/党员/预备党员/团员</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="0BDCFF9E" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="547"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="956" w:type="dxa"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="76FA656E" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>学历</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1135" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7F12328E" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="00651343" w:rsidP="007062B2">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.education}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1459" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="782A6CB1" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>毕业院校</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3100" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0E98ACED" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="008F0647" w:rsidP="007062B2">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="008F0647">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.school}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1332" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="122EC5DC" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>学习专业</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1651" w:type="dxa"/>
                                    <w:gridSpan w:val="2"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="27694085" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="00651343" w:rsidP="007062B2">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.major}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="60D9ED44" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="547"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1523" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="384706F2" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>身体状况</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="8110" w:type="dxa"/>
                                    <w:gridSpan w:val="22"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="17E254BF" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="000405BE">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>身高：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="000405BE">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> cm</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">体重 </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="000405BE">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidR="000405BE">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">   </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>kg</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">     鞋号：</w:t>
                                    </w:r>
                                    <w:r w:rsidR="000405BE">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  码</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="377850A1" w14:textId="77777777"
                              w:rsidTr="000E72A8">
                            <w:trPr>
                                <w:trHeight w:val="547"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2234" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2370274A" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00AE3069"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>申请职务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2552" w:type="dxa"/>
                                    <w:gridSpan w:val="8"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="20A016B4" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="000405BE" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2410" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3893C28B" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>推荐</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>单位</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>（分包方）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2437" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="179B2F75" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="000405BE" w:rsidP="000A1CEC">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="76EE3227" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="547"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="3225" w:type="dxa"/>
                                    <w:gridSpan w:val="11"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1DBA0203" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>家人联系电话及与本人关系</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2553" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="03EBF88C" w14:textId="77777777" w:rsidR="00494316" w:rsidRPr="00494316"
                                     w:rsidRDefault="00494316" w:rsidP="00494316">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00494316">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.emergencyContact}</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="0EAAC2F7" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="00494316" w:rsidP="00494316">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00494316">
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.relationship}</w:t>
                                    </w:r>
                                    <w:r w:rsidR="000405BE">
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1701" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="480648BC" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00E85BAA">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>本人</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00C2467E">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>联系电话</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2154" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="34212752" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="00651343" w:rsidP="00651343">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:b/>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t>${crewInfo.phoneNumber}</w:t>
                                    </w:r>
                                    <w:r w:rsidR="000405BE">
                                        <w:rPr>
                                            <w:rFonts w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="4596F7FB" w14:textId="77777777"
                              w:rsidTr="00C411D3">
                            <w:trPr>
                                <w:trHeight w:val="547"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="9633" w:type="dxa"/>
                                    <w:gridSpan w:val="25"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2B06F94E" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>最近三年船舶任职资历</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="4DB1267B" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="585"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1523" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4BB43F6F" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00EB5D5F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>起始日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1460" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5CF26AB3" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>终止日期</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1008" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5AF44C03" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>职务</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1787" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6914FA03" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00BE5D17">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>船名</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="64C826A5" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00BE5D17">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>船东</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1870" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6D7F74CF" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>GRT/</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>KW</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <#list experienceList as experience>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="285C03DB" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="429"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1523" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="201E0B23" w14:textId="77EA6852" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="000343A4" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${(experience.startTime?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1460" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3B9DF802" w14:textId="173AF120" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="000343A4" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${(experience.endTime?string("yyyy-MM-dd"))!}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1008" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="36404AB4" w14:textId="4B4121D4" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="000343A4" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.crewPostex}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1787" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="5B7EBD65" w14:textId="40A376EF" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="000343A4" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Ђˎ̥" w:hAnsi="Ђˎ̥" w:cs="宋体"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.vesselName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2834BC03" w14:textId="08F43681" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="000343A4" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00651343">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>${experience.shipownerName}</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1870" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="0A1D7D49" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0"/>
                            </w:tc>
                        </w:tr>
                        </#list>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="292B3DF2" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="279"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1523" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6D0CB415" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1460" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1B658D6E" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1008" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="21EF7BEC" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00957F93">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1787" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7B71C0BB" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="Ђˎ̥" w:hAnsi="Ђˎ̥" w:cs="宋体"/>
                                            <w:sz w:val="18"/>
                                            <w:szCs w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="55FBC862" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="007062B2">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1870" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="37433A95" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="auto"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="方正大标宋繁体" w:eastAsia="方正大标宋繁体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="46613A58" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="370"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1523" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="7AF44241" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1460" w:type="dxa"/>
                                    <w:gridSpan w:val="6"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="079EB38C" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1008" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6E5AC61E" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:adjustRightInd w:val="0"/>
                                        <w:snapToGrid w:val="0"/>
                                        <w:spacing w:line="220" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:sz w:val="18"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1787" w:type="dxa"/>
                                    <w:gridSpan w:val="4"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="2B6F58F2" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:rPr>
                                            <w:kern w:val="1"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1985" w:type="dxa"/>
                                    <w:gridSpan w:val="5"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="69B4130E" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00F962E1"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="1870" w:type="dxa"/>
                                    <w:gridSpan w:val="3"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="3C4BE371" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="008C75B0"
                                     w:rsidRDefault="0080232F" w:rsidP="00187FD0">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="auto"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="方正大标宋繁体" w:eastAsia="方正大标宋繁体"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="14DA6C0D" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="600"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2234" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vMerge w:val="restart"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1B082913" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t>现持证书情况</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7399" w:type="dxa"/>
                                    <w:gridSpan w:val="18"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="231C6EFC" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00591F1A"
                                     w:rsidRDefault="0080232F" w:rsidP="001C5461">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>海员证</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>护照</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>油</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">轮TO1 </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>油</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">轮TO2 </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00F962E1">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>化</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>学品</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>LPG</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="5CAFFA14" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="675"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2234" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vMerge/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="1A6FF289" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7399" w:type="dxa"/>
                                    <w:gridSpan w:val="18"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="53B83401" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00BF736B"
                                     w:rsidRDefault="0080232F" w:rsidP="00642509">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>水手</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>证</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00591F1A">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>机工</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">证 </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00591F1A">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">  厨师证书 </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>海事局</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>海船船员健康</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>证书</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="00BF736B">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:szCs w:val="21"/>
                                        </w:rPr>
                                        <w:t>□</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="0519BE6E" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="1305"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2234" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="6619CACC" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="000A7500"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:cs="仿宋_GB2312"/>
                                            <w:b/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="000A7500">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:cs="仿宋_GB2312"
                                                      w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>船员资源</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:cs="仿宋_GB2312"
                                                      w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>部</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="000A7500">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:cs="仿宋_GB2312"
                                                      w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>意见</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="0685E5C8" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="000A7500"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="000A7500">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:cs="仿宋_GB2312"
                                                      w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:kern w:val="0"/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>（初审）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7399" w:type="dxa"/>
                                    <w:gridSpan w:val="18"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="59CCCBD3" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="007062B2">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                                      </w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="20B430E6" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="007062B2">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="3BC4B9D4" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00E16958"
                                     w:rsidRDefault="0080232F" w:rsidP="0043530B">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                                        签名:</w:t>
                                    </w:r>
                                    <w:r w:rsidR="0043530B" w:rsidRPr="00E16958">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve"> </w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="3B12FC75" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="1079"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2234" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="47AB2F38" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="000A7500"
                                     w:rsidRDefault="0080232F" w:rsidP="00C40568">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="000A7500">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>面试人员意见</w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="0D3F4619" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="000A7500"
                                     w:rsidRDefault="0080232F" w:rsidP="00C40568">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r w:rsidRPr="000A7500">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>（复审）</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7399" w:type="dxa"/>
                                    <w:gridSpan w:val="18"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="4F70DCE3" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">      </w:t>
                                    </w:r>
                                </w:p>
                                <w:p w14:paraId="37D6C5F5" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="45752B82" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00E16958"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                     签名:</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                        <w:tr w:rsidR="0080232F" w:rsidRPr="00BF736B" w14:paraId="5A6FAA43" w14:textId="77777777"
                              w:rsidTr="0080232F">
                            <w:trPr>
                                <w:trHeight w:val="1245"/>
                            </w:trPr>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="2234" w:type="dxa"/>
                                    <w:gridSpan w:val="7"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="626BF35D" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="000A7500"
                                     w:rsidRDefault="0080232F" w:rsidP="00C40568">
                                    <w:pPr>
                                        <w:widowControl/>
                                        <w:spacing w:line="400" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>各库</w:t>
                                    </w:r>
                                    <w:r w:rsidRPr="000A7500">
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>经理</w:t>
                                    </w:r>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                        </w:rPr>
                                        <w:t>意见</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                            <w:tc>
                                <w:tcPr>
                                    <w:tcW w:w="7399" w:type="dxa"/>
                                    <w:gridSpan w:val="18"/>
                                    <w:vAlign w:val="center"/>
                                </w:tcPr>
                                <w:p w14:paraId="62640EF9" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="339F5EA5" w14:textId="77777777" w:rsidR="0080232F"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                    </w:pPr>
                                </w:p>
                                <w:p w14:paraId="03FEA0E9" w14:textId="77777777" w:rsidR="0080232F" w:rsidRPr="00E16958"
                                     w:rsidRDefault="0080232F" w:rsidP="00B96435">
                                    <w:pPr>
                                        <w:spacing w:line="360" w:lineRule="exact"/>
                                        <w:jc w:val="center"/>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                            <w:b/>
                                            <w:sz w:val="28"/>
                                            <w:szCs w:val="28"/>
                                            <w:u w:val="single"/>
                                        </w:rPr>
                                    </w:pPr>
                                    <w:r>
                                        <w:rPr>
                                            <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                            <w:b/>
                                            <w:sz w:val="24"/>
                                            <w:szCs w:val="24"/>
                                        </w:rPr>
                                        <w:t xml:space="preserve">                     签名:</w:t>
                                    </w:r>
                                </w:p>
                            </w:tc>
                        </w:tr>
                    </w:tbl>
                    <w:p w14:paraId="08E3BF35" w14:textId="77777777" w:rsidR="00E16958" w:rsidRDefault="00E16958"
                         w:rsidP="00D37879">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:ind w:firstLineChars="50" w:firstLine="141"/>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:p w14:paraId="242B1931" w14:textId="77777777" w:rsidR="00180AB9" w:rsidRPr="000A7500"
                         w:rsidRDefault="0000762D" w:rsidP="00D37879">
                        <w:pPr>
                            <w:spacing w:line="360" w:lineRule="exact"/>
                            <w:ind w:firstLineChars="50" w:firstLine="141"/>
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r w:rsidRPr="000A7500">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">                                  日期：</w:t>
                        </w:r>
                        <w:r w:rsidR="003E06CC">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidR="0043530B">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">    </w:t>
                        </w:r>
                        <w:r w:rsidR="003E06CC">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="000A7500">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>年</w:t>
                        </w:r>
                        <w:r w:rsidR="003E06CC">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidR="0043530B">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">   </w:t>
                        </w:r>
                        <w:r w:rsidR="003E06CC">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidRPr="000A7500">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>月</w:t>
                        </w:r>
                        <w:r w:rsidR="00C20DDE">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve"> </w:t>
                        </w:r>
                        <w:r w:rsidR="0043530B">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t xml:space="preserve">    </w:t>
                        </w:r>
                        <w:r w:rsidRPr="000A7500">
                            <w:rPr>
                                <w:rFonts w:ascii="仿宋" w:eastAsia="仿宋" w:hAnsi="仿宋" w:hint="eastAsia"/>
                                <w:b/>
                                <w:sz w:val="28"/>
                                <w:szCs w:val="28"/>
                            </w:rPr>
                            <w:t>日</w:t>
                        </w:r>
                    </w:p>
                    <w:sectPr w:rsidR="00180AB9" w:rsidRPr="000A7500" w:rsidSect="00E3396D">
                        <w:headerReference w:type="default" r:id="rId7"/>
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="907" w:right="1247" w:bottom="907" w:left="1247" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="425"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId8"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId7"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/header"
                              Target="header1.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"
                              Target="numbering.xml"/>
                <Relationship Id="rId6"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"
                              Target="endnotes.xml"/>
                <Relationship Id="rId5"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"
                              Target="footnotes.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
                              Target="webSettings.xml"/>
                <Relationship Id="rId9" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/footnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml">
        <pkg:xmlData>
            <w:footnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:footnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="54583521" w14:textId="77777777" w:rsidR="00512929" w:rsidRDefault="00512929"
                         w:rsidP="002F2947">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:footnote>
                <w:footnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="7C16056C" w14:textId="77777777" w:rsidR="00512929" w:rsidRDefault="00512929"
                         w:rsidP="002F2947">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:footnote>
            </w:footnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/endnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml">
        <pkg:xmlData>
            <w:endnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:endnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="302BC2C9" w14:textId="77777777" w:rsidR="00512929" w:rsidRDefault="00512929"
                         w:rsidP="002F2947">
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:endnote>
                <w:endnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="37634849" w14:textId="77777777" w:rsidR="00512929" w:rsidRDefault="00512929"
                         w:rsidP="002F2947">
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:endnote>
            </w:endnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/header1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.header+xml">
        <pkg:xmlData>
            <w:hdr xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                   xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                   xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                   xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                   xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                   xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                   xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                   xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                   xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                   xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                   xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                   xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                   xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                   xmlns:o="urn:schemas-microsoft-com:office:office"
                   xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                   xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                   xmlns:v="urn:schemas-microsoft-com:vml"
                   xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                   xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                   xmlns:w10="urn:schemas-microsoft-com:office:word"
                   xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                   xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                   xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                   xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                   xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                   xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                   xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                   xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                   xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                   mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:p w14:paraId="4B0E3DC1" w14:textId="77777777" w:rsidR="00470F67" w:rsidRDefault="00470F67"
                     w:rsidP="003901DE">
                    <w:pPr>
                        <w:pStyle w:val="a3"/>
                        <w:pBdr>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:pBdr>
                    </w:pPr>
                </w:p>
            </w:hdr>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题​​">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="CAEACE"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4472C4"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="等线 Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游ゴシック Light"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线 Light"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="等线" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main"
                                           name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}"
                                           vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"/>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main"
                        mc:Ignorable="w14 w15 w16se w16cid">
                <w:zoom w:percent="100"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:doNotTrackMoves/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridHorizontalSpacing w:val="105"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:hdrShapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2049"/>
                </w:hdrShapeDefaults>
                <w:footnotePr>
                    <w:footnote w:id="-1"/>
                    <w:footnote w:id="0"/>
                </w:footnotePr>
                <w:endnotePr>
                    <w:endnote w:id="-1"/>
                    <w:endnote w:id="0"/>
                </w:endnotePr>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:useNormalStyleForList/>
                    <w:doNotUseIndentAsNumberingTabStop/>
                    <w:useAltKinsokuLineBreakRules/>
                    <w:allowSpaceOfSameStyleInTable/>
                    <w:doNotSuppressIndentation/>
                    <w:doNotAutofitConstrainedTables/>
                    <w:autofitToFirstFixedWidthCell/>
                    <w:displayHangulFixedWidth/>
                    <w:splitPgBreakAndParaMark/>
                    <w:doNotVertAlignCellWithSp/>
                    <w:doNotBreakConstrainedForcedTable/>
                    <w:doNotVertAlignInTxbx/>
                    <w:useAnsiKerningPairs/>
                    <w:cachedColBalance/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="11"/>
                    <w:compatSetting w:name="allowHyphenationAtTrackBottom"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="useWord2013TrackBottomHyphenation"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="00983481"/>
                    <w:rsid w:val="00000D4B"/>
                    <w:rsid w:val="00003458"/>
                    <w:rsid w:val="00003684"/>
                    <w:rsid w:val="0000762D"/>
                    <w:rsid w:val="0001119D"/>
                    <w:rsid w:val="0001189C"/>
                    <w:rsid w:val="000213EB"/>
                    <w:rsid w:val="00023A6C"/>
                    <w:rsid w:val="00025873"/>
                    <w:rsid w:val="00031ACA"/>
                    <w:rsid w:val="000343A4"/>
                    <w:rsid w:val="00034533"/>
                    <w:rsid w:val="0003513F"/>
                    <w:rsid w:val="000405BE"/>
                    <w:rsid w:val="00043C13"/>
                    <w:rsid w:val="00044D44"/>
                    <w:rsid w:val="000506EB"/>
                    <w:rsid w:val="000618F8"/>
                    <w:rsid w:val="00064C26"/>
                    <w:rsid w:val="0007259D"/>
                    <w:rsid w:val="00073AA0"/>
                    <w:rsid w:val="0007502F"/>
                    <w:rsid w:val="00076089"/>
                    <w:rsid w:val="000814B6"/>
                    <w:rsid w:val="00081816"/>
                    <w:rsid w:val="00081DFE"/>
                    <w:rsid w:val="00084BFA"/>
                    <w:rsid w:val="00086B2F"/>
                    <w:rsid w:val="000909B5"/>
                    <w:rsid w:val="000A1CEC"/>
                    <w:rsid w:val="000A7189"/>
                    <w:rsid w:val="000A7500"/>
                    <w:rsid w:val="000B598A"/>
                    <w:rsid w:val="000C63FD"/>
                    <w:rsid w:val="000C662D"/>
                    <w:rsid w:val="000D171A"/>
                    <w:rsid w:val="000D7E0F"/>
                    <w:rsid w:val="000E72A8"/>
                    <w:rsid w:val="000F720C"/>
                    <w:rsid w:val="00104364"/>
                    <w:rsid w:val="00104E04"/>
                    <w:rsid w:val="00106F35"/>
                    <w:rsid w:val="001077F9"/>
                    <w:rsid w:val="00112CBB"/>
                    <w:rsid w:val="00116FE1"/>
                    <w:rsid w:val="00121A0B"/>
                    <w:rsid w:val="00126FB4"/>
                    <w:rsid w:val="00131C01"/>
                    <w:rsid w:val="001346F9"/>
                    <w:rsid w:val="001401B6"/>
                    <w:rsid w:val="0014291B"/>
                    <w:rsid w:val="00143495"/>
                    <w:rsid w:val="00150BBF"/>
                    <w:rsid w:val="00163A2B"/>
                    <w:rsid w:val="00172FEB"/>
                    <w:rsid w:val="001734F7"/>
                    <w:rsid w:val="001764C4"/>
                    <w:rsid w:val="0017682D"/>
                    <w:rsid w:val="00180AB9"/>
                    <w:rsid w:val="001838CE"/>
                    <w:rsid w:val="00183BF2"/>
                    <w:rsid w:val="00185BF4"/>
                    <w:rsid w:val="00187FD0"/>
                    <w:rsid w:val="00191843"/>
                    <w:rsid w:val="0019467C"/>
                    <w:rsid w:val="001A1A81"/>
                    <w:rsid w:val="001A40D3"/>
                    <w:rsid w:val="001B6F6E"/>
                    <w:rsid w:val="001B77C1"/>
                    <w:rsid w:val="001C0B65"/>
                    <w:rsid w:val="001C0C24"/>
                    <w:rsid w:val="001C4CA9"/>
                    <w:rsid w:val="001C5461"/>
                    <w:rsid w:val="001C7DF7"/>
                    <w:rsid w:val="001D4B52"/>
                    <w:rsid w:val="001D7453"/>
                    <w:rsid w:val="001E19FF"/>
                    <w:rsid w:val="001E4D47"/>
                    <w:rsid w:val="001E5B7D"/>
                    <w:rsid w:val="001E62B0"/>
                    <w:rsid w:val="001F030A"/>
                    <w:rsid w:val="001F5E6B"/>
                    <w:rsid w:val="001F7853"/>
                    <w:rsid w:val="001F79F6"/>
                    <w:rsid w:val="0020360E"/>
                    <w:rsid w:val="002046FC"/>
                    <w:rsid w:val="002120FA"/>
                    <w:rsid w:val="0022493F"/>
                    <w:rsid w:val="00224B4F"/>
                    <w:rsid w:val="00233207"/>
                    <w:rsid w:val="00246349"/>
                    <w:rsid w:val="00247575"/>
                    <w:rsid w:val="002524C2"/>
                    <w:rsid w:val="00254D24"/>
                    <w:rsid w:val="00261165"/>
                    <w:rsid w:val="002633A2"/>
                    <w:rsid w:val="00263C3D"/>
                    <w:rsid w:val="002703AE"/>
                    <w:rsid w:val="00270845"/>
                    <w:rsid w:val="00277542"/>
                    <w:rsid w:val="0028040F"/>
                    <w:rsid w:val="002817D2"/>
                    <w:rsid w:val="00287FB6"/>
                    <w:rsid w:val="0029530F"/>
                    <w:rsid w:val="00295AF5"/>
                    <w:rsid w:val="002A1DF8"/>
                    <w:rsid w:val="002A4E23"/>
                    <w:rsid w:val="002C3574"/>
                    <w:rsid w:val="002C4428"/>
                    <w:rsid w:val="002C6777"/>
                    <w:rsid w:val="002D4790"/>
                    <w:rsid w:val="002D67D6"/>
                    <w:rsid w:val="002F2947"/>
                    <w:rsid w:val="002F51C6"/>
                    <w:rsid w:val="00300FFB"/>
                    <w:rsid w:val="003022F6"/>
                    <w:rsid w:val="0030478D"/>
                    <w:rsid w:val="00305E12"/>
                    <w:rsid w:val="0031612F"/>
                    <w:rsid w:val="00320953"/>
                    <w:rsid w:val="00320D50"/>
                    <w:rsid w:val="00321926"/>
                    <w:rsid w:val="0032332D"/>
                    <w:rsid w:val="0032724D"/>
                    <w:rsid w:val="00330324"/>
                    <w:rsid w:val="003347FE"/>
                    <w:rsid w:val="00335828"/>
                    <w:rsid w:val="00340D58"/>
                    <w:rsid w:val="00341167"/>
                    <w:rsid w:val="003430AD"/>
                    <w:rsid w:val="00346890"/>
                    <w:rsid w:val="003527CA"/>
                    <w:rsid w:val="00357891"/>
                    <w:rsid w:val="00370A45"/>
                    <w:rsid w:val="00370C71"/>
                    <w:rsid w:val="003726C9"/>
                    <w:rsid w:val="003901DE"/>
                    <w:rsid w:val="003A56F1"/>
                    <w:rsid w:val="003A5D5E"/>
                    <w:rsid w:val="003B25E6"/>
                    <w:rsid w:val="003B69B4"/>
                    <w:rsid w:val="003B6D3C"/>
                    <w:rsid w:val="003B6DA5"/>
                    <w:rsid w:val="003D2696"/>
                    <w:rsid w:val="003D3BAF"/>
                    <w:rsid w:val="003D785D"/>
                    <w:rsid w:val="003E06CC"/>
                    <w:rsid w:val="003E4644"/>
                    <w:rsid w:val="003E6395"/>
                    <w:rsid w:val="003F2E5B"/>
                    <w:rsid w:val="003F393D"/>
                    <w:rsid w:val="003F4F3D"/>
                    <w:rsid w:val="003F53D6"/>
                    <w:rsid w:val="0040565F"/>
                    <w:rsid w:val="004132DC"/>
                    <w:rsid w:val="00417274"/>
                    <w:rsid w:val="0042149B"/>
                    <w:rsid w:val="0042547C"/>
                    <w:rsid w:val="00426E4C"/>
                    <w:rsid w:val="00432A0F"/>
                    <w:rsid w:val="00434834"/>
                    <w:rsid w:val="0043530B"/>
                    <w:rsid w:val="00435CF5"/>
                    <w:rsid w:val="00444629"/>
                    <w:rsid w:val="00451DE0"/>
                    <w:rsid w:val="004607A3"/>
                    <w:rsid w:val="00462506"/>
                    <w:rsid w:val="0046542D"/>
                    <w:rsid w:val="004659E3"/>
                    <w:rsid w:val="00470F67"/>
                    <w:rsid w:val="0047460C"/>
                    <w:rsid w:val="004749A5"/>
                    <w:rsid w:val="004764CF"/>
                    <w:rsid w:val="0047654E"/>
                    <w:rsid w:val="00494316"/>
                    <w:rsid w:val="00495FD9"/>
                    <w:rsid w:val="00497787"/>
                    <w:rsid w:val="004A19B3"/>
                    <w:rsid w:val="004A55A1"/>
                    <w:rsid w:val="004A5F0F"/>
                    <w:rsid w:val="004B08C9"/>
                    <w:rsid w:val="004B2187"/>
                    <w:rsid w:val="004B7252"/>
                    <w:rsid w:val="004D260D"/>
                    <w:rsid w:val="004D3751"/>
                    <w:rsid w:val="004E6181"/>
                    <w:rsid w:val="004F049F"/>
                    <w:rsid w:val="004F1591"/>
                    <w:rsid w:val="004F186E"/>
                    <w:rsid w:val="004F7607"/>
                    <w:rsid w:val="004F76B4"/>
                    <w:rsid w:val="00502687"/>
                    <w:rsid w:val="00506C11"/>
                    <w:rsid w:val="00512929"/>
                    <w:rsid w:val="0051443B"/>
                    <w:rsid w:val="00524FAF"/>
                    <w:rsid w:val="00531527"/>
                    <w:rsid w:val="005327E9"/>
                    <w:rsid w:val="00535E72"/>
                    <w:rsid w:val="00546075"/>
                    <w:rsid w:val="005512E9"/>
                    <w:rsid w:val="00555153"/>
                    <w:rsid w:val="0055581D"/>
                    <w:rsid w:val="00560FA4"/>
                    <w:rsid w:val="00572A31"/>
                    <w:rsid w:val="00573F17"/>
                    <w:rsid w:val="005759AB"/>
                    <w:rsid w:val="00575F86"/>
                    <w:rsid w:val="00576D3C"/>
                    <w:rsid w:val="00591F1A"/>
                    <w:rsid w:val="005965D8"/>
                    <w:rsid w:val="005A39F0"/>
                    <w:rsid w:val="005A6193"/>
                    <w:rsid w:val="005A7D3F"/>
                    <w:rsid w:val="005B1969"/>
                    <w:rsid w:val="005B3213"/>
                    <w:rsid w:val="005B40B3"/>
                    <w:rsid w:val="005D1212"/>
                    <w:rsid w:val="005D5F65"/>
                    <w:rsid w:val="005E26B3"/>
                    <w:rsid w:val="005F0DA7"/>
                    <w:rsid w:val="005F3106"/>
                    <w:rsid w:val="0060196C"/>
                    <w:rsid w:val="00601B13"/>
                    <w:rsid w:val="00604B37"/>
                    <w:rsid w:val="00612E2D"/>
                    <w:rsid w:val="00614205"/>
                    <w:rsid w:val="006157A4"/>
                    <w:rsid w:val="006214D0"/>
                    <w:rsid w:val="00632B03"/>
                    <w:rsid w:val="00637FAA"/>
                    <w:rsid w:val="00642509"/>
                    <w:rsid w:val="00643B0C"/>
                    <w:rsid w:val="00646E3E"/>
                    <w:rsid w:val="00647C63"/>
                    <w:rsid w:val="00651343"/>
                    <w:rsid w:val="0065476B"/>
                    <w:rsid w:val="00660262"/>
                    <w:rsid w:val="00662FF7"/>
                    <w:rsid w:val="00673538"/>
                    <w:rsid w:val="006746C3"/>
                    <w:rsid w:val="006747E9"/>
                    <w:rsid w:val="00674DB9"/>
                    <w:rsid w:val="006761B0"/>
                    <w:rsid w:val="006806A3"/>
                    <w:rsid w:val="006818BE"/>
                    <w:rsid w:val="00693A3D"/>
                    <w:rsid w:val="00697AE1"/>
                    <w:rsid w:val="006A0D61"/>
                    <w:rsid w:val="006A411E"/>
                    <w:rsid w:val="006B50EB"/>
                    <w:rsid w:val="006C06EB"/>
                    <w:rsid w:val="006C1681"/>
                    <w:rsid w:val="006C6BCF"/>
                    <w:rsid w:val="006D6751"/>
                    <w:rsid w:val="006E3E88"/>
                    <w:rsid w:val="006E6583"/>
                    <w:rsid w:val="006F4930"/>
                    <w:rsid w:val="006F57B7"/>
                    <w:rsid w:val="006F6B50"/>
                    <w:rsid w:val="007062B2"/>
                    <w:rsid w:val="00707B73"/>
                    <w:rsid w:val="00710E25"/>
                    <w:rsid w:val="00717E4D"/>
                    <w:rsid w:val="00717FF9"/>
                    <w:rsid w:val="00726D57"/>
                    <w:rsid w:val="00727C02"/>
                    <w:rsid w:val="00744805"/>
                    <w:rsid w:val="0075150F"/>
                    <w:rsid w:val="00752A06"/>
                    <w:rsid w:val="00756FB2"/>
                    <w:rsid w:val="0075796A"/>
                    <w:rsid w:val="0076171D"/>
                    <w:rsid w:val="007633E1"/>
                    <w:rsid w:val="00763FE7"/>
                    <w:rsid w:val="00764534"/>
                    <w:rsid w:val="00773C5E"/>
                    <w:rsid w:val="00776ED6"/>
                    <w:rsid w:val="00783760"/>
                    <w:rsid w:val="00785D1C"/>
                    <w:rsid w:val="0078734B"/>
                    <w:rsid w:val="007906D6"/>
                    <w:rsid w:val="00790E9C"/>
                    <w:rsid w:val="00790ED0"/>
                    <w:rsid w:val="007A3A2C"/>
                    <w:rsid w:val="007A6136"/>
                    <w:rsid w:val="007B55CF"/>
                    <w:rsid w:val="007B6B70"/>
                    <w:rsid w:val="007C127F"/>
                    <w:rsid w:val="007C7115"/>
                    <w:rsid w:val="007D1075"/>
                    <w:rsid w:val="007E622E"/>
                    <w:rsid w:val="007F0D49"/>
                    <w:rsid w:val="0080232F"/>
                    <w:rsid w:val="0081336B"/>
                    <w:rsid w:val="008142B5"/>
                    <w:rsid w:val="00815754"/>
                    <w:rsid w:val="00824346"/>
                    <w:rsid w:val="008328FF"/>
                    <w:rsid w:val="0083439F"/>
                    <w:rsid w:val="00834855"/>
                    <w:rsid w:val="00840AA0"/>
                    <w:rsid w:val="00841D8A"/>
                    <w:rsid w:val="0084223A"/>
                    <w:rsid w:val="00842312"/>
                    <w:rsid w:val="00850174"/>
                    <w:rsid w:val="008629B7"/>
                    <w:rsid w:val="0087451D"/>
                    <w:rsid w:val="00874B77"/>
                    <w:rsid w:val="00877FB9"/>
                    <w:rsid w:val="00884FCA"/>
                    <w:rsid w:val="008933CA"/>
                    <w:rsid w:val="008B08DE"/>
                    <w:rsid w:val="008B0A96"/>
                    <w:rsid w:val="008B114E"/>
                    <w:rsid w:val="008B6BD4"/>
                    <w:rsid w:val="008C12E7"/>
                    <w:rsid w:val="008C3E0E"/>
                    <w:rsid w:val="008C474D"/>
                    <w:rsid w:val="008D09A5"/>
                    <w:rsid w:val="008D21A8"/>
                    <w:rsid w:val="008D23BA"/>
                    <w:rsid w:val="008D2A6F"/>
                    <w:rsid w:val="008D60F2"/>
                    <w:rsid w:val="008E1263"/>
                    <w:rsid w:val="008E42CD"/>
                    <w:rsid w:val="008E7E1E"/>
                    <w:rsid w:val="008F0647"/>
                    <w:rsid w:val="008F39DB"/>
                    <w:rsid w:val="00901FBD"/>
                    <w:rsid w:val="00910584"/>
                    <w:rsid w:val="009179E6"/>
                    <w:rsid w:val="0092364E"/>
                    <w:rsid w:val="00925F71"/>
                    <w:rsid w:val="00927E6E"/>
                    <w:rsid w:val="00930DA9"/>
                    <w:rsid w:val="00932559"/>
                    <w:rsid w:val="0095183F"/>
                    <w:rsid w:val="0095252E"/>
                    <w:rsid w:val="00952562"/>
                    <w:rsid w:val="00954498"/>
                    <w:rsid w:val="00957F93"/>
                    <w:rsid w:val="00966635"/>
                    <w:rsid w:val="00970931"/>
                    <w:rsid w:val="00973D40"/>
                    <w:rsid w:val="009766AB"/>
                    <w:rsid w:val="00981D9D"/>
                    <w:rsid w:val="00983481"/>
                    <w:rsid w:val="00984607"/>
                    <w:rsid w:val="009869F2"/>
                    <w:rsid w:val="00986E44"/>
                    <w:rsid w:val="00992F05"/>
                    <w:rsid w:val="00993BAA"/>
                    <w:rsid w:val="009973B8"/>
                    <w:rsid w:val="009A3159"/>
                    <w:rsid w:val="009A3B43"/>
                    <w:rsid w:val="009B0DEE"/>
                    <w:rsid w:val="009B40C7"/>
                    <w:rsid w:val="009B7DC0"/>
                    <w:rsid w:val="009C0315"/>
                    <w:rsid w:val="009C438C"/>
                    <w:rsid w:val="009C719C"/>
                    <w:rsid w:val="009D2576"/>
                    <w:rsid w:val="009E1762"/>
                    <w:rsid w:val="009E242B"/>
                    <w:rsid w:val="009F178E"/>
                    <w:rsid w:val="00A03160"/>
                    <w:rsid w:val="00A07CDE"/>
                    <w:rsid w:val="00A07DEC"/>
                    <w:rsid w:val="00A128F8"/>
                    <w:rsid w:val="00A14CE2"/>
                    <w:rsid w:val="00A169A7"/>
                    <w:rsid w:val="00A3176F"/>
                    <w:rsid w:val="00A41882"/>
                    <w:rsid w:val="00A51A28"/>
                    <w:rsid w:val="00A523FD"/>
                    <w:rsid w:val="00A54D93"/>
                    <w:rsid w:val="00A60D18"/>
                    <w:rsid w:val="00A61DED"/>
                    <w:rsid w:val="00A627A0"/>
                    <w:rsid w:val="00A636D1"/>
                    <w:rsid w:val="00A675DA"/>
                    <w:rsid w:val="00A73174"/>
                    <w:rsid w:val="00A741BF"/>
                    <w:rsid w:val="00A80527"/>
                    <w:rsid w:val="00A84A2F"/>
                    <w:rsid w:val="00A84CC9"/>
                    <w:rsid w:val="00A86E4C"/>
                    <w:rsid w:val="00A9334E"/>
                    <w:rsid w:val="00AA1FD4"/>
                    <w:rsid w:val="00AB69D3"/>
                    <w:rsid w:val="00AC5574"/>
                    <w:rsid w:val="00AD4F0B"/>
                    <w:rsid w:val="00AD6178"/>
                    <w:rsid w:val="00AE3069"/>
                    <w:rsid w:val="00AE4A5C"/>
                    <w:rsid w:val="00B019D6"/>
                    <w:rsid w:val="00B03AE0"/>
                    <w:rsid w:val="00B04A48"/>
                    <w:rsid w:val="00B137A3"/>
                    <w:rsid w:val="00B1441E"/>
                    <w:rsid w:val="00B21084"/>
                    <w:rsid w:val="00B32F32"/>
                    <w:rsid w:val="00B34540"/>
                    <w:rsid w:val="00B36BFD"/>
                    <w:rsid w:val="00B376C9"/>
                    <w:rsid w:val="00B43890"/>
                    <w:rsid w:val="00B449FB"/>
                    <w:rsid w:val="00B51B67"/>
                    <w:rsid w:val="00B544F8"/>
                    <w:rsid w:val="00B54C0C"/>
                    <w:rsid w:val="00B60107"/>
                    <w:rsid w:val="00B62106"/>
                    <w:rsid w:val="00B627EB"/>
                    <w:rsid w:val="00B662B6"/>
                    <w:rsid w:val="00B72742"/>
                    <w:rsid w:val="00B7738B"/>
                    <w:rsid w:val="00B92611"/>
                    <w:rsid w:val="00B9550B"/>
                    <w:rsid w:val="00B96435"/>
                    <w:rsid w:val="00BB2F18"/>
                    <w:rsid w:val="00BB6D1C"/>
                    <w:rsid w:val="00BC515F"/>
                    <w:rsid w:val="00BC54DE"/>
                    <w:rsid w:val="00BC7524"/>
                    <w:rsid w:val="00BD088A"/>
                    <w:rsid w:val="00BE1D3D"/>
                    <w:rsid w:val="00BE39ED"/>
                    <w:rsid w:val="00BE5D17"/>
                    <w:rsid w:val="00BE6CD3"/>
                    <w:rsid w:val="00BF269A"/>
                    <w:rsid w:val="00BF52ED"/>
                    <w:rsid w:val="00BF5561"/>
                    <w:rsid w:val="00BF736B"/>
                    <w:rsid w:val="00C0019E"/>
                    <w:rsid w:val="00C04D30"/>
                    <w:rsid w:val="00C11835"/>
                    <w:rsid w:val="00C15ACC"/>
                    <w:rsid w:val="00C15BF2"/>
                    <w:rsid w:val="00C17398"/>
                    <w:rsid w:val="00C20DDE"/>
                    <w:rsid w:val="00C2467E"/>
                    <w:rsid w:val="00C31993"/>
                    <w:rsid w:val="00C31C19"/>
                    <w:rsid w:val="00C40568"/>
                    <w:rsid w:val="00C411D3"/>
                    <w:rsid w:val="00C42482"/>
                    <w:rsid w:val="00C42E12"/>
                    <w:rsid w:val="00C5295D"/>
                    <w:rsid w:val="00C5408F"/>
                    <w:rsid w:val="00C55E56"/>
                    <w:rsid w:val="00C724A4"/>
                    <w:rsid w:val="00C83C4D"/>
                    <w:rsid w:val="00C84EF6"/>
                    <w:rsid w:val="00C90920"/>
                    <w:rsid w:val="00C931CB"/>
                    <w:rsid w:val="00C957CD"/>
                    <w:rsid w:val="00C9611D"/>
                    <w:rsid w:val="00CA4C22"/>
                    <w:rsid w:val="00CB16FF"/>
                    <w:rsid w:val="00CB6549"/>
                    <w:rsid w:val="00CC1215"/>
                    <w:rsid w:val="00CC5D90"/>
                    <w:rsid w:val="00CC6FB2"/>
                    <w:rsid w:val="00CD0431"/>
                    <w:rsid w:val="00CD217C"/>
                    <w:rsid w:val="00CD32CB"/>
                    <w:rsid w:val="00CD400A"/>
                    <w:rsid w:val="00CE3E88"/>
                    <w:rsid w:val="00CF6458"/>
                    <w:rsid w:val="00D01359"/>
                    <w:rsid w:val="00D01740"/>
                    <w:rsid w:val="00D06E8E"/>
                    <w:rsid w:val="00D14760"/>
                    <w:rsid w:val="00D171B9"/>
                    <w:rsid w:val="00D24B95"/>
                    <w:rsid w:val="00D260A2"/>
                    <w:rsid w:val="00D2637B"/>
                    <w:rsid w:val="00D35F33"/>
                    <w:rsid w:val="00D37879"/>
                    <w:rsid w:val="00D40F7C"/>
                    <w:rsid w:val="00D43A96"/>
                    <w:rsid w:val="00D45087"/>
                    <w:rsid w:val="00D4755A"/>
                    <w:rsid w:val="00D53FF6"/>
                    <w:rsid w:val="00D60BBF"/>
                    <w:rsid w:val="00D6555B"/>
                    <w:rsid w:val="00D736D0"/>
                    <w:rsid w:val="00D73ECD"/>
                    <w:rsid w:val="00D81A3E"/>
                    <w:rsid w:val="00DA0372"/>
                    <w:rsid w:val="00DA5B2B"/>
                    <w:rsid w:val="00DB2505"/>
                    <w:rsid w:val="00DB28FB"/>
                    <w:rsid w:val="00DB5528"/>
                    <w:rsid w:val="00DD5E40"/>
                    <w:rsid w:val="00DD698C"/>
                    <w:rsid w:val="00DD7823"/>
                    <w:rsid w:val="00DD7981"/>
                    <w:rsid w:val="00DE07A5"/>
                    <w:rsid w:val="00DE6D57"/>
                    <w:rsid w:val="00DF005E"/>
                    <w:rsid w:val="00DF2A6A"/>
                    <w:rsid w:val="00DF6F3B"/>
                    <w:rsid w:val="00E02554"/>
                    <w:rsid w:val="00E0540B"/>
                    <w:rsid w:val="00E11B71"/>
                    <w:rsid w:val="00E13A2C"/>
                    <w:rsid w:val="00E14230"/>
                    <w:rsid w:val="00E16958"/>
                    <w:rsid w:val="00E25695"/>
                    <w:rsid w:val="00E3254B"/>
                    <w:rsid w:val="00E3303D"/>
                    <w:rsid w:val="00E3396D"/>
                    <w:rsid w:val="00E3466F"/>
                    <w:rsid w:val="00E365D3"/>
                    <w:rsid w:val="00E40DFA"/>
                    <w:rsid w:val="00E44299"/>
                    <w:rsid w:val="00E54847"/>
                    <w:rsid w:val="00E60D13"/>
                    <w:rsid w:val="00E63B04"/>
                    <w:rsid w:val="00E640CE"/>
                    <w:rsid w:val="00E741BF"/>
                    <w:rsid w:val="00E85BAA"/>
                    <w:rsid w:val="00E9004B"/>
                    <w:rsid w:val="00E902CA"/>
                    <w:rsid w:val="00E903CB"/>
                    <w:rsid w:val="00EA1A96"/>
                    <w:rsid w:val="00EA45E0"/>
                    <w:rsid w:val="00EA4B5F"/>
                    <w:rsid w:val="00EB208D"/>
                    <w:rsid w:val="00EB5D5F"/>
                    <w:rsid w:val="00EB6B49"/>
                    <w:rsid w:val="00EC177A"/>
                    <w:rsid w:val="00EC326F"/>
                    <w:rsid w:val="00EC40CF"/>
                    <w:rsid w:val="00EC695C"/>
                    <w:rsid w:val="00ED48D6"/>
                    <w:rsid w:val="00ED56C2"/>
                    <w:rsid w:val="00ED7279"/>
                    <w:rsid w:val="00EE174F"/>
                    <w:rsid w:val="00EE45DB"/>
                    <w:rsid w:val="00EE61D1"/>
                    <w:rsid w:val="00EF05F6"/>
                    <w:rsid w:val="00EF18DF"/>
                    <w:rsid w:val="00EF3274"/>
                    <w:rsid w:val="00EF5BD0"/>
                    <w:rsid w:val="00EF7B38"/>
                    <w:rsid w:val="00F00FEA"/>
                    <w:rsid w:val="00F0152D"/>
                    <w:rsid w:val="00F069F8"/>
                    <w:rsid w:val="00F10D48"/>
                    <w:rsid w:val="00F27B9B"/>
                    <w:rsid w:val="00F34452"/>
                    <w:rsid w:val="00F37A11"/>
                    <w:rsid w:val="00F40B88"/>
                    <w:rsid w:val="00F50468"/>
                    <w:rsid w:val="00F55C83"/>
                    <w:rsid w:val="00F61A74"/>
                    <w:rsid w:val="00F62FB6"/>
                    <w:rsid w:val="00F664D8"/>
                    <w:rsid w:val="00F67C52"/>
                    <w:rsid w:val="00F709B2"/>
                    <w:rsid w:val="00F73FD2"/>
                    <w:rsid w:val="00F7425A"/>
                    <w:rsid w:val="00F757AE"/>
                    <w:rsid w:val="00F76311"/>
                    <w:rsid w:val="00F83359"/>
                    <w:rsid w:val="00F84590"/>
                    <w:rsid w:val="00F92123"/>
                    <w:rsid w:val="00F939ED"/>
                    <w:rsid w:val="00F94296"/>
                    <w:rsid w:val="00F962E1"/>
                    <w:rsid w:val="00FB6282"/>
                    <w:rsid w:val="00FB6ED4"/>
                    <w:rsid w:val="00FB7FD1"/>
                    <w:rsid w:val="00FD4994"/>
                    <w:rsid w:val="00FE2A52"/>
                    <w:rsid w:val="00FF2DDE"/>
                    <w:rsid w:val="00FF5665"/>
                    <w:rsid w:val="00FF6847"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2049"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="1"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="220FA41E"/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{06053D0D-B86A-4D9C-966D-96A1AF810E3C}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/numbering.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
        <pkg:xmlData>
            <w:numbering xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid wp14">
                <w:abstractNum w:abstractNumId="0" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="106F35B0"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="5BA2A810"/>
                    <w:lvl w:ilvl="0" w:tplc="137028C2">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1440" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1560" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1980" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2400" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2820" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3240" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3660" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4080" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4500" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="1" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="1A0A2693"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="26029A68"/>
                    <w:lvl w:ilvl="0" w:tplc="137028C2">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1440" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1560" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1980" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2400" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2820" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3240" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3660" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4080" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4500" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="2" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="1B0F341F"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="1E96AFCC"/>
                    <w:lvl w:ilvl="0" w:tplc="2550D63A">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="（%1）"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="855" w:hanging="855"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="840" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1260" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1680" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2100" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2520" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2940" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3360" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3780" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="3" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="1EE61D4A"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="5E5EBF9E"/>
                    <w:lvl w:ilvl="0" w:tplc="65667CC0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1280" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1400" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1820" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2240" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2660" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3080" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3500" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3920" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4340" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="4" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="2CCB1968"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="1648400C"/>
                    <w:lvl w:ilvl="0" w:tplc="5964A964">
                        <w:start w:val="5"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="（%1）"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1415" w:hanging="855"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1400" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1820" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2240" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2660" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3080" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3500" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3920" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4340" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="5" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="3B3F1DD1"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="DD2C63A4"/>
                    <w:lvl w:ilvl="0" w:tplc="DFEAD758">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4539" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4659" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="5079" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="5499" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="5919" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="6339" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="6759" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="7179" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="7599" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="6" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="3BF55C18"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="6CF42F60"/>
                    <w:lvl w:ilvl="0" w:tplc="7A0A5A30">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1188" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1668" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2088" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2508" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2928" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3348" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3768" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4188" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4608" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="7" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="3ED6696C"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="3738AEBC"/>
                    <w:lvl w:ilvl="0" w:tplc="DA709106">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1275" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1395" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1815" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2235" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2655" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3075" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3495" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3915" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4335" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="8" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="3F624E11"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="38F2F09A"/>
                    <w:lvl w:ilvl="0" w:tplc="7CE6FF5C">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1440" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1560" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1980" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2400" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2820" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3240" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3660" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4080" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4500" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="9" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="407F19DD"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="E8C090D8"/>
                    <w:lvl w:ilvl="0" w:tplc="44BC5268">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1140" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1260" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1680" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2100" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2520" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2940" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3360" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3780" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4200" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="10" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="42E768F0"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="2B5242BC"/>
                    <w:lvl w:ilvl="0" w:tplc="0B089322">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2000" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2120" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2540" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2960" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3380" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3800" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4220" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4640" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="5060" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="11" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="46F9044E"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="84E6ED5A"/>
                    <w:lvl w:ilvl="0" w:tplc="FF5AC404">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="720" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="840" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1260" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1680" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2100" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2520" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2940" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3360" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3780" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="12" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="47FB3ED7"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="55EE093E"/>
                    <w:lvl w:ilvl="0" w:tplc="60BECD80">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1440" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1560" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1980" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2400" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2820" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3240" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3660" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4080" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4500" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="13" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="4E0025BE"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="E74284F6"/>
                    <w:lvl w:ilvl="0" w:tplc="33021C1C">
                        <w:start w:val="2"/>
                        <w:numFmt w:val="decimalEnclosedCircle"/>
                        <w:lvlText w:val="%1"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="720" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1200" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1620" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2040" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2460" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2880" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3300" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3720" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4140" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="14" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="57E94329"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="EBA4B6F2"/>
                    <w:lvl w:ilvl="0" w:tplc="8C02A712">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="（%1）"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1275" w:hanging="855"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1260" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1680" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2100" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2520" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2940" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3360" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3780" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4200" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="15" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="5F255F78"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="3356DA72"/>
                    <w:lvl w:ilvl="0" w:tplc="77464494">
                        <w:start w:val="2"/>
                        <w:numFmt w:val="decimalEnclosedCircle"/>
                        <w:lvlText w:val="%1"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="360" w:hanging="360"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="840" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1260" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1680" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2100" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2520" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2940" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3360" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3780" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="16" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="61BE7325"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="070EFA34"/>
                    <w:lvl w:ilvl="0" w:tplc="E9AABC58">
                        <w:start w:val="2"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1420" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1540" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1960" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2380" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2800" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3220" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3640" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4060" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4480" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="17" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="6855551E"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="26029A68"/>
                    <w:lvl w:ilvl="0" w:tplc="137028C2">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1440" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1560" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1980" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2400" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2820" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3240" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3660" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="4080" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4500" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="18" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="789666DA"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="F3DA782A"/>
                    <w:lvl w:ilvl="0" w:tplc="804C6FAE">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="720" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:ascii="黑体" w:eastAsia="黑体" w:hAnsi="宋体" w:hint="default"/>
                            <w:sz w:val="32"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="840" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1260" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1680" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2100" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="2520" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2940" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3360" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3780" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="19" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="7FCD41E2"/>
                    <w:multiLevelType w:val="hybridMultilevel"/>
                    <w:tmpl w:val="3738AEBC"/>
                    <w:lvl w:ilvl="0" w:tplc="DA709106">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="japaneseCounting"/>
                        <w:lvlText w:val="%1、"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1275" w:hanging="720"/>
                        </w:pPr>
                        <w:rPr>
                            <w:rFonts w:hint="default"/>
                        </w:rPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%2)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1395" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%3."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="1815" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2235" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%5)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="2655" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%6."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="3075" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3495" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerLetter"/>
                        <w:lvlText w:val="%8)"/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="3915" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="lowerRoman"/>
                        <w:lvlText w:val="%9."/>
                        <w:lvlJc w:val="right"/>
                        <w:pPr>
                            <w:ind w:left="4335" w:hanging="420"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:num w:numId="1">
                    <w:abstractNumId w:val="11"/>
                </w:num>
                <w:num w:numId="2">
                    <w:abstractNumId w:val="8"/>
                </w:num>
                <w:num w:numId="3">
                    <w:abstractNumId w:val="12"/>
                </w:num>
                <w:num w:numId="4">
                    <w:abstractNumId w:val="16"/>
                </w:num>
                <w:num w:numId="5">
                    <w:abstractNumId w:val="1"/>
                </w:num>
                <w:num w:numId="6">
                    <w:abstractNumId w:val="17"/>
                </w:num>
                <w:num w:numId="7">
                    <w:abstractNumId w:val="0"/>
                </w:num>
                <w:num w:numId="8">
                    <w:abstractNumId w:val="9"/>
                </w:num>
                <w:num w:numId="9">
                    <w:abstractNumId w:val="2"/>
                </w:num>
                <w:num w:numId="10">
                    <w:abstractNumId w:val="14"/>
                </w:num>
                <w:num w:numId="11">
                    <w:abstractNumId w:val="4"/>
                </w:num>
                <w:num w:numId="12">
                    <w:abstractNumId w:val="7"/>
                </w:num>
                <w:num w:numId="13">
                    <w:abstractNumId w:val="19"/>
                </w:num>
                <w:num w:numId="14">
                    <w:abstractNumId w:val="3"/>
                </w:num>
                <w:num w:numId="15">
                    <w:abstractNumId w:val="10"/>
                </w:num>
                <w:num w:numId="16">
                    <w:abstractNumId w:val="5"/>
                </w:num>
                <w:num w:numId="17">
                    <w:abstractNumId w:val="18"/>
                </w:num>
                <w:num w:numId="18">
                    <w:abstractNumId w:val="6"/>
                </w:num>
                <w:num w:numId="19">
                    <w:abstractNumId w:val="15"/>
                </w:num>
                <w:num w:numId="20">
                    <w:abstractNumId w:val="13"/>
                </w:num>
            </w:numbering>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                      xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                      xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                      xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                      xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                      mc:Ignorable="w14 w15 w16se w16cid">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:ascii="Calibri" w:eastAsia="宋体" w:hAnsi="Calibri" w:cs="Times New Roman"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0"
                                w:defQFormat="0" w:count="377">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1"
                                    w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:uiPriority="59"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hashtag" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Unresolved Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link Error" w:semiHidden="1" w:unhideWhenUsed="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:rsid w:val="00983481"/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        <w:kern w:val="2"/>
                        <w:sz w:val="21"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a3">
                    <w:name w:val="header"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a4"/>
                    <w:uiPriority w:val="99"/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="002F2947"/>
                    <w:pPr>
                        <w:pBdr>
                            <w:bottom w:val="single" w:sz="6" w:space="1" w:color="auto"/>
                        </w:pBdr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="center"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a4">
                    <w:name w:val="页眉 字符"/>
                    <w:link w:val="a3"/>
                    <w:uiPriority w:val="99"/>
                    <w:rsid w:val="002F2947"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a5">
                    <w:name w:val="footer"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a6"/>
                    <w:uiPriority w:val="99"/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="002F2947"/>
                    <w:pPr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a6">
                    <w:name w:val="页脚 字符"/>
                    <w:link w:val="a5"/>
                    <w:uiPriority w:val="99"/>
                    <w:rsid w:val="002F2947"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a7">
                    <w:name w:val="Balloon Text"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a8"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="008C3E0E"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a8">
                    <w:name w:val="批注框文本 字符"/>
                    <w:link w:val="a7"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:rsid w:val="008C3E0E"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Times New Roman" w:hAnsi="Times New Roman"/>
                        <w:kern w:val="2"/>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="table" w:styleId="a9">
                    <w:name w:val="Table Grid"/>
                    <w:basedOn w:val="a1"/>
                    <w:uiPriority w:val="59"/>
                    <w:rsid w:val="00A86E4C"/>
                    <w:tblPr>
                        <w:tblBorders>
                            <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                            <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                        </w:tblBorders>
                    </w:tblPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                           xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                           xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                           xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                           xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                           xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                           xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                           mc:Ignorable="w14 w15 w16se w16cid">
                <w:divs>
                    <w:div w:id="283852077">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                    <w:div w:id="1318194469">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                    <w:div w:id="1467235881">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                    <w:div w:id="1818262411">
                        <w:bodyDiv w:val="1"/>
                        <w:marLeft w:val="0"/>
                        <w:marRight w:val="0"/>
                        <w:marTop w:val="0"/>
                        <w:marBottom w:val="0"/>
                        <w:divBdr>
                            <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                            <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
                        </w:divBdr>
                    </w:div>
                </w:divs>
                <w:optimizeForBrowser/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                     xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                     xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                     xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                     xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                     xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                     mc:Ignorable="w14 w15 w16se w16cid">
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="黑体">
                    <w:altName w:val="SimHei"/>
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Calibri">
                    <w:panose1 w:val="020F0502020204030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000247B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋">
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Ђˎ̥">
                    <w:altName w:val="Arial"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000000" w:usb1="00000000" w:usb2="00000000" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="方正大标宋繁体">
                    <w:altName w:val="宋体"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="default"/>
                    <w:sig w:usb0="00000000" w:usb1="00000000" w:usb2="00000010" w:usb3="00000000" w:csb0="00040000"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="仿宋_GB2312">
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="00000001" w:usb1="080E0000" w:usb2="00000010" w:usb3="00000000" w:csb0="00040000"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线 Light">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线">
                    <w:altName w:val="DengXian"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml"
              pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:dcmitype="http://purl.org/dc/dcmitype/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title>散派船员管理办法</dc:title>
                <dc:subject/>
                <dc:creator>renbaokun</dc:creator>
                <cp:keywords/>
                <cp:lastModifiedBy>夏 德航</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <cp:lastPrinted>2017-04-07T01:58:00Z</cp:lastPrinted>
                <dcterms:created xsi:type="dcterms:W3CDTF">2020-05-25T01:50:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2020-05-25T01:50:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>0</TotalTime>
                <Pages>2</Pages>
                <Words>145</Words>
                <Characters>829</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>6</Lines>
                <Paragraphs>1</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company>IT</Company>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>973</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>