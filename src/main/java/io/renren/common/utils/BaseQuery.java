package io.renren.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 通用查询参数
 */
@ApiModel(description = "通用查询参数")
@Data
public class BaseQuery {

    @ApiModelProperty(position = 1, required = false, value = "当前页码")
    public Integer page = 1;

    @ApiModelProperty(position = 2, required = false, value = "每页显示记录数")
    public Integer limit = 10;

}
