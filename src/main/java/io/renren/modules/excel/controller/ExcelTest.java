package io.renren.modules.excel.controller;

import com.alibaba.excel.EasyExcel;
import io.renren.modules.excel.entity.InsuranceExcelEntity;
import io.renren.modules.excel.entity.InsuranceExcelEntityListener;
import io.renren.modules.excel.entity.SalaryExcelEntity;
import io.renren.modules.excel.entity.SalaryExcelEntityListener;

import java.util.List;

public class ExcelTest {

    public static void main(String[] args) {
        String path = "C:/other/Yangzuxian/test1.xlsx";
        //读取excel文件
//        List<SalaryExcelEntity> beans = EasyExcel.read(path, SalaryExcelEntity.class, new SalaryExcelEntityListener()).sheet().doReadSync();
        List<InsuranceExcelEntity> beans = EasyExcel.read(path, InsuranceExcelEntity.class, new InsuranceExcelEntityListener()).sheet().doReadSync();

        // 业务处理
        beans.stream().forEach(System.out::println);

        //批量入库

    }
}
