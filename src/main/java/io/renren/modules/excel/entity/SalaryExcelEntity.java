package io.renren.modules.excel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SalaryExcelEntity {

    //index代表所在列 从0开始
    @ExcelProperty(value = "船员姓名",index = 0)
    private String crewName;

    @ExcelProperty(value = "船员身份证号",index = 1)
    private String documentNumber;

    @ExcelProperty(value = "基本工资",index = 2)
    private String basePay;

    @ExcelProperty(value = "加班工资",index = 3)
    private String overtimePay;

    @ExcelProperty(value = "补贴",index = 4)
    private String subsidyPay;

    @ExcelProperty(value = "奖金",index = 5)
    private String bonus;

    @ExcelProperty(value = "社保",index = 6)
    private String socialInsurance;

    @ExcelProperty(value = "个人所得税",index = 7)
    private String individualTax;

    @ExcelProperty(value = "其他",index = 8)
    private String otherPay;

}
