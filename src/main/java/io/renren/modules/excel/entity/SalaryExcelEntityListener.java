package io.renren.modules.excel.entity;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

public class SalaryExcelEntityListener extends AnalysisEventListener<SalaryExcelEntity> {
    //读取的时候处理xxx可做数据校验
    @Override
    public void invoke(SalaryExcelEntity salaryexcelEntity, AnalysisContext analysisContext) {
    }

    //读取完了之后做的事情，可以做xxx
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
