package io.renren.modules.excel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class InsuranceExcelEntity {

    //index代表所在列 从0开始
    @ExcelProperty(value = "船员姓名",index = 0)
    private String crewName;

    @ExcelProperty(value = "船员身份证号",index = 1)
    private String documentNumber;

    @ExcelProperty(value = "养老保险",index = 2)
    private String endowmentInsurance;

    @ExcelProperty(value = "医疗保险",index = 3)
    private String medicalInsurance;

    @ExcelProperty(value = "失业保险",index = 4)
    private String unemploymentInsurance;

    @ExcelProperty(value = "工伤保险",index = 5)
    private String employmentInsurance;

    @ExcelProperty(value = "生育保险",index = 6)
    private String maternityInsurance;

    @ExcelProperty(value = "公积金",index = 7)
    private String accumulationFund;

    @ExcelProperty(value = "商业保险",index = 8)
    private String commercialInsurance;

    @ExcelProperty(value = "其他",index = 9)
    private String otherInsurance;

}
