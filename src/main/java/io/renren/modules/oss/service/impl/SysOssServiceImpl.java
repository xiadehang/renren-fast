/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.oss.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.generator.dao.InsuranceInfoDao;
import io.renren.modules.generator.entity.InsuranceInfoEntity;
import io.renren.modules.generator.query.InsuranceInfoQuery;
import io.renren.modules.oss.dao.SysOssDao;
import io.renren.modules.oss.entity.SysOssEntity;
import io.renren.modules.oss.query.SysOssQuery;
import io.renren.modules.oss.service.SysOssService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("sysOssService")
public class SysOssServiceImpl extends ServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

	@Autowired
	private SysOssDao sysOssDao;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {

		SysOssQuery ciq = SysOssQuery.builder()
				.companyId((Long) params.get("companyId"))
				.build();

		IPage<SysOssEntity> page = sysOssDao.selectPage(
				new Query<SysOssEntity>().getPage(params),
				new QueryWrapper<SysOssEntity>().lambda()
						//根据每个人的公司id查询，管理员没有绑定公司id
						.eq(ciq.getCompanyId() != null, SysOssEntity::getAffiliatedCompanyId, ciq.getCompanyId())
						//倒序创建时间排序
						.orderByDesc(SysOssEntity::getCreateDate)
		);

		return new PageUtils(page);
	}
	
}
