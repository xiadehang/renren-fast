package io.renren.modules.generator.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.generator.entity.ContractInfoEntity;
import io.renren.modules.generator.entity.DispatchInfoEntity;
import io.renren.modules.generator.service.DispatchInfoService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * 派遣信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Api(tags = "派遣信息表")
@RestController
@RequestMapping("generator/dispatchinfo")
public class DispatchInfoController {
    @Autowired
    private DispatchInfoService dispatchInfoService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 列表
     */
    @ApiOperation("列表")
    @GetMapping("/list")
    @RequiresPermissions("generator:dispatchinfo:list")
    public R list(@RequestParam Map<String, Object> params){

        //1.每个人登录后，有userid
        Long userId = ShiroUtils.getUserId();
        //2.查询该userid的companyid
        Optional.ofNullable(sysUserService.getOne(
                new QueryWrapper<SysUserEntity>().lambda().eq(SysUserEntity::getUserId, userId)))
                .filter(f -> f.getAffiliatedCompanyId() != null)
                .map(m -> params.put("companyId", m.getAffiliatedCompanyId()));
        //3.再用companyid查询合同表的数据，派遣表的数据，船员表的数据
        PageUtils page = dispatchInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("信息")
    @RequestMapping("/info/{dispatchId}")
    @RequiresPermissions("generator:dispatchinfo:info")
    public R info(@PathVariable("dispatchId") Long dispatchId){
		DispatchInfoEntity dispatchInfo = dispatchInfoService.getById(dispatchId);

        return R.ok().put("dispatchInfo", dispatchInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:dispatchinfo:save")
    public R save(@RequestBody DispatchInfoEntity dispatchInfo){

        Date date = new Date();
        SysUserEntity user= ShiroUtils.getUserEntity();
        String createUsername = user.getUsername();
        Long company_name = user.getAffiliatedCompanyId();

        dispatchInfo.setCreateTime(date);
        dispatchInfo.setCreateUserId(createUsername);
        dispatchInfo.setAffiliatedCompanyId(company_name);

		dispatchInfoService.save(dispatchInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:dispatchinfo:update")
    public R update(@RequestBody DispatchInfoEntity dispatchInfo){
		dispatchInfoService.updateById(dispatchInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:dispatchinfo:delete")
    public R delete(@RequestBody Long[] dispatchIds){
		dispatchInfoService.removeByIds(Arrays.asList(dispatchIds));

        return R.ok();
    }

    /**
     * 下船
     * @return
     */
    @PostMapping("disembarkdHandle")
    @RequiresPermissions("generator:dispatchinfo:disembarkdHandle")
    public  R disembarkdHandle(@RequestBody Long[] dispatchIds){
        List<Long> idList = Arrays.asList(dispatchIds);
        idList.stream().forEach(id -> {
            DispatchInfoEntity cie = new DispatchInfoEntity();
            cie.setDispatchId(id);
            cie.setDispatchStatus(Constant.DispatchInfoStatus.DISEMBARKED.getName());
            dispatchInfoService.updateById(cie);
        });
        return R.ok();
    }

    /**
     * 下船时间
     */
    @RequestMapping("/updateSailingTime")
    public R updateSailingTime(@RequestBody DispatchInfoEntity dispatchInfo){

        dispatchInfo.setDispatchStatus("已下船");
        dispatchInfoService.updateById(dispatchInfo);

        return R.ok();
    }

    /**
     * 预计下船
     */
    @ApiOperation("预计下船")
    @GetMapping("/linkDisembarkationExpected")
    public R linkDisembarkationExpected(@RequestParam Map<String, Object> params,@RequestParam("dispatchId") Long dispatchId){

        params.put("dispatchId", dispatchId);

        PageUtils page = dispatchInfoService.queryPage(params);
        return R.ok().put("page", page);
    }
}
