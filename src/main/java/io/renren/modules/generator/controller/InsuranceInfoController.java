package io.renren.modules.generator.controller;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.generator.entity.InsuranceInfoEntity;
import io.renren.modules.generator.service.InsuranceInfoService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 社保信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Api(tags = "社保信息表")
@RestController
@RequestMapping("generator/insuranceinfo")
public class InsuranceInfoController {
    @Autowired
    private InsuranceInfoService insuranceInfoService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 列表
     */
    @ApiOperation("列表")
    @GetMapping("/list")
    @RequiresPermissions("generator:insuranceinfo:list")
    public R list(@RequestParam Map<String, Object> params){

        //1.每个人登录后，有userid
        Long userId = ShiroUtils.getUserId();
        //2.查询该userid的companyid
        Optional.ofNullable(sysUserService.getOne(
                new QueryWrapper<SysUserEntity>().lambda().eq(SysUserEntity::getUserId, userId)))
                .filter(f -> f.getAffiliatedCompanyId() != null)
                .map(m -> params.put("companyId", m.getAffiliatedCompanyId()));
        //3.再用companyid查询合同表的数据，派遣表的数据，船员表的数据
        PageUtils page = insuranceInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("社保信息")
    @GetMapping("/info/{insuranceInfoId}")
    @RequiresPermissions("generator:insuranceinfo:info")
    public R info(@PathVariable("insuranceInfoId") Long insuranceInfoId){
		InsuranceInfoEntity insuranceInfo = insuranceInfoService.getById(insuranceInfoId);

        return R.ok().put("insuranceInfo", insuranceInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:insuranceinfo:save")
    public R save(@RequestBody InsuranceInfoEntity insuranceInfo){

        Date date = new Date();
        SysUserEntity user= ShiroUtils.getUserEntity();
        String createUsername = user.getUsername();
        Long company_name = user.getAffiliatedCompanyId();

        insuranceInfo.setCreateTime(date);
        insuranceInfo.setCreateUserId(createUsername);
        insuranceInfo.setAffiliatedCompanyId(company_name);
		insuranceInfoService.save(insuranceInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation("社保修改")
    @PostMapping("/update")
    @RequiresPermissions("generator:insuranceinfo:update")
    public R update(@RequestBody InsuranceInfoEntity insuranceInfo){
		insuranceInfoService.updateById(insuranceInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:insuranceinfo:delete")
    public R delete(@RequestBody Long[] insuranceInfoIds){
		insuranceInfoService.removeByIds(Arrays.asList(insuranceInfoIds));

        return R.ok();
    }

    /**
     * 缴纳人数
     */
    @ApiOperation("缴纳人数")
    @GetMapping("/selectPayCountList")
    public R selectPayCountList(){

        List<Map> selectPayCountList = insuranceInfoService.selectPayCount();

        return R.ok().put("selectPayCountList", selectPayCountList);
    }

    /**
     * 缴纳总额
     */
    @ApiOperation("缴纳总额")
    @GetMapping("/selectPayTotalList")
    public R selectPayTotalList(){

        List<Map> selectPayTotalList = insuranceInfoService.selectPayTotal();

        return R.ok().put("selectPayTotalList", selectPayTotalList);
    }
}
