package io.renren.modules.generator.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.generator.entity.ExperienceInfoEntity;
import io.renren.modules.generator.service.ExperienceInfoService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 工作履历表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-12-04 21:48:38
 */
@RestController
@RequestMapping("generator/experienceinfo")
public class ExperienceInfoController {
    @Autowired
    private ExperienceInfoService experienceInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:experienceinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = experienceInfoService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{workExperienceId}")
    @RequiresPermissions("generator:experienceinfo:info")
    public R info(@PathVariable("workExperienceId") Long workExperienceId){
		ExperienceInfoEntity experienceInfo = experienceInfoService.getById(workExperienceId);

        return R.ok().put("experienceInfo", experienceInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:experienceinfo:save")
    public R save(@RequestBody ExperienceInfoEntity experienceInfo){
		experienceInfoService.save(experienceInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:experienceinfo:update")
    public R update(@RequestBody ExperienceInfoEntity experienceInfo){
		experienceInfoService.updateById(experienceInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:experienceinfo:delete")
    public R delete(@RequestBody Long[] workExperienceIds){
		experienceInfoService.removeByIds(Arrays.asList(workExperienceIds));

        return R.ok();
    }

}
