package io.renren.modules.generator.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.R;
import io.renren.common.validator.Assert;
import io.renren.modules.generator.entity.DataDictionaryEntity;
import io.renren.modules.generator.service.DataDictionaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 数据字典
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-30 20:48:09
 */
@Api(tags = "数据字典")
@RestController
@RequestMapping("generator/datadictionary")
public class DataDictionaryController {
    @Autowired
    private DataDictionaryService dataDictionaryService;


    /**
     * 信息
     */
    @ApiOperation("信息")
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long dictionaryId){
        Assert.isNull(dictionaryId,"字典id不能为空");

        return R.ok().put("list",dataDictionaryService.list(new QueryWrapper<DataDictionaryEntity>()
                .lambda().eq(DataDictionaryEntity::getParentId,dictionaryId)));
    }

}
