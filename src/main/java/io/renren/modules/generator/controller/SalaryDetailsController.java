package io.renren.modules.generator.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.excel.entity.SalaryExcelEntity;
import io.renren.modules.excel.entity.SalaryExcelEntityListener;
import io.renren.modules.generator.entity.CrewInfoEntity;
import io.renren.modules.generator.entity.SalaryDetailsEntity;
import io.renren.modules.generator.service.CrewInfoService;
import io.renren.modules.generator.service.SalaryDetailsService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * 薪资详情表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Api(tags = "薪资详情表")
@RestController
@RequestMapping("generator/salarydetails")
public class SalaryDetailsController {
    @Autowired
    private SalaryDetailsService salaryDetailsService;
    @Autowired
    private CrewInfoService crewInfoService;

    /**
     * 列表
     */
    @ApiOperation("列表")
    @GetMapping("/list")
//    @RequiresPermissions("generator:salarydetails:list")
    public R list(@RequestParam Map<String, Object> params, @RequestParam(value = "salaryInfoId", required = false) Long salaryInfoId) {

        params.put("salaryInfoId", salaryInfoId);
        PageUtils page = salaryDetailsService.querySalaryDetailsPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("薪资详情信息")
    @GetMapping("/info/{salaryDetailsId}")
//    @RequiresPermissions("generator:salarydetails:info")
    public R info(@PathVariable("salaryDetailsId") Long salaryDetailsId) {
        SalaryDetailsEntity salaryDetails = salaryDetailsService.getById(salaryDetailsId);

        return R.ok().put("salaryDetails", salaryDetails);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:salarydetails:save")
    public R save(@RequestBody SalaryDetailsEntity salaryDetails) {
        salaryDetailsService.save(salaryDetails);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:salarydetails:update")
    public R update(@RequestBody SalaryDetailsEntity salaryDetails) {
        salaryDetailsService.updateById(salaryDetails);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:salarydetails:delete")
    public R delete(@RequestBody Long[] salaryDetailsIds) {
        salaryDetailsService.removeByIds(Arrays.asList(salaryDetailsIds));

        return R.ok();
    }

    /**
     * 导入薪资
     */
    @ApiOperation("导入薪资")
    @PostMapping("/importSalary")
//    @RequiresPermissions("generator:salarydetails:importSalary")
    public String importSalary(@RequestParam(value = "salaryInfoId", required = false) Long salaryInfoId, @RequestParam("file") MultipartFile file
    ) throws IOException {

        String path = "D:/test.xlsx";
        //读取excel文件
        List<SalaryExcelEntity> beans = EasyExcel.read(file.getInputStream(), SalaryExcelEntity.class, new SalaryExcelEntityListener()).sheet().doReadSync();
//        List<SalaryExcelEntity> beans = EasyExcel.read(path, SalaryExcelEntity.class, new SalaryExcelEntityListener()).sheet().doReadSync();
        // 业务处理
        beans.stream().forEach(System.out::println);

        //批量入库
        for (SalaryExcelEntity excel : beans) {
            SalaryDetailsEntity salaryDetails = new SalaryDetailsEntity();
            CrewInfoEntity crewInfoEntity = crewInfoService.getOne(new QueryWrapper<CrewInfoEntity>().lambda()
                    .eq(StringUtils.isNotBlank(excel.getDocumentNumber()), CrewInfoEntity::getDocumentNumber, excel.getDocumentNumber()));
            if (crewInfoEntity == null) {
                return "error";
            } else {
                salaryDetails.setCrewId(crewInfoEntity.getCrewId());
                salaryDetails.setSalaryInfoId(salaryInfoId);
                salaryDetails.setBasePay(excel.getBasePay());
                salaryDetails.setOvertimePay(excel.getOvertimePay());
                salaryDetails.setSubsidyPay(excel.getSubsidyPay());
                salaryDetails.setBonus(excel.getBonus());
                salaryDetails.setSocialInsurance(excel.getSocialInsurance());
                salaryDetails.setIndividualTax(excel.getIndividualTax());
                salaryDetails.setOtherPay(excel.getOtherPay());
                salaryDetailsService.save(salaryDetails);
            }

        }
        return "success";
    }

}
