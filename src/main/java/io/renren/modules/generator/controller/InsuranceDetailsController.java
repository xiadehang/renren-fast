package io.renren.modules.generator.controller;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.modules.excel.entity.InsuranceExcelEntity;
import io.renren.modules.excel.entity.InsuranceExcelEntityListener;
import io.renren.modules.generator.entity.CrewInfoEntity;
import io.renren.modules.generator.entity.InsuranceDetailsEntity;
import io.renren.modules.generator.service.CrewInfoService;
import io.renren.modules.generator.service.InsuranceDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 保险详情表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Api(tags = "保险详情表")
@RestController
@RequestMapping("generator/insurancedetails")
public class InsuranceDetailsController {
    @Autowired
    private InsuranceDetailsService insuranceDetailsService;
    @Autowired
    private CrewInfoService crewInfoService;

    /**
     * 列表
     */
    @ApiOperation("列表")
    @GetMapping("/list")
//    @RequiresPermissions("generator:insurancedetails:list")
    public R list(@RequestParam Map<String, Object> params, @RequestParam(value = "insuranceInfoId", required = false) Long insuranceInfoId){

        params.put("insuranceInfoId", insuranceInfoId);
        PageUtils page = insuranceDetailsService.queryInsuranceDetailsPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("保险详情信息")
    @GetMapping("/info/{insuranceDetailsId}")
//    @RequiresPermissions("generator:insurancedetails:info")
    public R info(@PathVariable("insuranceDetailsId") Long insuranceDetailsId){
		InsuranceDetailsEntity insuranceDetails = insuranceDetailsService.getById(insuranceDetailsId);

        return R.ok().put("insuranceDetails", insuranceDetails);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:insurancedetails:save")
    public R save(@RequestBody InsuranceDetailsEntity insuranceDetails){
		insuranceDetailsService.save(insuranceDetails);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @RequiresPermissions("generator:insurancedetails:update")
    public R update(@RequestBody InsuranceDetailsEntity insuranceDetails){
		insuranceDetailsService.updateById(insuranceDetails);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("generator:insurancedetails:delete")
    public R delete(@RequestBody Long[] insuranceDetailsIds){
		insuranceDetailsService.removeByIds(Arrays.asList(insuranceDetailsIds));

        return R.ok();
    }

    /**
     * 导入保险
     */
    @ApiOperation("导入保险")
    @PostMapping("/importInsurance")
//    @RequiresPermissions("generator:salarydetails:importInsurance")
    public String importInsurance(@RequestParam(value="insuranceInfoId") Long insuranceInfoId, @RequestParam("file") MultipartFile file
    ) throws IOException {

        //读取excel文件
        List<InsuranceExcelEntity> insuranceExcelList = EasyExcel.read(file.getInputStream(), InsuranceExcelEntity.class, new InsuranceExcelEntityListener()).sheet().doReadSync();
        // 业务处理

        //批量入库
        for (InsuranceExcelEntity insuranceExcel : insuranceExcelList){
            InsuranceDetailsEntity insuranceDetails = new InsuranceDetailsEntity();
            CrewInfoEntity crewInfoEntity = crewInfoService.getOne(new QueryWrapper<CrewInfoEntity>().lambda()
                    .eq(StringUtils.isNotBlank(insuranceExcel.getDocumentNumber()), CrewInfoEntity::getDocumentNumber, insuranceExcel.getDocumentNumber()));
            insuranceDetails.setCrewId(crewInfoEntity.getCrewId());
            insuranceDetails.setInsuranceInfoId(insuranceInfoId);
            insuranceDetails.setEndowmentInsurance(insuranceExcel.getEndowmentInsurance());
            insuranceDetails.setMedicalInsurance(insuranceExcel.getMedicalInsurance());
            insuranceDetails.setUnemploymentInsurance(insuranceExcel.getUnemploymentInsurance());
            insuranceDetails.setEmploymentInsurance(insuranceExcel.getEmploymentInsurance());
            insuranceDetails.setMaternityInsurance(insuranceExcel.getMaternityInsurance());
            insuranceDetails.setAccumulationFund(insuranceExcel.getAccumulationFund());
            insuranceDetails.setCommercialInsurance(insuranceExcel.getCommercialInsurance());
            insuranceDetails.setOtherInsurance(insuranceExcel.getOtherInsurance());
            insuranceDetailsService.save(insuranceDetails);
        }
        return "success";
    }
}
