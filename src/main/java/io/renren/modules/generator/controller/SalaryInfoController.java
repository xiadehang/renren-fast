package io.renren.modules.generator.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.generator.entity.SalaryDetailsEntity;
import io.renren.modules.generator.entity.SalaryInfoEntity;
import io.renren.modules.generator.service.SalaryDetailsService;
import io.renren.modules.generator.service.SalaryInfoService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * 薪资信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Api(tags = "薪资信息表")
@RestController
@RequestMapping("generator/salaryinfo")
public class SalaryInfoController {
    @Autowired
    private SalaryInfoService salaryInfoService;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SalaryDetailsService salaryDetailsService;
    /**
     * 列表
     */
    @ApiOperation("列表")
    @GetMapping("/list")
    @RequiresPermissions("generator:salaryinfo:list")
    public R list(@RequestParam Map<String, Object> params){

        //1.每个人登录后，有userid
        Long userId = ShiroUtils.getUserId();
        //2.查询该userid的companyid
        Optional.ofNullable(sysUserService.getOne(
                new QueryWrapper<SysUserEntity>().lambda().eq(SysUserEntity::getUserId, userId)))
                .filter(f -> f.getAffiliatedCompanyId() != null)
                .map(m -> params.put("companyId", m.getAffiliatedCompanyId()));
        //3.再用companyid查询合同表的数据，派遣表的数据，船员表的数据
        PageUtils page = salaryInfoService.queryPage(params);


        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("薪资信息")
    @GetMapping("/info/{salaryInfoId}")
    @RequiresPermissions("generator:salaryinfo:info")
    public R info(@PathVariable("salaryInfoId") Long salaryInfoId){
		SalaryInfoEntity salaryInfo = salaryInfoService.getById(salaryInfoId);

        return R.ok().put("salaryInfo", salaryInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:salaryinfo:save")
    public R save(@RequestBody SalaryInfoEntity salaryInfo){

        Date date = new Date();
        SysUserEntity user= ShiroUtils.getUserEntity();
        String createUsername = user.getUsername();
        Long company_name = user.getAffiliatedCompanyId();

        salaryInfo.setCreateTime(date);
        salaryInfo.setCreateUserId(createUsername);
        salaryInfo.setAffiliatedCompanyId(company_name);
		salaryInfoService.save(salaryInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation("薪资修改")
    @PostMapping("/update")
    @RequiresPermissions("generator:salaryinfo:update")
    public R update(@RequestBody SalaryInfoEntity salaryInfo){
		salaryInfoService.updateById(salaryInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:salaryinfo:delete")
    public R delete(@RequestBody Long[] salaryInfoIds){
		salaryInfoService.removeByIds(Arrays.asList(salaryInfoIds));

        return R.ok();
    }

    /**
     * 发放人数
     */
    @ApiOperation("发放人数")
    @GetMapping("/selectGrantCountList")
    public R selectGrantCountList(){

        List<Map> selectGrantCountList = salaryInfoService.selectGrantCount();

        return R.ok().put("selectGrantCountList", selectGrantCountList);
    }

    /**
     * 发放总额
     */
    @ApiOperation("发放总额")
    @GetMapping("/selectGrantTotalList")
    public R selectGrantTotalList(){

        List<Map> selectGrantTotalList = salaryInfoService.selectGrantTotal();

        return R.ok().put("selectGrantTotalList", selectGrantTotalList);
    }

}
