package io.renren.modules.generator.controller;

import java.util.*;

import io.renren.common.utils.Constant;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.sys.entity.SysRoleEntity;
import io.renren.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.generator.entity.AffiliatedCompanyInfoEntity;
import io.renren.modules.generator.service.AffiliatedCompanyInfoService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 所属公司信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Api(tags = "所属公司信息表")
@RestController
@RequestMapping("generator/affiliatedcompanyinfo")
public class AffiliatedCompanyInfoController extends ShiroUtils {
    @Autowired
    private AffiliatedCompanyInfoService affiliatedCompanyInfoService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("generator:affiliatedcompanyinfo:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = affiliatedCompanyInfoService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 公司列表
     */
    @ApiOperation("公司列表")
    @GetMapping("/select")
//    @RequiresPermissions("generator:affiliatedcompanyinfo:select")
    public R select(){
        Map<String, Object> map = new HashMap<>();

        SysUserEntity user= ShiroUtils.getUserEntity();
        Long affiliated_company_id = user.getAffiliatedCompanyId();

        //如果不是超级管理员，则只查询自己所拥有的角色列表
        if(getUserId() != Constant.SUPER_ADMIN){
            map.put("affiliated_company_id", affiliated_company_id);
        }
        List<AffiliatedCompanyInfoEntity> list = (List<AffiliatedCompanyInfoEntity>) affiliatedCompanyInfoService.listByMap(map);

        return R.ok().put("list", list);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{affiliatedCompanyId}")
    @RequiresPermissions("generator:affiliatedcompanyinfo:info")
    public R info(@PathVariable("affiliatedCompanyId") Long affiliatedCompanyId){
		AffiliatedCompanyInfoEntity affiliatedCompanyInfo = affiliatedCompanyInfoService.getById(affiliatedCompanyId);

        return R.ok().put("affiliatedCompanyInfo", affiliatedCompanyInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:affiliatedcompanyinfo:save")
    public R save(@RequestBody AffiliatedCompanyInfoEntity affiliatedCompanyInfo){

        SysUserEntity user= ShiroUtils.getUserEntity();
        String username = user.getUsername();
        user.setCreateTime(new Date());
        Date date = new Date();

        affiliatedCompanyInfo.setUpdateUser(username);
        affiliatedCompanyInfo.setUpdateDatetime(date);

        affiliatedCompanyInfoService.save(affiliatedCompanyInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:affiliatedcompanyinfo:update")
    public R update(@RequestBody AffiliatedCompanyInfoEntity affiliatedCompanyInfo){
		affiliatedCompanyInfoService.updateById(affiliatedCompanyInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:affiliatedcompanyinfo:delete")
    public R delete(@RequestBody Long[] affiliatedCompanyIds){
		affiliatedCompanyInfoService.removeByIds(Arrays.asList(affiliatedCompanyIds));

        return R.ok();
    }

}
