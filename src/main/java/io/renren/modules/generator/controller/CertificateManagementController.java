package io.renren.modules.generator.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.renren.modules.generator.entity.CertificateManagementEntity;
import io.renren.modules.generator.service.CertificateManagementService;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;



/**
 * 证书信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Api(tags = "证书信息表")
@RestController
@RequestMapping("generator/certificatemanagement")
public class CertificateManagementController {
    @Autowired
    private CertificateManagementService certificateManagementService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 列表
     */
    @ApiOperation("列表")
    @GetMapping("/list")
    @RequiresPermissions("generator:certificatemanagement:list")
    public R list(@RequestParam Map<String, Object> params){

        //1.每个人登录后，有userid
        Long userId = ShiroUtils.getUserId();
        //2.查询该userid的companyid
        Optional.ofNullable(sysUserService.getOne(
                new QueryWrapper<SysUserEntity>().lambda().eq(SysUserEntity::getUserId, userId)))
                .filter(f -> f.getAffiliatedCompanyId() != null)
                .map(m -> params.put("companyId", m.getAffiliatedCompanyId()));
        //3.再用companyid查询合同表的数据，派遣表的数据，船员表的数据
        PageUtils page = certificateManagementService.queryCertificatePage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation("证书信息")
    @GetMapping("/info/{certificateId}")
    @RequiresPermissions("generator:certificatemanagement:info")
    public R info(@PathVariable("certificateId") Long certificateId){
		CertificateManagementEntity certificateManagement = certificateManagementService.getById(certificateId);

        return R.ok().put("certificateManagement", certificateManagement);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:certificatemanagement:save")
    public R save(@RequestBody CertificateManagementEntity certificateManagement){

        Date date = new Date();
        SysUserEntity user= ShiroUtils.getUserEntity();
        String createUsername = user.getUsername();
        Long company_name = user.getAffiliatedCompanyId();

        certificateManagement.setCreateTime(date);
        certificateManagement.setCreateUserId(createUsername);
        certificateManagement.setAffiliatedCompanyId(company_name);
		certificateManagementService.save(certificateManagement);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation("信息修改")
    @PostMapping("/update")
    @RequiresPermissions("generator:certificatemanagement:update")
    public R update(@RequestBody CertificateManagementEntity certificateManagement){
		certificateManagementService.updateById(certificateManagement);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:certificatemanagement:delete")
    public R delete(@RequestBody Long[] certificateIds){
		certificateManagementService.removeByIds(Arrays.asList(certificateIds));

        return R.ok();
    }

    /**
     * 预计到期证书
     */
    @ApiOperation("预计到期证书")
    @GetMapping("/linkCertificateExpected")
    public R linkCertificateExpected(@RequestParam Map<String, Object> params,@RequestParam("certificateId") Long certificateId){

        params.put("certificateId", certificateId);

        PageUtils page = certificateManagementService.queryCertificatePage(params);
        return R.ok().put("page", page);
    }

}
