package io.renren.modules.generator.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.Constant;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.generator.entity.ContractInfoEntity;
import io.renren.modules.generator.service.ContractInfoService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * 合同信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Api(tags = "合同信息表")
@RestController
@RequestMapping("generator/contractinfo")
public class ContractInfoController extends ShiroUtils {
    @Autowired
    private ContractInfoService contractInfoService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 列表
     */
    @ApiOperation("列表")
    @GetMapping("/list")
    @RequiresPermissions("generator:contractinfo:list")
    public R list(@RequestParam Map<String, Object> params){

        //如果不是超级管理员，则只查询自己所拥有的角色列表
//        if(getUserId() != Constant.SUPER_ADMIN){
//            Long userId= ShiroUtils.getUserId();
//            params.put("userId",userId);
//        }
        //1.每个人登录后，有userid
        Long userId = ShiroUtils.getUserId();
        //2.查询该userid的companyid
        Optional.ofNullable(sysUserService.getOne(
                new QueryWrapper<SysUserEntity>().lambda().eq(SysUserEntity::getUserId, userId)))
                .filter(f -> f.getAffiliatedCompanyId() != null)
                .map(m -> params.put("companyId", m.getAffiliatedCompanyId()));
        //3.再用companyid查询合同表的数据，派遣表的数据，船员表的数据
//        PageUtils page = contractInfoService.queryObjectCntractInfo(params, userId);
        PageUtils page = contractInfoService.queryPage(params);
        return R.ok().put("page", page);
    }

//    /**
//     * 合同列表
//     */
//    @ApiOperation("合同列表")
//    @GetMapping("/select")
//    public R select(){
//        Map<String, Object> map = new HashMap<>();
//
//        SysUserEntity user= ShiroUtils.getUserEntity();
//        Long contract_id = user.getContractId();
//
//        //如果不是超级管理员，则只查询自己所拥有的角色列表
//        if(getUserId() != Constant.SUPER_ADMIN){
//            map.put("contract_id", contract_id);
//        }
//        List<ContractInfoEntity> list = (List<ContractInfoEntity>) contractInfoService.listByMap(map);
//
//        return R.ok().put("list", list);
//    }

    /**
     * 信息
     */
    @RequestMapping("/info/{contractId}")
    @RequiresPermissions("generator:contractinfo:info")
    public R info(@PathVariable("contractId") Long contractId){
		ContractInfoEntity contractInfo = contractInfoService.getById(contractId);

        return R.ok().put("contractInfo", contractInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("generator:contractinfo:save")
    public R save(@RequestBody ContractInfoEntity contractInfo){

        Date date = new Date();
        SysUserEntity user= ShiroUtils.getUserEntity();
        String createUsername = user.getUsername();
        Long affiliated_company_id = user.getAffiliatedCompanyId();

        contractInfo.setCreateTime(date);
        contractInfo.setCreateUserId(createUsername);
        contractInfo.setAffiliatedCompanyId(affiliated_company_id);

		contractInfoService.save(contractInfo);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("generator:contractinfo:update")
    public R update(@RequestBody ContractInfoEntity contractInfo){
		contractInfoService.updateById(contractInfo);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("generator:contractinfo:delete")
    public R delete(@RequestBody Long[] contractIds){
		contractInfoService.removeByIds(Arrays.asList(contractIds));

        return R.ok();
    }

    /**
     * 归档
     * @return
     */
    @PostMapping("fileHandle")
    @RequiresPermissions("generator:contractinfo:update")
    public  R fileHandle(@RequestBody Long[] contractIds){
        List<Long> idList = Arrays.asList(contractIds);
        idList.stream().forEach(id -> {
            ContractInfoEntity cie = new ContractInfoEntity();
            cie.setContractId(id);
            cie.setContractStatus(Constant.ContractInfoStatus.FILEHANDLE.getName());
            contractInfoService.updateById(cie);
        });
        return R.ok();
    }

    /**
     * 作废
     * @return
     */
    @PostMapping("invalidHandle")
    @RequiresPermissions("generator:contractinfo:delete")
    public  R invalidHandle(@RequestBody Long[] contractIds){
        List<Long> idList = Arrays.asList(contractIds);
        idList.stream().forEach(id -> {
            ContractInfoEntity cie = new ContractInfoEntity();
            cie.setContractId(id);
            cie.setContractStatus(Constant.ContractInfoStatus.INVALIDHANDLE.getName());
            contractInfoService.updateById(cie);
        });
        return R.ok();
    }

    /**
     * 预计到期合同
     */
    @ApiOperation("预计到期合同")
    @GetMapping("/linkContractExpected")
    public R linkContractExpected(@RequestParam Map<String, Object> params,@RequestParam("contractId") Long contractId){

        params.put("contractId", contractId);

        PageUtils page = contractInfoService.queryPage(params);
        return R.ok().put("page", page);
    }

}
