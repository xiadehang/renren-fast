/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.generator.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.generator.service.SelectHomeCountService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 首页
 *
 * @author Mark sunlightcs@gmail.com
 */
@Api(tags = "首页")
@RestController
@RequestMapping("generator/selectHomeCount")
public class SelectHomeCountController extends ShiroUtils {
	@Autowired
	private SelectHomeCountService selectHomeCountService;

	
	/**
	 * 首页
	 */
    @ApiOperation("首页信息")
    @GetMapping("/selectHomeCountList")
	public R selectHomeCountList(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();
		
		List<Map> selectHomeCountList = selectHomeCountService.selectHomeCount(affiliatedCompanyId);

		return R.ok().put("selectHomeCountList", selectHomeCountList);
	}

	/**
	 * 当前年度
	 */
	@ApiOperation("当前年度")
	@GetMapping("/currentYearCount")
	public R currentYearCount(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> currentYearCount = selectHomeCountService.currentYearCount(affiliatedCompanyId);

		return R.ok().put("currentYearCount", currentYearCount);
	}

	/**
	 * 上一年度
	 */
	@ApiOperation("上一年度")
	@GetMapping("/lastYearCount")
	public R lastYearCount(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> lastYearCount = selectHomeCountService.lastYearCount(affiliatedCompanyId);

		return R.ok().put("lastYearCount", lastYearCount);
	}

	/**
	 * 查询职务占比
	 */
	@ApiOperation("查询职务占比")
	@GetMapping("/selectPostPercent")
	public R selectPostPercent(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> selectPostPercent = selectHomeCountService.selectPostPercent(affiliatedCompanyId);

		return R.ok().put("selectPostPercent", selectPostPercent);
	}

	/**
	 * 查询状态占比
	 */
	@ApiOperation("查询状态占比")
	@GetMapping("/selectStatusPercent")
	public R selectStatusPercent(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> selectStatusPercent = selectHomeCountService.selectStatusPercent(affiliatedCompanyId);

		return R.ok().put("selectStatusPercent", selectStatusPercent);
	}

	/**
	 * 预计到期合同
	 */
	@ApiOperation("预计到期合同")
	@GetMapping("/contractExpected")
	public R contractExpected(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> contractExpected = selectHomeCountService.contractExpected(affiliatedCompanyId);

		return R.ok().put("contractExpected", contractExpected);
	}

	/**
	 * 预计到期证书
	 */
	@ApiOperation("预计到期证书")
	@GetMapping("/certificateExpected")
	public R certificateExpected(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> certificateExpected = selectHomeCountService.certificateExpected(affiliatedCompanyId);

		return R.ok().put("certificateExpected", certificateExpected);
	}

	/**
	 * 预计下船
	 */
	@ApiOperation("预计下船")
	@GetMapping("/disembarkationExpected")
	public R disembarkationExpected(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> disembarkationExpected = selectHomeCountService.disembarkationExpected(affiliatedCompanyId);

		return R.ok().put("disembarkationExpected", disembarkationExpected);
	}

	/**
	 * 已过期信息
	 */
	@ApiOperation("已过期信息")
	@GetMapping("/informationExpired")
	public R informationExpired(){

		SysUserEntity user= ShiroUtils.getUserEntity();
		Long affiliatedCompanyId = user.getAffiliatedCompanyId();

		List<Map> informationExpired = selectHomeCountService.informationExpired(affiliatedCompanyId);

		return R.ok().put("informationExpired", informationExpired);
	}
	
}
