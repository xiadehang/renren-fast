package io.renren.modules.generator.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalaryDetailsQuery {

    /**
     * 关键字
     */
    private String key;

    /**
     * 薪资信息ID
     */
    private Long salaryInfoId;

    /**
     * 船员ID
     */
    private Long crewId;
}
