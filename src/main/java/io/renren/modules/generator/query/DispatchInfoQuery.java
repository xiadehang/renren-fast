package io.renren.modules.generator.query;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DispatchInfoQuery {

    /**
     * 关键字
     */
    private String key;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 上船职务
     */
    private String aboardPost;

    /**
     * 派遣状态
     */
    private String dispatchStatus;

    /**
     * 派遣标识
     */
    @TableId
    private Long dispatchId;
}
