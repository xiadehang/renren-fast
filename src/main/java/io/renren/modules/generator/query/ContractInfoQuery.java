package io.renren.modules.generator.query;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractInfoQuery {

    /**
     * 关键字
     */
    private String key;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 合同类型
     */
    private String contractType;

    /**
     * 合同状态
     */
    private String contractStatus;

    /**
     * 合同标识
     */
    private Long contractId;
}
