package io.renren.modules.generator.query;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CertificateQuery {

    /**
     * 船员姓名
     */
    private String crewName;

    /**
     * 证书名称
     */
    private String certificateName;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 状态
     */
    private String certificateState;

    /**
     * 证件信息ID
     */
    @TableId
    private Long certificateId;
}
