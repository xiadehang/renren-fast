package io.renren.modules.generator.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceDetailsQuery {

    /**
     * 关键字
     */
    private String key;

    /**
     * 薪资信息ID
     */
    private Long insuranceInfoId;

    /**
     * 船员ID
     */
    private Long crewId;
}
