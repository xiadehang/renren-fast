package io.renren.modules.generator.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CrewInfoQuery {

    /**
     * 关键字
     */
    private String key;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 职务
     */
    private String crewPost;

    /**
     * 证书等级
     */
    private String certificateLevel;

    /**
     * 状态
     */
    private String crewStatus;

    /**
     * 年龄范围From
     */
    private Long ageFrom;

    /**
     * 年龄范围To
     */
    private Long ageTo;
}
