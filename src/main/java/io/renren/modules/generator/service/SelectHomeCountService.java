/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.generator.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.extension.service.IService;

import io.renren.modules.generator.entity.SelectHomeCountEntity;


/**
 * 角色
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface SelectHomeCountService extends IService<SelectHomeCountEntity> {

	/**
	 * 查询首页
	 */
	List<Map> selectHomeCount(Long affiliatedCompanyId);

	/**
	 * 查询当前年度
	 */
	List<Map> currentYearCount(Long affiliatedCompanyId);

	/**
	 * 查询上一年度
	 */
	List<Map> lastYearCount(Long affiliatedCompanyId);

	/**
	 * 查询职务占比
	 */
	List<Map> selectPostPercent(Long affiliatedCompanyId);

	/**
	 * 查询状态占比
	 */
	List<Map> selectStatusPercent(Long affiliatedCompanyId);

	/**
	 * 预计到期合同
	 */
	List<Map> contractExpected(Long affiliatedCompanyId);

	/**
	 * 预计到期证书
	 */
	List<Map> certificateExpected(Long affiliatedCompanyId);

	/**
	 * 预计下船
	 */
	List<Map> disembarkationExpected(Long affiliatedCompanyId);

	/**
	 * 已过期信息
	 */
	List<Map> informationExpired(Long affiliatedCompanyId);
}
