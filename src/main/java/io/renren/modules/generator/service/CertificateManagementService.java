package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.CertificateManagementEntity;

import java.util.Map;

/**
 * 证书信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
public interface CertificateManagementService extends IService<CertificateManagementEntity> {

    PageUtils queryCertificatePage(Map<String, Object> params);
}

