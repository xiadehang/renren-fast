package io.renren.modules.generator.service.impl;

import io.renren.modules.generator.dao.CrewInfoDao;
import io.renren.modules.generator.query.SalaryInfoQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.generator.dao.SalaryInfoDao;
import io.renren.modules.generator.entity.SalaryInfoEntity;
import io.renren.modules.generator.service.SalaryInfoService;


@Service("salaryInfoService")
@Slf4j
public class SalaryInfoServiceImpl extends ServiceImpl<SalaryInfoDao, SalaryInfoEntity> implements SalaryInfoService {

    @Autowired
    private SalaryInfoDao salaryInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        SalaryInfoQuery ciq = SalaryInfoQuery.builder()
                .companyId((Long) params.get("companyId"))
                .build();

        IPage<SalaryInfoEntity> page = salaryInfoDao.selectPage(
                new Query<SalaryInfoEntity>().getPage(params),
                new QueryWrapper<SalaryInfoEntity>().lambda()
                        //根据每个人的公司id查询，管理员没有绑定公司id
                        .eq(ciq.getCompanyId() != null, SalaryInfoEntity::getAffiliatedCompanyId, ciq.getCompanyId())
                        //倒序创建时间排序
                        .orderByDesc(SalaryInfoEntity::getCreateTime)
        );

        return new PageUtils(page);
    }

    @Override
    public List<Map> selectGrantCount() {
        return baseMapper.selectGrantCount();
    }

    @Override
    public List<Map> selectGrantTotal() {
        return baseMapper.selectGrantTotal();
    }
}