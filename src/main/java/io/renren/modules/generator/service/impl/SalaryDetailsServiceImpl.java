package io.renren.modules.generator.service.impl;

import io.renren.modules.generator.dao.CertificateManagementDao;
import io.renren.modules.generator.entity.CertificateManagementEntity;
import io.renren.modules.generator.query.CertificateQuery;
import io.renren.modules.generator.query.SalaryDetailsQuery;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.generator.dao.SalaryDetailsDao;
import io.renren.modules.generator.entity.SalaryDetailsEntity;
import io.renren.modules.generator.service.SalaryDetailsService;


@Service("salaryDetailsService")
public class SalaryDetailsServiceImpl extends ServiceImpl<SalaryDetailsDao, SalaryDetailsEntity> implements SalaryDetailsService {

    @Autowired
    private SalaryDetailsDao salaryDetailsDao;

    @Override
    public PageUtils querySalaryDetailsPage(Map<String, Object> params) {

        SalaryDetailsQuery ciq = SalaryDetailsQuery.builder()
                .key(params.get("key") != null ? params.get("key").toString() : null)
                .salaryInfoId((Long) params.get("salaryInfoId"))
                .crewId((Long) params.get("crewId"))
                .build();

        //条件构造器对象
        QueryWrapper<SalaryDetailsEntity> queryWrapper = new QueryWrapper<SalaryDetailsEntity>();

        //入参
        if (ciq.getSalaryInfoId() != null) {
            queryWrapper.like("crew_name", ciq.getKey());
            queryWrapper.eq("salary_info_id", ciq.getSalaryInfoId());
        }


        IPage<SalaryDetailsEntity> page = this.salaryDetailsDao.querySalaryDetailsPage(new Query<SalaryDetailsEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

}