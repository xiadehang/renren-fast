package io.renren.modules.generator.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.generator.dao.InsuranceInfoDao;
import io.renren.modules.generator.entity.InsuranceInfoEntity;
import io.renren.modules.generator.query.InsuranceInfoQuery;
import io.renren.modules.generator.service.InsuranceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("insuranceInfoService")
public class InsuranceInfoServiceImpl extends ServiceImpl<InsuranceInfoDao, InsuranceInfoEntity> implements InsuranceInfoService {

    @Autowired
    private InsuranceInfoDao insuranceInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        InsuranceInfoQuery ciq = InsuranceInfoQuery.builder()
                .companyId((Long) params.get("companyId"))
                .build();

        IPage<InsuranceInfoEntity> page = insuranceInfoDao.selectPage(
                new Query<InsuranceInfoEntity>().getPage(params),
                new QueryWrapper<InsuranceInfoEntity>().lambda()
                        //根据每个人的公司id查询，管理员没有绑定公司id
                        .eq(ciq.getCompanyId() != null, InsuranceInfoEntity::getAffiliatedCompanyId, ciq.getCompanyId())
                        //倒序创建时间排序
                        .orderByDesc(InsuranceInfoEntity::getCreateTime)
        );

        return new PageUtils(page);
    }

    @Override
    public List<Map> selectPayCount() {
        return baseMapper.selectPayCount();
    }

    @Override
    public List<Map> selectPayTotal() {
        return baseMapper.selectPayTotal();
    }
}