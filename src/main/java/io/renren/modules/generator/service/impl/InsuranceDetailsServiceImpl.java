package io.renren.modules.generator.service.impl;

import io.renren.modules.generator.dao.InsuranceDetailsDao;
import io.renren.modules.generator.entity.InsuranceDetailsEntity;
import io.renren.modules.generator.query.InsuranceDetailsQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.generator.dao.InsuranceDetailsDao;
import io.renren.modules.generator.entity.InsuranceDetailsEntity;
import io.renren.modules.generator.service.InsuranceDetailsService;


@Service("insuranceDetailsService")
public class InsuranceDetailsServiceImpl extends ServiceImpl<InsuranceDetailsDao, InsuranceDetailsEntity> implements InsuranceDetailsService {

    @Autowired
    private InsuranceDetailsDao InsuranceDetailsDao;

    @Override
    public PageUtils queryInsuranceDetailsPage(Map<String, Object> params) {

        InsuranceDetailsQuery ciq = InsuranceDetailsQuery.builder()
                .key(params.get("key") != null ? params.get("key").toString() : null)
                .insuranceInfoId((Long) params.get("insuranceInfoId"))
                .crewId((Long) params.get("crewId"))
                .build();

        //条件构造器对象
        QueryWrapper<InsuranceDetailsEntity> queryWrapper = new QueryWrapper<InsuranceDetailsEntity>();

        //入参
        if (ciq.getInsuranceInfoId() != null) {
            queryWrapper.like("crew_name", ciq.getKey());
            queryWrapper.eq("insurance_info_id", ciq.getInsuranceInfoId());
        }

        IPage<InsuranceDetailsEntity> page = this.InsuranceDetailsDao.queryInsuranceDetailsPage(new Query<InsuranceDetailsEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

}