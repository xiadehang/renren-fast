package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.InsuranceInfoEntity;

import java.util.List;
import java.util.Map;

/**
 * 社保信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
public interface InsuranceInfoService extends IService<InsuranceInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 缴纳人数
     */
    List<Map> selectPayCount();

    /**
     * 缴纳总额
     */
    List<Map> selectPayTotal();
}

