package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.DispatchInfoEntity;

import java.util.Map;

/**
 * 派遣信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
public interface DispatchInfoService extends IService<DispatchInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
    PageUtils queryObjectDispatchInfo(Map<String, Object> params, Long userId);
}

