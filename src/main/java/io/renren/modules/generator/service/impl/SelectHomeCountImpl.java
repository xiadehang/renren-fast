/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren.modules.generator.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import io.renren.modules.generator.dao.SelectHomeCountDao;
import io.renren.modules.generator.entity.SelectHomeCountEntity;
import io.renren.modules.generator.service.SelectHomeCountService;
import lombok.extern.slf4j.Slf4j;

/**
 * 角色
 *
 * @author Mark sunlightcs@gmail.com
 */
@Service("selectHomeCountService")
@Slf4j
public class SelectHomeCountImpl extends ServiceImpl<SelectHomeCountDao, SelectHomeCountEntity> implements SelectHomeCountService {

    @Override
    public List<Map> selectHomeCount(Long affiliatedCompanyId) {
        return baseMapper.selectHomeCount(affiliatedCompanyId);
    }

    @Override
    public List<Map> currentYearCount(Long affiliatedCompanyI) {
        return baseMapper.currentYearCount(affiliatedCompanyI);
    }

    @Override
    public List<Map> lastYearCount(Long affiliatedCompanyI) {
        return baseMapper.lastYearCount(affiliatedCompanyI);
    }

    @Override
    public List<Map> selectPostPercent(Long affiliatedCompanyI) {
        return baseMapper.selectPostPercent(affiliatedCompanyI);
    }

    @Override
    public List<Map> selectStatusPercent(Long affiliatedCompanyI) {
        return baseMapper.selectStatusPercent(affiliatedCompanyI);
    }

    @Override
    public List<Map> contractExpected(Long affiliatedCompanyI) {
        return baseMapper.contractExpected(affiliatedCompanyI);
    }

    @Override
    public List<Map> certificateExpected(Long affiliatedCompanyI) {
        return baseMapper.certificateExpected(affiliatedCompanyI);
    }

    @Override
    public List<Map> disembarkationExpected(Long affiliatedCompanyI) {
        return baseMapper.disembarkationExpected(affiliatedCompanyI);
    }

    @Override
    public List<Map> informationExpired(Long affiliatedCompanyI) {
        return baseMapper.informationExpired(affiliatedCompanyI);
    }
}
