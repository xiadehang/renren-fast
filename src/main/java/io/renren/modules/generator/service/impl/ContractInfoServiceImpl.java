package io.renren.modules.generator.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.generator.dao.ContractInfoDao;
import io.renren.modules.generator.entity.ContractInfoEntity;
import io.renren.modules.generator.entity.CrewInfoEntity;
import io.renren.modules.generator.query.ContractInfoQuery;
import io.renren.modules.generator.service.ContractInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cn.hutool.core.date.DateUtil.today;


@Service("contractInfoService")
public class ContractInfoServiceImpl extends ServiceImpl<ContractInfoDao, ContractInfoEntity> implements ContractInfoService {

    @Autowired
    private ContractInfoDao contractInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        ContractInfoQuery ciq = ContractInfoQuery.builder()
                .key(params.get("key") != null ? params.get("key").toString() : null)
                .userId((Long) params.get("userId"))
                .companyId((Long) params.get("companyId"))
                .contractType(params.get("contractType") != null ? params.get("contractType").toString() : null)
                .contractStatus(params.get("contractStatus") != null ? params.get("contractStatus").toString() : null)
                .contractId((Long) params.get("contractId"))
                .build();

        IPage<ContractInfoEntity> page = contractInfoDao.selectPage(
                new Query<ContractInfoEntity>().getPage(params),
                new QueryWrapper<ContractInfoEntity>().lambda()
                        //根据每个人的公司id查询，管理员没有绑定公司id
                        .eq(ciq.getCompanyId() != null, ContractInfoEntity::getAffiliatedCompanyId, ciq.getCompanyId())
                        //关键字不为空则加入到sql
                        .and(StringUtils.isNotBlank(ciq.getKey()),
                                //船员姓名
                                i -> i.like(ContractInfoEntity::getStakeholder, ciq.getKey())
                                        //合同编号
                                        .or().like(ContractInfoEntity::getContractNum, ciq.getKey())
                                        //合同名称
                                        .or().like(ContractInfoEntity::getContractName, ciq.getKey()))
                        .and(StringUtils.isNotBlank(ciq.getContractType()),
                                //证书等级
                                i -> i.eq(ContractInfoEntity::getContractType, ciq.getContractType()))
                        .and(StringUtils.isNotBlank(ciq.getContractStatus()),
                                //状态
                                i -> i.eq(ContractInfoEntity::getContractStatus, ciq.getContractStatus()))
                        .eq(ciq.getContractId() != null, ContractInfoEntity::getContractId, ciq.getContractId())
                        //倒序创建时间排序
                        .orderByDesc(ContractInfoEntity::getCreateTime)
        );
//        page.getRecords().stream().forEach(f -> {
//            //计算距离到期时间
//            Optional.ofNullable(f.getEndTime()).ifPresent(endTime -> {
//                Date date1 = new Date();
//                Date date2 = new Date(String.valueOf(endTime));
//                if (date2.compareTo(date1) > 0) {
//                    f.setTimeToExpiration((int) DateUtil.betweenDay(new Date(), endTime, false) + 1);
//                } else {
//                    f.setTimeToExpiration(0);
//                }
//
//            });
//        });
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryObjectCntractInfo(Map<String, Object> params, Long userId) {
        IPage<ContractInfoEntity> page = this.page(new Query<ContractInfoEntity>().getPage(params));
        List<ContractInfoEntity> cntractInfoEntity = contractInfoDao.queryObjectCntractInfo(userId);
        page.setRecords(cntractInfoEntity);

        return new PageUtils(page);
    }
}