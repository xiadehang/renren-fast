package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.ContractInfoEntity;
import io.renren.modules.generator.entity.CrewInfoEntity;

import java.util.Map;

/**
 * 合同信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
public interface ContractInfoService extends IService<ContractInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryObjectCntractInfo(Map<String, Object> params, Long userId);
}

