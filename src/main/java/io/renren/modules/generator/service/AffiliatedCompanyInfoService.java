package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.AffiliatedCompanyInfoEntity;

import java.util.Map;

/**
 * 所属公司信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
public interface AffiliatedCompanyInfoService extends IService<AffiliatedCompanyInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

