package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.ExperienceInfoEntity;

import java.util.Map;

/**
 * 工作履历表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-12-04 21:48:38
 */
public interface ExperienceInfoService extends IService<ExperienceInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

