package io.renren.modules.generator.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.generator.dao.DataDictionaryDao;
import io.renren.modules.generator.entity.DataDictionaryEntity;
import io.renren.modules.generator.service.DataDictionaryService;


@Service("dataDictionaryService")
public class DataDictionaryServiceImpl extends ServiceImpl<DataDictionaryDao, DataDictionaryEntity> implements DataDictionaryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DataDictionaryEntity> page = this.page(
                new Query<DataDictionaryEntity>().getPage(params),
                new QueryWrapper<DataDictionaryEntity>()
        );

        return new PageUtils(page);
    }

}