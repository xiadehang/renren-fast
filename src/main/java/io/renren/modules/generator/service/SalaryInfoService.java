package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.SalaryInfoEntity;

import java.util.List;
import java.util.Map;

/**
 * 薪资信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
public interface SalaryInfoService extends IService<SalaryInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 发放人数
     */
    List<Map> selectGrantCount();

    /**
     * 发放总额
     */
    List<Map> selectGrantTotal();
}

