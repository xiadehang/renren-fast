package io.renren.modules.generator.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.generator.dao.AffiliatedCompanyInfoDao;
import io.renren.modules.generator.entity.AffiliatedCompanyInfoEntity;
import io.renren.modules.generator.service.AffiliatedCompanyInfoService;


@Service("affiliatedCompanyInfoService")
public class AffiliatedCompanyInfoServiceImpl extends ServiceImpl<AffiliatedCompanyInfoDao, AffiliatedCompanyInfoEntity> implements AffiliatedCompanyInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AffiliatedCompanyInfoEntity> page = this.page(
                new Query<AffiliatedCompanyInfoEntity>().getPage(params),
                new QueryWrapper<AffiliatedCompanyInfoEntity>()
        );

        return new PageUtils(page);
    }

}