package io.renren.modules.generator.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.generator.dao.CertificateManagementDao;
import io.renren.modules.generator.entity.CertificateManagementEntity;
import io.renren.modules.generator.entity.CertificateManagementEntity;
import io.renren.modules.generator.query.CertificateQuery;
import io.renren.modules.generator.service.CertificateManagementService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;
import java.util.Optional;


@Service("certificateManagementService")
@Slf4j
public class CertificateManagementServiceImpl extends ServiceImpl<CertificateManagementDao, CertificateManagementEntity> implements CertificateManagementService {

    @Autowired
    private CertificateManagementDao certificateManagementDao;

//    @Override
//    public PageUtils queryPage(Map<String, Object> params) {
//
//        CertificateQuery ciq = CertificateQuery.builder()
////                .crewName(params.get("CrewName") != null ? params.get("CrewName").toString() : null)
//                .certificateName(params.get("certificateName") != null ? params.get("certificateName").toString() : null)
//                .certificateState(params.get("certificateState") != null ? params.get("certificateState").toString() : null)
//                .build();
//
//        IPage<CertificateManagementEntity> page = certificateManagementDao.selectPage(
//                new Query<CertificateManagementEntity>().getPage(params),
//                new QueryWrapper<CertificateManagementEntity>().lambda()
//                        //根据每个人的公司id查询，管理员没有绑定公司id
//                        .eq(ciq.getCompanyId() != null, CertificateManagementEntity::getAffiliatedCompanyId, ciq.getCompanyId())
//                        //关键字不为空则加入到sql
////                        .and(StringUtils.isNotBlank(ciq.getCrewName()),
////                                //船员姓名
////                                i -> i.like(CertificateManagementEntity::getCrewName, ciq.getCrewName()))
//                        .and(StringUtils.isNotBlank(ciq.getCertificateName()),
//                                //证书名称
//                                i -> i.like(CertificateManagementEntity::getCertificateName, ciq.getCertificateName()))
//                        .and(StringUtils.isNotBlank(ciq.getCertificateState()),
//                                //状态
//                                i -> i.eq(CertificateManagementEntity::getCertificateState, ciq.getCertificateState()))
//                        //倒序创建时间排序
//                        .orderByDesc(CertificateManagementEntity::getCreateTime)
//        );
//        return new PageUtils(page);
//    }

    public PageUtils queryCertificatePage(Map<String, Object> params) {

        CertificateQuery ciq = CertificateQuery.builder()
                .companyId((Long) params.get("companyId"))
                .crewName(params.get("crewName") != null ? params.get("crewName").toString() : null)
                .certificateName(params.get("certificateName") != null ? params.get("certificateName").toString() : null)
                .certificateState(params.get("certificateState") != null ? params.get("certificateState").toString() : null)
                .certificateId((Long) params.get("certificateId"))
                .build();

        //条件构造器对象
        QueryWrapper<CertificateManagementEntity> queryWrapper = new QueryWrapper<CertificateManagementEntity>();

        //入参
        if (StringUtils.isNotBlank(ciq.getCrewName())) {
            queryWrapper.like("crew_name", ciq.getCrewName());
        }
        if (ciq.getCompanyId() != null) {
            queryWrapper.eq("cm.affiliated_company_id", ciq.getCompanyId());
        }
        if (StringUtils.isNotBlank(ciq.getCertificateName())) {
            queryWrapper.like("certificate_name", ciq.getCertificateName());
        }
        if (ciq.getCertificateId() != null) {
            queryWrapper.eq("cm.certificate_id", ciq.getCertificateId());
        }

        IPage<CertificateManagementEntity> page = this.certificateManagementDao.queryCertificatePage(new Query<CertificateManagementEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

}