package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.SalaryDetailsEntity;

import java.util.Map;

/**
 * 薪资详情表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
public interface SalaryDetailsService extends IService<SalaryDetailsEntity> {

    PageUtils querySalaryDetailsPage(Map<String, Object> params);
}

