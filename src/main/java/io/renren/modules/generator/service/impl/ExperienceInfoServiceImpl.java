package io.renren.modules.generator.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;

import io.renren.modules.generator.dao.ExperienceInfoDao;
import io.renren.modules.generator.entity.ExperienceInfoEntity;
import io.renren.modules.generator.service.ExperienceInfoService;


@Service("experienceInfoService")
public class ExperienceInfoServiceImpl extends ServiceImpl<ExperienceInfoDao, ExperienceInfoEntity> implements ExperienceInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ExperienceInfoEntity> page = this.page(
                new Query<ExperienceInfoEntity>().getPage(params),
                new QueryWrapper<ExperienceInfoEntity>()
        );

        return new PageUtils(page);
    }

}