package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.CrewInfoEntity;

import java.util.Map;

/**
 * 船员基本信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
public interface CrewInfoService extends IService<CrewInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

}

