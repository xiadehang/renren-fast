package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.InsuranceDetailsEntity;

import java.util.Map;

/**
 * 保险详情表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
public interface InsuranceDetailsService extends IService<InsuranceDetailsEntity> {

    PageUtils queryInsuranceDetailsPage(Map<String, Object> params);
}

