package io.renren.modules.generator.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.modules.generator.dao.DispatchInfoDao;
import io.renren.modules.generator.entity.CrewInfoEntity;
import io.renren.modules.generator.entity.DispatchInfoEntity;
import io.renren.modules.generator.query.DispatchInfoQuery;
import io.renren.modules.generator.service.DispatchInfoService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service("dispatchInfoService")
public class DispatchInfoServiceImpl extends ServiceImpl<DispatchInfoDao, DispatchInfoEntity> implements DispatchInfoService {

    @Autowired
    private DispatchInfoDao dspatchInfoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        DispatchInfoQuery ciq = DispatchInfoQuery.builder()
                .key(params.get("key") != null ? params.get("key").toString() : null)
                .userId((Long) params.get("userId"))
                .companyId((Long) params.get("companyId"))
                .aboardPost(params.get("aboardPost") != null ? params.get("aboardPost").toString() : null)
                .dispatchStatus(params.get("dispatchStatus") != null ? params.get("dispatchStatus").toString() : null)
                .dispatchId((Long) params.get("dispatchId"))
                .build();

        IPage<DispatchInfoEntity> page = dspatchInfoDao.selectPage(
                new Query<DispatchInfoEntity>().getPage(params),
                new QueryWrapper<DispatchInfoEntity>().lambda()
                        //根据每个人的公司id查询，管理员没有绑定公司id
                        .eq(ciq.getCompanyId() != null, DispatchInfoEntity::getAffiliatedCompanyId, ciq.getCompanyId())
                        //关键字不为空则加入到sql
                        .and(StringUtils.isNotBlank(ciq.getKey()),
                                //船员
                                i -> i.like(DispatchInfoEntity::getDispatchCrew, ciq.getKey())
                                        //船东名称
                                        .or().like(DispatchInfoEntity::getShipownerName, ciq.getKey())
                                        //船舶名称
                                        .or().like(DispatchInfoEntity::getVesselName, ciq.getKey()))
                        .and(StringUtils.isNotBlank(ciq.getAboardPost()),
                                //上船职务
                                i -> i.eq(DispatchInfoEntity::getAboardPost, ciq.getAboardPost()))
                        .and(StringUtils.isNotBlank(ciq.getDispatchStatus()),
                                //派遣状态
                                i -> i.eq(DispatchInfoEntity::getDispatchStatus, ciq.getDispatchStatus()))
                        .eq(ciq.getDispatchId() != null, DispatchInfoEntity::getDispatchId, ciq.getDispatchId())
                        //倒序创建时间排序
                        .orderByDesc(DispatchInfoEntity::getCreateTime)
        );
        page.getRecords().stream().forEach(f -> {
            //计算在船时间
            Optional.ofNullable(f.getAboardTime()).ifPresent(aboardTime -> {
                Date date1 = new Date();
                Date date2 = new Date(String.valueOf(aboardTime));
                if (date1.compareTo(date2) > 0) {
                    f.setEstimateBoardTime(DateUtil.betweenDay(new Date(), aboardTime, false) + 1);
                } else {
                    f.setEstimateBoardTime((long) 0);
                }

            });
        });
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryObjectDispatchInfo(Map<String, Object> params, Long userId) {
        IPage<DispatchInfoEntity> page = this.page(new Query<DispatchInfoEntity>().getPage(params));
        List<DispatchInfoEntity> dispatchInfoEntity = dspatchInfoDao.queryObjectDispatchInfo(userId);
        page.setRecords(dispatchInfoEntity);

        return new PageUtils(page);
    }

}