package io.renren.modules.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.common.utils.PageUtils;
import io.renren.modules.generator.entity.DataDictionaryEntity;

import java.util.Map;

/**
 * 数据字典
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-30 20:48:09
 */
public interface DataDictionaryService extends IService<DataDictionaryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

