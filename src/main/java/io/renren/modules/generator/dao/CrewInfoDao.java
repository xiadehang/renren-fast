package io.renren.modules.generator.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.modules.generator.entity.ContractInfoEntity;
import io.renren.modules.generator.entity.CrewInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 船员基本信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Mapper
@Repository
public interface CrewInfoDao extends BaseMapper<CrewInfoEntity> {

    List<CrewInfoEntity> queryObjectByKey(Long userId);

    IPage<CrewInfoEntity> queryCrewInfoPage(IPage iPage, @Param(Constants.WRAPPER) Wrapper<CrewInfoEntity> wrapper);
}
