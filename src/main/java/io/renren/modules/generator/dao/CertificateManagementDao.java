package io.renren.modules.generator.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import io.renren.modules.generator.entity.CertificateManagementEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.generator.entity.ContractInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 证书信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Mapper
@Repository
public interface CertificateManagementDao extends BaseMapper<CertificateManagementEntity> {

    IPage<CertificateManagementEntity> queryCertificatePage(IPage iPage, @Param(Constants.WRAPPER) Wrapper<CertificateManagementEntity> wrapper);
}
