package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.InsuranceInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 社保信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Mapper
@Repository
public interface InsuranceInfoDao extends BaseMapper<InsuranceInfoEntity> {

    /**
     * 缴纳人数
     */
    List<Map> selectPayCount();

    /**
     * 缴纳总额
     */
    List<Map> selectPayTotal();
}
