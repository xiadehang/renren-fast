package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.SalaryInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 薪资信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Mapper
@Repository
public interface SalaryInfoDao extends BaseMapper<SalaryInfoEntity> {

    /**
     * 发放人数
     */
    List<Map> selectGrantCount();

    /**
     * 发放总额
     */
    List<Map> selectGrantTotal();
}
