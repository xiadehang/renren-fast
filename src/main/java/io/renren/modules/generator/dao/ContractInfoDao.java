package io.renren.modules.generator.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import io.renren.modules.generator.entity.ContractInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.sys.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 合同信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Mapper
@Repository
public interface ContractInfoDao extends BaseMapper<ContractInfoEntity> {

    List<ContractInfoEntity> queryObjectCntractInfo(Long userId);

    IPage<ContractInfoEntity> queryContractInfoPage(IPage iPage, @Param(Constants.WRAPPER) Wrapper<ContractInfoEntity> wrapper);
}
