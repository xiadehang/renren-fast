package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.DispatchInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 派遣信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Mapper
@Repository
public interface DispatchInfoDao extends BaseMapper<DispatchInfoEntity> {
    List<DispatchInfoEntity> queryObjectDispatchInfo(Long userId);
}
