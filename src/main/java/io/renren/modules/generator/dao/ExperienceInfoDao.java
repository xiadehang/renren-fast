package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.ExperienceInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 工作履历表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-12-04 21:48:38
 */
@Mapper
@Repository
public interface ExperienceInfoDao extends BaseMapper<ExperienceInfoEntity> {
	
}
