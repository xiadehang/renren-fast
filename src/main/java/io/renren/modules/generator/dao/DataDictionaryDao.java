package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.DataDictionaryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-30 20:48:09
 */
@Mapper
public interface DataDictionaryDao extends BaseMapper<DataDictionaryEntity> {
	
}
