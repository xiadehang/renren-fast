package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.AffiliatedCompanyInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 所属公司信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Mapper
public interface AffiliatedCompanyInfoDao extends BaseMapper<AffiliatedCompanyInfoEntity> {
	
}
