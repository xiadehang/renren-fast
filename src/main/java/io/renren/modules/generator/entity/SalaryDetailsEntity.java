package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 薪资详情表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Data
@TableName("salary_details")
public class SalaryDetailsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 薪资详情ID
	 */
	@TableId
	private Long salaryDetailsId;
	/**
	 * 薪资信息ID
	 */
	private Long salaryInfoId;
	/**
	 * 船员ID
	 */
	private Long crewId;
	/**
	 * 基本工资
	 */
	private String basePay;
	/**
	 * 加班工资
	 */
	private String overtimePay;
	/**
	 * 补贴
	 */
	private String subsidyPay;
	/**
	 * 奖金
	 */
	private String bonus;
	/**
	 * 社保
	 */
	private String socialInsurance;
	/**
	 * 个人所得税
	 */
	private String individualTax;
	/**
	 * 其他
	 */
	private String otherPay;

	/**
	 * 船员姓名
	 */
	@TableField(exist = false)
	private String crewName;
	/**
	 * 船员职务
	 */
	@TableField(exist = false)
	private String crewPost;
}
