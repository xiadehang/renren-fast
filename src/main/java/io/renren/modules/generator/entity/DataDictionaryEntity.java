package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 数据字典
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-30 20:48:09
 */
@Data
@TableName("data_dictionary")
public class DataDictionaryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 父节点，一级节点为0
	 */
	private Long parentId;
	/**
	 * 
	 */
	private String keyword;
	/**
	 * 
	 */
	private String value;
	/**
	 * 数据类别
	 */
	private String type;
	/**
	 * 排序
	 */
	private Integer orderNum;

}
