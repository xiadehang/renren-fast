package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 派遣信息表
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Data
@TableName("dispatch_info")
public class DispatchInfoEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 派遣标识
     */
    @TableId
    private Long dispatchId;
    /**
     * 派遣船员
     */
    private String dispatchCrew;
    /**
     * 船东名称
     */
    private String shipownerName;
    /**
     * 船舶名称
     */
    private String vesselName;
    /**
     * 上船职务
     */
    private String aboardPost;
    /**
     * 上船时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date aboardTime;
    /**
     * 下船时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date sailingTime;
    /**
     * 预计在船时间
     */
    private Long estimateBoardTime;
    /**
     * 预计下船时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date estimateDisembarkationTime;
    /**
     * 派遣状态
     */
    private String dispatchStatus;
    /**
     * 船舶类型
     */
    private String shipType;
    /**
     * 航区
     */
    private String navigationArea;
    /**
     * 载重吨位
     */
    private String deadweightTonnage;
    /**
     * 上船地点
     */
    private String ashoreSite;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 薪资
     */
    private String salaryEquirement;
    /**
     * 币种
     */
    private String currency;
    /**
     * 合同
     */
    private String contractId;
    /**
     * 所属公司
     */
    private Long affiliatedCompanyId;
    /**
     * 派遣人
     */
    private String createUserId;
    /**
     * 创建时间
     */
    private Date createTime;

}
