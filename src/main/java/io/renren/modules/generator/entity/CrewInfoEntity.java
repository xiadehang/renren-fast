package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 船员基本信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-20 22:18:45
 */
@Data
@TableName("crew_info")
public class CrewInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 船员标识
	 */
	@TableId
	private Long crewId;
	/**
	 * 船员编号
	 */
	private String crewNum;
	/**
	 * 姓名
	 */
	private String crewName;
	/**
	 * 职务
	 */
	private String crewPost;
	/**
	 * 船员状态
	 */
	private String crewStatus;
	/**
	 * 国籍
	 */
	private String nationality;
	/**
	 * 证书等级
	 */
	private String certificateLevel;
	/**
	 * 备注
	 */
	private String remarks;
	/**
	 * 手机号
	 */
	private String phoneNumber;
	/**
	 * 家庭地址
	 */
	private String homeAddress;
	/**
	 * 推荐人
	 */
	private String recommenderId;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date createTime;
	/**
	 * 出生地
	 */
	private String birthPlace;
	/**
	 * 证件类型
	 */
	private String documentType;
	/**
	 * 证件号
	 */
	private String documentNumber;
	/**
	 * 出生日期
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date birthday;
	/**
	 * 年龄
	 */
	private int age;
	/**
	 * 性别
	 */
	private Integer sex;
	/**
	 * 船员类型
	 */
	private String crewType;
	/**
	 * 政治面貌
	 */
	private String political;
	/**
	 * 婚姻状况
	 */
	private String maritalStatus;
	/**
	 * 血型
	 */
	private String bloodType;
	/**
	 * 航海经验
	 */
	private String experienceSailing;
	/**
	 * 遣返地
	 */
	private String repatriationArea;
	/**
	 * 特殊技能
	 */
	private String technicalSkill;
	/**
	 * 其他联络电话
	 */
	private String otherPhone;
	/**
	 * 通讯地址
	 */
	private String postalAddress;
	/**
	 * 电子邮箱
	 */
	private String email;
	/**
	 * 紧急联络人
	 */
	private String emergencyContact;
	/**
	 * 与本人关系
	 */
	private String relationship;
	/**
	 * 关系人手机号
	 */
	private String relationshipPhone;
	/**
	 * 关系人其他联络电话
	 */
	private String relationshipOtherPhone;
	/**
	 * 创建IP
	 */
	private String createUserIp;
	/**
	 * 所属公司
	 */
	private Long affiliatedCompanyId;
	/**
	 * 创建人
	 */
	private Long createUserId;
/**
	 * 毕业院校
	 */
	private String school;
	/**
	 * 最高学历
	 */
	private String education;
	/**
	 * 学历证号
	 */
	private String certificate;
	/**
	 * 毕业时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date graduationTime;
	/**
	 * 专业
	 */
	private String major;
	/**
	 * 英文水平
	 */
	private String englishProficiency;
	/**
	 * 其他语种
	 */
	private String otherLanguages;
	/**
	 * 语言水平
	 */
	private String otherLanguageProficiency;
	/**
	 * 寻求职务
	 */
	private String seekingJob;
	/**
	 * 船舶类型
	 */
	private String shipType;
	/**
	 * 航区
	 */
	private String navigationArea;
	/**
	 * 载重吨位
	 */
	private String deadweightTonnage;
	/**
	 * 薪资要求
	 */
	private String salaryEquirement;
	/**
	 * 币种
	 */
	private String currency;
	/**
	 * 其他要求
	 */
	private String otherRequirements;

	/**
	 * 履历列表
	 */
	@TableField(exist = false)
	private List<ExperienceInfoEntity> experience_list;

}
