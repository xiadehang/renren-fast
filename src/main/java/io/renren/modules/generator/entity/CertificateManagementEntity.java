package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 证书信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Data
@TableName("certificate_management")
public class CertificateManagementEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 证件信息ID
	 */
	@TableId
	private Long certificateId;
	/**
	 * 船员ID
	 */
	private Long crewId;
	/**
	 * 证书名称
	 */
	private String certificateName;
	/**
	 * 证书号码
	 */
	private String certificateNumber;
	/**
	 * 签发时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date certificateTime;
	/**
	 * 到期时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date expirationDate;
	/**
	 * 签发地
	 */
	private String placeIssue;
	/**
	 * 状态
	 */
	private String certificateState;
	/**
	 * 备注
	 */
	private String certificateRemarks;
	/**
	 * 创建人
	 */
	private String createUserId;
	/**
	 * 所属公司
	 */
	private Long affiliatedCompanyId;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date createTime;

	/**
	 * 船员姓名
	 */
	@TableField(exist = false)
	private String crewName;
	/**
	 * 船员职务
	 */
	@TableField(exist = false)
	private String crewPost;

}
