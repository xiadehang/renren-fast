package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 工作履历表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-12-04 21:48:38
 */
@Data
@TableName("experience_info")
public class ExperienceInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long workExperienceId;
	/**
	 * 
	 */
	private Long crewId;
	/**
	 * 起始时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date startTime;
	/**
	 * 终止时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date endTime;
	/**
	 * 职务
	 */
	private String crewPostex;
	/**
	 * 公司名称
	 */
	private String companyName;
	/**
	 * 船舶名称
	 */
	private String vesselName;
	/**
	 * 船东名称
	 */
	private String shipownerName;
	/**
	 * 船舶类型
	 */
	private String shipTypeex;
	/**
	 * 主机型号
	 */
	private String hostModel;
	/**
	 * 航区
	 */
	private String navigationAreaex;

}
