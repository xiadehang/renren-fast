package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 保险详情表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Data
@TableName("insurance_details")
public class InsuranceDetailsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 社保详情ID
	 */
	@TableId
	private Long insuranceDetailsId;
	/**
	 * 社保信息ID
	 */
	private Long insuranceInfoId;
	/**
	 * 船员ID
	 */
	private Long crewId;
	/**
	 * 养老保险
	 */
	private String endowmentInsurance;
	/**
	 * 医疗保险
	 */
	private String medicalInsurance;
	/**
	 * 失业保险
	 */
	private String unemploymentInsurance;
	/**
	 * 工伤保险
	 */
	private String employmentInsurance;
	/**
	 * 生育保险
	 */
	private String maternityInsurance;
	/**
	 * 公积金
	 */
	private String accumulationFund;
	/**
	 * 商业保险
	 */
	private String commercialInsurance;
	/**
	 * 其他
	 */
	private String otherInsurance;
	/**
	 * 船员姓名
	 */
	@TableField(exist = false)
	private String crewName;
	/**
	 * 船员职务
	 */
	@TableField(exist = false)
	private String crewPost;

}
