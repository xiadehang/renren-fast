package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 所属公司信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Data
@TableName("affiliated_company_info")
public class AffiliatedCompanyInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long affiliatedCompanyId;
	/**
	 * 公司名称
	 */
	private String companyName;
	/**
	 * 联络人
	 */
	private String contact;
	/**
	 * 联络电话
	 */
	private String contactNumber;
	/**
	 * 入网时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date admissionTime;
	/**
	 * 有效期截止时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date expiryDate;
	/**
	 * 管理船员人数上线
	 */
	private Integer administrators;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 最后修改人
	 */
	private String updateUser;
	/**
	 * 最后修改时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date updateDatetime;
	private Integer createUserId;
	private String createUserIp;
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date createTime;
	private String orbiddenReason;
	private String updateUserIp;

}
