package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 社保信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-01-06 21:40:37
 */
@Data
@TableName("insurance_info")
public class InsuranceInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 保险信息id
	 */
	@TableId
	private Long insuranceInfoId;
	/**
	 * 缴纳时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date insuranceTime;
	/**
	 * 名称
	 */
	private String insuranceName;
	/**
	 * 创建人
	 */
	private String createUserId;
	/**
	 * 所属公司
	 */
	private Long affiliatedCompanyId;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date createTime;

}
