package io.renren.modules.generator.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 合同信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2019-10-19 11:58:04
 */
@Data
@TableName("contract_info")
public class ContractInfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 合同标识
	 */
	@TableId
	private Long contractId;
	/**
	 * 合同干系人
	 */
	private String stakeholder;
	/**
	 * 合同编号
	 */
	private String contractNum;
	/**
	 * 合同类型
	 */
	private String contractType;
	/**
	 * 合同名称
	 */
	private String contractName;
	/**
	 * 起始时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date startTime;
	/**
	 * 截止时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date endTime;
	/**
	 * 合同状态
	 */
	private String contractStatus;
	/**
	 * 委托方（甲方）
	 */
	private String partyA;
	/**
	 * 受托方（乙方）
	 */
	private String partyB;
	/**
	 * 签订日期
	 */
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date signData;
	/**
	 * 创建人
	 */
	private String createUserId;
	/**
	 * 所属公司
	 */
	private Long affiliatedCompanyId;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date createTime;

	/**
	 * 所属公司名称
	 */
	@TableField(exist = false)
	private String companyName;

	/**
	 * 距离到期时间
	 */
	@TableField(exist = false)
	private int timeToExpiration;


}
