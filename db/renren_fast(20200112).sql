/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : renren_fast

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 12/01/2020 14:28:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for affiliated_company_info
-- ----------------------------
DROP TABLE IF EXISTS `affiliated_company_info`;
CREATE TABLE `affiliated_company_info`  (
  `affiliated_company_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司名称',
  `contact` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联络人',
  `contact_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联络电话',
  `admission_time` date NULL DEFAULT NULL COMMENT '入网时间',
  `expiry_date` date NULL DEFAULT NULL COMMENT '有效期截止时间',
  `administrators` int(20) NULL DEFAULT NULL COMMENT '管理船员人数上线',
  `status` int(20) NULL DEFAULT NULL COMMENT '状态',
  `update_user` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人',
  `update_datetime` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_user_id` int(20) NULL DEFAULT NULL COMMENT '创建人',
  `create_user_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人IP',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `orbidden_reason` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '禁用原因',
  `update_user_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后修改人IP',
  PRIMARY KEY (`affiliated_company_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '所属公司信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of affiliated_company_info
-- ----------------------------
INSERT INTO `affiliated_company_info` VALUES (1, '阿里', '2', '1', '2019-10-26', '2019-10-27', 2, 0, 'admin', '2019-10-27 22:29:23', 1, '1', '2019-10-27 16:00:00', '1', '1');
INSERT INTO `affiliated_company_info` VALUES (2, '腾讯', '1', '1', '2019-10-29', '2019-10-22', 1, 1, 'admin', '2019-10-27 22:30:09', 1, '1', '2019-10-21 16:00:00', '2', '1');
INSERT INTO `affiliated_company_info` VALUES (3, '华为', '2', '1', '2019-10-27', '2019-09-30', 1, 1, 'admin', '2019-10-27 22:33:49', 1, '1', '2019-10-28 16:00:00', '2', '1');
INSERT INTO `affiliated_company_info` VALUES (4, '新浪', '222', '222', '2019-10-31', '2019-11-27', 2, 0, 'admin', '2019-11-03 14:15:39', 22, '22', '2019-11-26 16:00:00', '22', '22');

-- ----------------------------
-- Table structure for certificate_management
-- ----------------------------
DROP TABLE IF EXISTS `certificate_management`;
CREATE TABLE `certificate_management`  (
  `certificate_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '证件信息ID',
  `crew_id` bigint(20) NULL DEFAULT NULL COMMENT '船员ID',
  `certificate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证书名称',
  `certificate_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证书号码',
  `certificate_time` datetime(0) NULL DEFAULT NULL COMMENT '签发时间',
  `expiration_date` datetime(0) NULL DEFAULT NULL COMMENT '到期时间',
  `place_issue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签发地',
  `certificate_state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `certificate_remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `affiliated_company_id` int(20) NULL DEFAULT NULL COMMENT '所属公司',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`certificate_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '证书信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of certificate_management
-- ----------------------------
INSERT INTO `certificate_management` VALUES (1, 22, '11', '11', '2020-01-28 00:00:00', '2020-01-01 00:00:00', '3', '3', '', NULL, NULL, NULL);
INSERT INTO `certificate_management` VALUES (2, 11, '22', '33', '2020-01-28 00:00:00', '2020-01-28 00:00:00', '3333', '11', '22', 'admin', 1, '2020-01-10 08:26:39');
INSERT INTO `certificate_management` VALUES (3, NULL, 'zheng2', '222', '2020-01-06 00:00:00', '2020-01-29 00:00:00', '', '22', '', 'admin', 1, '2020-01-12 09:12:40');

-- ----------------------------
-- Table structure for contract_info
-- ----------------------------
DROP TABLE IF EXISTS `contract_info`;
CREATE TABLE `contract_info`  (
  `contract_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '合同标识',
  `stakeholder` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同干系人',
  `contract_num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同编号',
  `contract_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同类型',
  `contract_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同名称',
  `start_time` date NULL DEFAULT NULL COMMENT '起始时间',
  `end_time` date NULL DEFAULT NULL COMMENT '截止时间',
  `contract_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同状态',
  `party_a` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '委托方（甲方）',
  `party_b` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '受托方（乙方）',
  `sign_data` date NULL DEFAULT NULL COMMENT '签订日期',
  `create_user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `affiliated_company_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属公司',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`contract_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '合同信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contract_info
-- ----------------------------
INSERT INTO `contract_info` VALUES (12, '小王', '', '劳动合同', '222', '2011-06-27', '2019-12-15', '续签', '22', '22', '2019-11-29', 'admin', '1', '2019-11-03 14:09:52');
INSERT INTO `contract_info` VALUES (13, '小鹿', '0001', '委托合同', '0000111', '2019-12-24', '2019-12-31', '作废', '22', '222', NULL, 'admin', NULL, '2019-12-08 13:31:02');
INSERT INTO `contract_info` VALUES (15, '444', '0003', '其他合同', 'woi', '2019-12-01', '2019-12-12', '归档', 'aaa', 'bbb', '2019-12-26', 'admin', NULL, '2019-12-08 16:07:26');

-- ----------------------------
-- Table structure for crew_info
-- ----------------------------
DROP TABLE IF EXISTS `crew_info`;
CREATE TABLE `crew_info`  (
  `crew_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '船员标识',
  `crew_num` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船员编号',
  `crew_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `crew_post` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务',
  `crew_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船员状态',
  `nationality` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '国籍',
  `certificate_level` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证书等级',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `phone_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `home_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '家庭地址',
  `recommender_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `birth_place` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '出生地',
  `document_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证件类型',
  `document_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '证件号',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `age` int(20) NULL DEFAULT NULL COMMENT '年龄',
  `sex` tinyint(4) NULL DEFAULT NULL COMMENT '性别 0：男   1：女',
  `crew_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船员类型',
  `political` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '政治面貌',
  `marital_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '婚姻状况',
  `blood_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '血型',
  `experience_sailing` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '航海经验',
  `repatriation_area` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '遣返地',
  `technical_skill` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '特殊技能',
  `other_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他联络电话',
  `postal_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通讯地址',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `emergency_contact` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '紧急联络人',
  `relationship` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '与本人关系',
  `relationship_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关系人手机号',
  `relationship_other_phone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关系人其他联络电话',
  `create_user_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建IP',
  `affiliated_company_id` int(20) NULL DEFAULT NULL COMMENT '所属公司',
  `create_user_id` int(20) NULL DEFAULT NULL COMMENT '创建人',
  `school` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '毕业院校',
  `education` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最高学历',
  `certificate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学历证号',
  `graduation_time` date NULL DEFAULT NULL COMMENT '毕业时间',
  `major` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
  `english_proficiency` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '英文水平',
  `other_languages` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他语种',
  `other_language_proficiency` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语言水平',
  `seeking_job` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '寻求职务',
  `ship_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船舶类型',
  `navigation_area` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '航区',
  `deadweight_tonnage` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '载重吨位',
  `salary_equirement` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '薪资要求',
  `currency` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '币种',
  `other_requirements` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他要求',
  PRIMARY KEY (`crew_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '船员基本信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of crew_info
-- ----------------------------
INSERT INTO `crew_info` VALUES (12, '0001', '小刘', '船长', '离职', '', '', '', '222', '', '', '2019-11-03 15:41:28', '', '居民身份证', '222', '2018-11-20', NULL, 1, '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (13, '', '222', '船长', '已填报', '', '', '', '111', '', '', '2019-11-03 15:54:10', '', '居民身份证', '222', '2018-11-20', NULL, 0, '', '', '', '', '', '', '', '', '222', NULL, '222', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (14, '0008', '小鹿', '船长', '已填报', '', '', '', '11', '', '', '2019-11-03 16:33:47', '', '居民身份证', '222', '2019-11-27', NULL, 0, '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (15, '', '444', '船长', '已填报', '', '', '', '222', '', '', '2019-11-03 16:37:55', '', '居民身份证', '2222', '2012-11-27', NULL, 0, '', '', '', '', '', '', '', '', '', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (16, '11', '212121', '实习船长', '已填报', '', '', '', '222', '', '', '2019-11-03 16:45:39', '', '居民身份证', '2222', '2016-11-27', NULL, 0, '', '', '', '', '', '', '', '', '222', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (17, '222', '22', '二副', '离职', '', '', '', '22', '', '', '2019-11-03 16:46:07', '', '其他证件', '222', '2017-11-27', NULL, 0, '', '', '', '', '', '', '', '', '22', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (18, '', '2311121', '二副', '已填报', '', '', '', '22', '', '', '2019-11-03 16:50:17', '', '居民身份证', '222', '2017-11-27', NULL, 0, '', '', '', '', '', '', '', '', '555', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (19, '', '3131', '二副', '离职', '', '', '', '22', '', '', '2019-11-03 16:51:42', '', '其他证件', '222', '2015-11-27', NULL, 0, '', '', '', '', '', '', '', '', '111', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (20, '', '666', '二副', '待派遣', '', '', '', '666', '', '', '2019-11-03 16:52:24', '', '其他证件', '666', '2016-11-27', NULL, 0, '', '', '', '', '', '', '', '', 'aaaa', NULL, '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (21, '', '3333', '大副', '离职', '', '', '', '22', '', '', '2019-11-03 17:01:23', '', '居民身份证', '22', '2019-11-27', NULL, 0, '', '', '', '', '', '', '', '', '11', '333', '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', '2019-11-27', '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (22, '0001', '王小1111111111111111111111', '船长', '已填报', '', '甲类', '', '22', '', 'aaa', '2019-12-08 09:11:14', '', '居民身份证', '2222', '2011-06-07', NULL, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0:0:0:0:0:0:0:1', 2, 2, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (23, '111', '大大大', '实习船长', '待上船', '', '', '', '222222', '', '', '2020-01-07 21:29:43', '', '居民身份证', '11111111111111111', '2020-01-21', -1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0:0:0:0:0:0:0:1', 1, 1, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (24, '111', '大大大', '实习船长', '待上船', '', '', '', '222222', '', '', '2020-01-07 21:30:25', '', '居民身份证', '11111111111111111', '2020-01-21', -1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0:0:0:0:0:0:0:1', 1, 1, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (25, '111', '大大大', '实习船长', '待上船', '', '', '', '222222', '', '', '2020-01-07 21:31:20', '', '居民身份证', '11111111111111111', '2020-01-21', -1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0:0:0:0:0:0:0:1', 1, 1, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (26, '2121', '222', '实习大副', '已填报', '', '', '', '33', '', '', '2020-01-07 21:49:01', '', '其他证件', '222', NULL, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0:0:0:0:0:0:0:1', 1, 1, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (27, '2312', '22', '实习大副', '待派遣', '', '', '', '33', '', '', '2020-01-07 21:57:54', '', '居民身份证', '222', NULL, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0:0:0:0:0:0:0:1', 1, 1, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');
INSERT INTO `crew_info` VALUES (28, '5554', '21', '船长', '已填报', '', '', '', '22', '', '', '2020-01-07 22:12:50', '', '居民身份证', '22', '2019-01-01', 1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0:0:0:0:0:0:0:1', 1, 1, '', '', '', NULL, '', '', '', '', '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `data_dictionary`;
CREATE TABLE `data_dictionary`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父节点，一级节点为0',
  `keyword` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `value` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据类别',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 244 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '数据字典' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of data_dictionary
-- ----------------------------
INSERT INTO `data_dictionary` VALUES (1, NULL, '', '职务', '', 0);
INSERT INTO `data_dictionary` VALUES (2, NULL, '', '船员状态', '', 1);
INSERT INTO `data_dictionary` VALUES (3, NULL, '', '国籍', '', 2);
INSERT INTO `data_dictionary` VALUES (4, NULL, '', '出生地', '', 3);
INSERT INTO `data_dictionary` VALUES (5, NULL, '', '证件类型', '', 4);
INSERT INTO `data_dictionary` VALUES (6, NULL, '', '性别', '', 5);
INSERT INTO `data_dictionary` VALUES (7, NULL, NULL, '证书等级', NULL, 6);
INSERT INTO `data_dictionary` VALUES (8, NULL, NULL, '船员类型', NULL, 7);
INSERT INTO `data_dictionary` VALUES (9, NULL, NULL, '政治面貌', NULL, 8);
INSERT INTO `data_dictionary` VALUES (10, NULL, NULL, '婚姻状况', NULL, 9);
INSERT INTO `data_dictionary` VALUES (11, NULL, NULL, '血型', NULL, 10);
INSERT INTO `data_dictionary` VALUES (12, NULL, NULL, '与本人关系', NULL, 11);
INSERT INTO `data_dictionary` VALUES (13, NULL, NULL, '学历', NULL, 12);
INSERT INTO `data_dictionary` VALUES (14, NULL, NULL, '英文水平', NULL, 13);
INSERT INTO `data_dictionary` VALUES (15, NULL, NULL, '船舶类型', NULL, 14);
INSERT INTO `data_dictionary` VALUES (16, NULL, NULL, '航区', NULL, 15);
INSERT INTO `data_dictionary` VALUES (17, NULL, NULL, '管理级别', NULL, 240);
INSERT INTO `data_dictionary` VALUES (18, NULL, NULL, '币种', NULL, 17);
INSERT INTO `data_dictionary` VALUES (19, NULL, NULL, '启用状态', NULL, 18);
INSERT INTO `data_dictionary` VALUES (20, NULL, NULL, '派遣状态', NULL, 19);
INSERT INTO `data_dictionary` VALUES (21, NULL, NULL, '合同类型', NULL, 20);
INSERT INTO `data_dictionary` VALUES (22, NULL, NULL, '合同状态', NULL, 21);
INSERT INTO `data_dictionary` VALUES (23, 1, NULL, '船长', NULL, 22);
INSERT INTO `data_dictionary` VALUES (24, 1, NULL, '实习船长', NULL, 23);
INSERT INTO `data_dictionary` VALUES (25, 1, NULL, '大副', NULL, 24);
INSERT INTO `data_dictionary` VALUES (26, 1, NULL, '实习大副', NULL, 25);
INSERT INTO `data_dictionary` VALUES (27, 1, NULL, '二副', NULL, 26);
INSERT INTO `data_dictionary` VALUES (28, 1, NULL, '三副', NULL, 27);
INSERT INTO `data_dictionary` VALUES (29, 1, NULL, '操作员', NULL, 28);
INSERT INTO `data_dictionary` VALUES (30, 1, NULL, '驾驶员助理', NULL, 29);
INSERT INTO `data_dictionary` VALUES (31, 1, NULL, '轮机长', NULL, 30);
INSERT INTO `data_dictionary` VALUES (32, 1, NULL, '实习轮机长', NULL, 31);
INSERT INTO `data_dictionary` VALUES (33, 1, NULL, '大管轮', NULL, 32);
INSERT INTO `data_dictionary` VALUES (34, 1, NULL, '实习大管轮', NULL, 33);
INSERT INTO `data_dictionary` VALUES (35, 1, NULL, '电机员', NULL, 34);
INSERT INTO `data_dictionary` VALUES (36, 1, NULL, '实习电机员', NULL, 35);
INSERT INTO `data_dictionary` VALUES (37, 1, NULL, '电工', NULL, 36);
INSERT INTO `data_dictionary` VALUES (38, 1, NULL, '轮机员助理', NULL, 37);
INSERT INTO `data_dictionary` VALUES (39, 1, NULL, '水手长', NULL, 38);
INSERT INTO `data_dictionary` VALUES (40, 1, NULL, '副水手长', NULL, 39);
INSERT INTO `data_dictionary` VALUES (41, 1, NULL, '甲板铜匠', NULL, 40);
INSERT INTO `data_dictionary` VALUES (42, 1, NULL, '木匠', NULL, 41);
INSERT INTO `data_dictionary` VALUES (43, 1, NULL, '一水', NULL, 42);
INSERT INTO `data_dictionary` VALUES (44, 1, NULL, '二水', NULL, 43);
INSERT INTO `data_dictionary` VALUES (45, 1, NULL, '水手', NULL, 44);
INSERT INTO `data_dictionary` VALUES (46, 1, NULL, '实习水手', NULL, 45);
INSERT INTO `data_dictionary` VALUES (47, 1, NULL, '甲板实习生', NULL, 46);
INSERT INTO `data_dictionary` VALUES (48, 1, NULL, '机工长', NULL, 47);
INSERT INTO `data_dictionary` VALUES (49, 1, NULL, '铜匠', NULL, 48);
INSERT INTO `data_dictionary` VALUES (50, 1, NULL, '泵匠', NULL, 49);
INSERT INTO `data_dictionary` VALUES (51, 1, NULL, '机工', NULL, 50);
INSERT INTO `data_dictionary` VALUES (52, 1, NULL, '实习机工', NULL, 51);
INSERT INTO `data_dictionary` VALUES (53, 1, NULL, '二机', NULL, 52);
INSERT INTO `data_dictionary` VALUES (54, 1, NULL, '轮机实习生', NULL, 53);
INSERT INTO `data_dictionary` VALUES (55, 1, NULL, '大厨', NULL, 54);
INSERT INTO `data_dictionary` VALUES (56, 1, NULL, '二厨', NULL, 55);
INSERT INTO `data_dictionary` VALUES (57, 1, NULL, '大台', NULL, 56);
INSERT INTO `data_dictionary` VALUES (58, 1, NULL, '二台', NULL, 57);
INSERT INTO `data_dictionary` VALUES (59, 1, NULL, '船医', NULL, 58);
INSERT INTO `data_dictionary` VALUES (60, 1, NULL, '海乘', NULL, 59);
INSERT INTO `data_dictionary` VALUES (61, 1, NULL, '荷官', NULL, 60);
INSERT INTO `data_dictionary` VALUES (62, 1, NULL, '演艺人员', NULL, 61);
INSERT INTO `data_dictionary` VALUES (63, 1, NULL, '操作员', NULL, 62);
INSERT INTO `data_dictionary` VALUES (64, 2, NULL, '已填报', NULL, 63);
INSERT INTO `data_dictionary` VALUES (65, 2, NULL, '待派遣', NULL, 64);
INSERT INTO `data_dictionary` VALUES (66, 2, NULL, '待上船', NULL, 65);
INSERT INTO `data_dictionary` VALUES (67, 2, NULL, '已上船', NULL, 66);
INSERT INTO `data_dictionary` VALUES (68, 2, NULL, '休假', NULL, 67);
INSERT INTO `data_dictionary` VALUES (69, 2, NULL, '离职', NULL, 68);
INSERT INTO `data_dictionary` VALUES (70, 3, NULL, '中国', NULL, 69);
INSERT INTO `data_dictionary` VALUES (71, 3, NULL, '日本', NULL, 70);
INSERT INTO `data_dictionary` VALUES (72, 3, NULL, '韩国', NULL, 71);
INSERT INTO `data_dictionary` VALUES (73, 3, NULL, '新加坡', NULL, 72);
INSERT INTO `data_dictionary` VALUES (74, 3, NULL, '中国香港', NULL, 73);
INSERT INTO `data_dictionary` VALUES (75, 3, NULL, '中国台湾', NULL, 74);
INSERT INTO `data_dictionary` VALUES (76, 3, NULL, '马来西亚', NULL, 75);
INSERT INTO `data_dictionary` VALUES (77, 3, NULL, '菲律宾', NULL, 76);
INSERT INTO `data_dictionary` VALUES (78, 3, NULL, '越南', NULL, 77);
INSERT INTO `data_dictionary` VALUES (79, 4, NULL, '北京', NULL, 78);
INSERT INTO `data_dictionary` VALUES (80, 4, NULL, '天津', NULL, 79);
INSERT INTO `data_dictionary` VALUES (81, 4, NULL, '河北', NULL, 80);
INSERT INTO `data_dictionary` VALUES (82, 4, NULL, '山西', NULL, 81);
INSERT INTO `data_dictionary` VALUES (83, 4, NULL, '内蒙古', NULL, 82);
INSERT INTO `data_dictionary` VALUES (84, 4, NULL, '辽宁', NULL, 83);
INSERT INTO `data_dictionary` VALUES (85, 4, NULL, '吉林', NULL, 84);
INSERT INTO `data_dictionary` VALUES (86, 4, NULL, '黑龙江', NULL, 85);
INSERT INTO `data_dictionary` VALUES (87, 4, NULL, '上海', NULL, 86);
INSERT INTO `data_dictionary` VALUES (88, 4, NULL, '江苏', NULL, 87);
INSERT INTO `data_dictionary` VALUES (89, 4, NULL, '浙江', NULL, 88);
INSERT INTO `data_dictionary` VALUES (90, 4, NULL, '安徽', NULL, 89);
INSERT INTO `data_dictionary` VALUES (91, 4, NULL, '福建', NULL, 90);
INSERT INTO `data_dictionary` VALUES (92, 4, NULL, '江西', NULL, 91);
INSERT INTO `data_dictionary` VALUES (93, 4, NULL, '山东', NULL, 92);
INSERT INTO `data_dictionary` VALUES (94, 4, NULL, '河南', NULL, 93);
INSERT INTO `data_dictionary` VALUES (95, 4, NULL, '湖北', NULL, 94);
INSERT INTO `data_dictionary` VALUES (96, 4, NULL, '湖南', NULL, 95);
INSERT INTO `data_dictionary` VALUES (97, 4, NULL, '广东', NULL, 96);
INSERT INTO `data_dictionary` VALUES (98, 4, NULL, '广西', NULL, 97);
INSERT INTO `data_dictionary` VALUES (99, 4, NULL, '海南', NULL, 98);
INSERT INTO `data_dictionary` VALUES (100, 4, NULL, '重庆', NULL, 99);
INSERT INTO `data_dictionary` VALUES (101, 4, NULL, '四川', NULL, 100);
INSERT INTO `data_dictionary` VALUES (102, 4, NULL, '贵州', NULL, 101);
INSERT INTO `data_dictionary` VALUES (103, 4, NULL, '云南', NULL, 102);
INSERT INTO `data_dictionary` VALUES (104, 4, NULL, '西藏', NULL, 103);
INSERT INTO `data_dictionary` VALUES (105, 4, NULL, '陕西', NULL, 104);
INSERT INTO `data_dictionary` VALUES (106, 4, NULL, '甘肃', NULL, 105);
INSERT INTO `data_dictionary` VALUES (107, 4, NULL, '青海', NULL, 106);
INSERT INTO `data_dictionary` VALUES (108, 4, NULL, '宁夏', NULL, 107);
INSERT INTO `data_dictionary` VALUES (109, 4, NULL, '香港', NULL, 108);
INSERT INTO `data_dictionary` VALUES (110, 4, NULL, '澳门', NULL, 109);
INSERT INTO `data_dictionary` VALUES (111, 4, NULL, '台湾', NULL, 110);
INSERT INTO `data_dictionary` VALUES (112, 5, NULL, '居民身份证', NULL, 111);
INSERT INTO `data_dictionary` VALUES (113, 5, NULL, '护照', NULL, 112);
INSERT INTO `data_dictionary` VALUES (114, 5, NULL, '其他证件', NULL, 113);
INSERT INTO `data_dictionary` VALUES (115, 6, NULL, '男', NULL, 114);
INSERT INTO `data_dictionary` VALUES (116, 6, NULL, '女', NULL, 115);
INSERT INTO `data_dictionary` VALUES (117, 7, NULL, '甲类', NULL, 116);
INSERT INTO `data_dictionary` VALUES (118, 7, NULL, '乙一', NULL, 117);
INSERT INTO `data_dictionary` VALUES (119, 7, NULL, '乙二', NULL, 118);
INSERT INTO `data_dictionary` VALUES (120, 7, NULL, '丙一', NULL, 119);
INSERT INTO `data_dictionary` VALUES (121, 7, NULL, '丙二', NULL, 120);
INSERT INTO `data_dictionary` VALUES (122, 7, NULL, '丁类', NULL, 121);
INSERT INTO `data_dictionary` VALUES (123, 7, NULL, '长一', NULL, 122);
INSERT INTO `data_dictionary` VALUES (124, 7, NULL, '长二', NULL, 123);
INSERT INTO `data_dictionary` VALUES (125, 7, NULL, '内河一', NULL, 124);
INSERT INTO `data_dictionary` VALUES (126, 7, NULL, '内河二', NULL, 125);
INSERT INTO `data_dictionary` VALUES (127, 7, NULL, '内河三', NULL, 126);
INSERT INTO `data_dictionary` VALUES (128, 7, NULL, '其他', NULL, 127);
INSERT INTO `data_dictionary` VALUES (129, 8, NULL, '自有船员', NULL, 128);
INSERT INTO `data_dictionary` VALUES (130, 8, NULL, '自由船员', NULL, 129);
INSERT INTO `data_dictionary` VALUES (131, 8, NULL, '派遣船员', NULL, 130);
INSERT INTO `data_dictionary` VALUES (132, 8, NULL, '解约船员', NULL, 131);
INSERT INTO `data_dictionary` VALUES (133, 8, NULL, '社会船员', NULL, 132);
INSERT INTO `data_dictionary` VALUES (134, 8, NULL, '代管船员', NULL, 133);
INSERT INTO `data_dictionary` VALUES (135, 9, NULL, '中共党员', NULL, 134);
INSERT INTO `data_dictionary` VALUES (136, 9, NULL, '共青团员', NULL, 135);
INSERT INTO `data_dictionary` VALUES (137, 9, NULL, '群众', NULL, 136);
INSERT INTO `data_dictionary` VALUES (138, 9, NULL, '其他', NULL, 137);
INSERT INTO `data_dictionary` VALUES (139, 10, NULL, '未婚', NULL, 138);
INSERT INTO `data_dictionary` VALUES (140, 10, NULL, '已婚', NULL, 139);
INSERT INTO `data_dictionary` VALUES (141, 10, NULL, '离异', NULL, 140);
INSERT INTO `data_dictionary` VALUES (142, 10, NULL, '丧偶', NULL, 141);
INSERT INTO `data_dictionary` VALUES (143, 11, NULL, 'A型', NULL, 142);
INSERT INTO `data_dictionary` VALUES (144, 11, NULL, 'B型', NULL, 143);
INSERT INTO `data_dictionary` VALUES (145, 11, NULL, 'O型', NULL, 144);
INSERT INTO `data_dictionary` VALUES (146, 11, NULL, 'AB型', NULL, 145);
INSERT INTO `data_dictionary` VALUES (147, 12, NULL, '父子关系', NULL, 146);
INSERT INTO `data_dictionary` VALUES (148, 12, NULL, '母子关系', NULL, 147);
INSERT INTO `data_dictionary` VALUES (149, 12, NULL, '夫妻关系', NULL, 148);
INSERT INTO `data_dictionary` VALUES (150, 13, NULL, '研究生', NULL, 149);
INSERT INTO `data_dictionary` VALUES (151, 13, NULL, '大学本科', NULL, 150);
INSERT INTO `data_dictionary` VALUES (152, 13, NULL, '大专', NULL, 151);
INSERT INTO `data_dictionary` VALUES (153, 13, NULL, '高中', NULL, 152);
INSERT INTO `data_dictionary` VALUES (154, 13, NULL, '初中', NULL, 153);
INSERT INTO `data_dictionary` VALUES (155, 13, NULL, '小学', NULL, 154);
INSERT INTO `data_dictionary` VALUES (156, 14, NULL, '母语', NULL, 155);
INSERT INTO `data_dictionary` VALUES (157, 14, NULL, '优秀', NULL, 156);
INSERT INTO `data_dictionary` VALUES (158, 14, NULL, '中间级别', NULL, 157);
INSERT INTO `data_dictionary` VALUES (159, 14, NULL, '基础水平', NULL, 158);
INSERT INTO `data_dictionary` VALUES (160, 14, NULL, '完全不会', NULL, 159);
INSERT INTO `data_dictionary` VALUES (161, 15, NULL, '杂货船', NULL, 160);
INSERT INTO `data_dictionary` VALUES (162, 15, NULL, '木材船', NULL, 161);
INSERT INTO `data_dictionary` VALUES (163, 15, NULL, '冷藏船', NULL, 162);
INSERT INTO `data_dictionary` VALUES (164, 15, NULL, '运沙船', NULL, 163);
INSERT INTO `data_dictionary` VALUES (165, 15, NULL, '滚装船', NULL, 164);
INSERT INTO `data_dictionary` VALUES (166, 15, NULL, '多功能船', NULL, 165);
INSERT INTO `data_dictionary` VALUES (167, 15, NULL, '汽车运输船', NULL, 166);
INSERT INTO `data_dictionary` VALUES (168, 15, NULL, '散货船', NULL, 167);
INSERT INTO `data_dictionary` VALUES (169, 15, NULL, '重大件船', NULL, 168);
INSERT INTO `data_dictionary` VALUES (170, 15, NULL, '矿砂船', NULL, 169);
INSERT INTO `data_dictionary` VALUES (171, 15, NULL, '水泥船', NULL, 170);
INSERT INTO `data_dictionary` VALUES (172, 15, NULL, '散杂货船', NULL, 171);
INSERT INTO `data_dictionary` VALUES (173, 15, NULL, '甲板运输船', NULL, 172);
INSERT INTO `data_dictionary` VALUES (174, 15, NULL, '集装箱船', NULL, 173);
INSERT INTO `data_dictionary` VALUES (175, 15, NULL, '油船', NULL, 174);
INSERT INTO `data_dictionary` VALUES (176, 15, NULL, '油化船', NULL, 175);
INSERT INTO `data_dictionary` VALUES (177, 15, NULL, '成品油船', NULL, 176);
INSERT INTO `data_dictionary` VALUES (178, 15, NULL, '原油船', NULL, 177);
INSERT INTO `data_dictionary` VALUES (179, 15, NULL, '加油船', NULL, 178);
INSERT INTO `data_dictionary` VALUES (180, 15, NULL, '化学品船', NULL, 179);
INSERT INTO `data_dictionary` VALUES (181, 15, NULL, '液化气船', NULL, 180);
INSERT INTO `data_dictionary` VALUES (182, 15, NULL, '巨型油轮', NULL, 181);
INSERT INTO `data_dictionary` VALUES (183, 15, NULL, '超大型油轮', NULL, 182);
INSERT INTO `data_dictionary` VALUES (184, 15, NULL, '沥青船', NULL, 183);
INSERT INTO `data_dictionary` VALUES (185, 15, NULL, '普通客船', NULL, 184);
INSERT INTO `data_dictionary` VALUES (186, 15, NULL, '客滚船', NULL, 185);
INSERT INTO `data_dictionary` VALUES (187, 15, NULL, '高速客船', NULL, 186);
INSERT INTO `data_dictionary` VALUES (188, 15, NULL, '车容渡船', NULL, 187);
INSERT INTO `data_dictionary` VALUES (189, 15, NULL, '邮轮', NULL, 188);
INSERT INTO `data_dictionary` VALUES (190, 15, NULL, '游艇', NULL, 189);
INSERT INTO `data_dictionary` VALUES (191, 15, NULL, '渔船', NULL, 190);
INSERT INTO `data_dictionary` VALUES (192, 15, NULL, '驳船', NULL, 191);
INSERT INTO `data_dictionary` VALUES (193, 15, NULL, '拖船', NULL, 192);
INSERT INTO `data_dictionary` VALUES (194, 15, NULL, '挖砂船', NULL, 193);
INSERT INTO `data_dictionary` VALUES (195, 15, NULL, '工程船', NULL, 194);
INSERT INTO `data_dictionary` VALUES (196, 15, NULL, '半潜船', NULL, 195);
INSERT INTO `data_dictionary` VALUES (197, 15, NULL, '科考船', NULL, 196);
INSERT INTO `data_dictionary` VALUES (198, 15, NULL, '特种船', NULL, 197);
INSERT INTO `data_dictionary` VALUES (199, 16, NULL, '远洋', NULL, 198);
INSERT INTO `data_dictionary` VALUES (200, 16, NULL, '近海', NULL, 199);
INSERT INTO `data_dictionary` VALUES (201, 16, NULL, '中日韩', NULL, 200);
INSERT INTO `data_dictionary` VALUES (202, 16, NULL, '东南亚', NULL, 201);
INSERT INTO `data_dictionary` VALUES (203, 16, NULL, '南北线', NULL, 202);
INSERT INTO `data_dictionary` VALUES (204, 16, NULL, '渤海湾', NULL, 203);
INSERT INTO `data_dictionary` VALUES (205, 16, NULL, '港澳台', NULL, 204);
INSERT INTO `data_dictionary` VALUES (206, 16, NULL, '港口', NULL, 205);
INSERT INTO `data_dictionary` VALUES (207, 16, NULL, '进江', NULL, 206);
INSERT INTO `data_dictionary` VALUES (208, 16, NULL, '长江', NULL, 207);
INSERT INTO `data_dictionary` VALUES (209, 16, NULL, '内外兼营', NULL, 208);
INSERT INTO `data_dictionary` VALUES (210, 16, NULL, '两广', NULL, 209);
INSERT INTO `data_dictionary` VALUES (211, 16, NULL, '中东', NULL, 210);
INSERT INTO `data_dictionary` VALUES (212, 16, NULL, '欧美', NULL, 211);
INSERT INTO `data_dictionary` VALUES (213, 16, NULL, '近洋', NULL, 212);
INSERT INTO `data_dictionary` VALUES (214, 16, NULL, '无限航区', NULL, 213);
INSERT INTO `data_dictionary` VALUES (215, 16, NULL, '日韩', NULL, 214);
INSERT INTO `data_dictionary` VALUES (216, 16, NULL, '沿海', NULL, 215);
INSERT INTO `data_dictionary` VALUES (217, 16, NULL, '环球', NULL, 216);
INSERT INTO `data_dictionary` VALUES (218, 16, NULL, '南北美', NULL, 217);
INSERT INTO `data_dictionary` VALUES (226, 18, NULL, '人民币', NULL, 225);
INSERT INTO `data_dictionary` VALUES (227, 18, NULL, '美元', NULL, 226);
INSERT INTO `data_dictionary` VALUES (228, 19, NULL, '启用', NULL, 227);
INSERT INTO `data_dictionary` VALUES (229, 19, NULL, '禁用', NULL, 228);
INSERT INTO `data_dictionary` VALUES (230, 20, NULL, '待上船', NULL, 229);
INSERT INTO `data_dictionary` VALUES (231, 20, NULL, '已上船', NULL, 230);
INSERT INTO `data_dictionary` VALUES (232, 20, NULL, '已下船', NULL, 231);
INSERT INTO `data_dictionary` VALUES (233, 21, NULL, '劳务合同', NULL, 232);
INSERT INTO `data_dictionary` VALUES (234, 21, NULL, '派遣合同', NULL, 233);
INSERT INTO `data_dictionary` VALUES (235, 21, NULL, '委托合同', NULL, 234);
INSERT INTO `data_dictionary` VALUES (236, 21, NULL, '其他合同', NULL, 235);
INSERT INTO `data_dictionary` VALUES (237, 22, NULL, '有效', NULL, 236);
INSERT INTO `data_dictionary` VALUES (238, 22, NULL, '过期', NULL, 237);
INSERT INTO `data_dictionary` VALUES (239, 22, NULL, '作废', NULL, 238);
INSERT INTO `data_dictionary` VALUES (240, 22, NULL, '归档', NULL, 239);
INSERT INTO `data_dictionary` VALUES (241, 17, NULL, '系统级', NULL, 241);
INSERT INTO `data_dictionary` VALUES (242, 17, NULL, '公司级', NULL, 242);
INSERT INTO `data_dictionary` VALUES (243, 17, NULL, '员工级', NULL, 243);

-- ----------------------------
-- Table structure for dispatch_info
-- ----------------------------
DROP TABLE IF EXISTS `dispatch_info`;
CREATE TABLE `dispatch_info`  (
  `dispatch_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '派遣标识',
  `dispatch_crew` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '派遣船员',
  `shipowner_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船东名称',
  `vessel_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船舶名称',
  `aboard_post` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上船职务',
  `aboard_time` date NULL DEFAULT NULL COMMENT '上船时间',
  `sailing_time` date NULL DEFAULT NULL COMMENT '下船时间',
  `estimate_board_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预计在船时间',
  `estimate_disembarkation_time` date NULL DEFAULT NULL COMMENT '预计下船时间',
  `dispatch_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '派遣状态',
  `ship_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船舶类型',
  `navigation_area` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '航区',
  `deadweight_tonnage` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '载重吨位',
  `ashore_site` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上船地点',
  `remarks` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `salary_equirement` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '薪资',
  `currency` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '币种',
  `contract_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同',
  `affiliated_company_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属公司',
  `create_user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '派遣人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`dispatch_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '派遣信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dispatch_info
-- ----------------------------
INSERT INTO `dispatch_info` VALUES (12, '小张', '22', '', '实习船长', '2019-11-26', NULL, NULL, '2019-11-28', '已下船', '', '', '', '11', '', '', '', '', '2', 'test', '2019-11-02 16:28:19');
INSERT INTO `dispatch_info` VALUES (13, '22245', '晓东', '22', '船长', '2019-11-27', NULL, NULL, NULL, '待上船', '', '', '', '22', '', '', '', '', '1', 'admin', '2019-11-03 13:28:18');
INSERT INTO `dispatch_info` VALUES (14, 'xiao22', '222', '', '船长', '2019-11-25', NULL, NULL, '2019-11-15', '已上船', '', '', '', '22', '', '', '', '', NULL, 'admin', '2019-11-03 14:59:43');
INSERT INTO `dispatch_info` VALUES (17, '21', '44444', '', '船长', '2020-01-28', NULL, NULL, NULL, '待上船', '', '', '', '22', '', '', '', '', '1', 'admin', '2020-01-11 16:24:58');
INSERT INTO `dispatch_info` VALUES (18, '22', '333', '222', '船长', '2020-01-28', '2020-01-25', NULL, NULL, '已下船', '', '', '', '222', '', '', '', '', '1', 'admin', '2020-01-11 16:38:05');

-- ----------------------------
-- Table structure for experience_info
-- ----------------------------
DROP TABLE IF EXISTS `experience_info`;
CREATE TABLE `experience_info`  (
  `work_experience_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `crew_id` bigint(20) NULL DEFAULT NULL,
  `start_time` date NULL DEFAULT NULL COMMENT '起始时间',
  `end_time` date NULL DEFAULT NULL COMMENT '终止时间',
  `crew_post` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职务',
  `company_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司名称',
  `vessel_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船舶名称',
  `shipowner_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船东名称',
  `ship_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '船舶类型',
  `host_model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主机型号',
  `navigation_area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '航区',
  PRIMARY KEY (`work_experience_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作履历表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of experience_info
-- ----------------------------
INSERT INTO `experience_info` VALUES (1, NULL, '2020-01-08', '2020-01-24', NULL, '22', '33', '44', NULL, '55', NULL);
INSERT INTO `experience_info` VALUES (2, 28, '2020-01-02', '2020-01-10', NULL, '2', '2', '3', NULL, '3', NULL);

-- ----------------------------
-- Table structure for insurance_details
-- ----------------------------
DROP TABLE IF EXISTS `insurance_details`;
CREATE TABLE `insurance_details`  (
  `insurance_details_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '社保详情ID',
  `insurance_info_id` bigint(20) NULL DEFAULT NULL COMMENT '社保信息ID',
  `crew_id` bigint(20) NULL DEFAULT NULL COMMENT '船员ID',
  `endowment_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '养老保险',
  `medical_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医疗保险',
  `unemployment_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '失业保险',
  `employment_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工伤保险',
  `maternity_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生育保险',
  `accumulation_fund` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公积金',
  `commercial_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商业保险',
  `other_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他',
  PRIMARY KEY (`insurance_details_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '保险详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for insurance_info
-- ----------------------------
DROP TABLE IF EXISTS `insurance_info`;
CREATE TABLE `insurance_info`  (
  `insurance_info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '保险信息id',
  `insurance_time` datetime(0) NULL DEFAULT NULL COMMENT '缴纳时间',
  `insurance_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `create_user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `affiliated_company_id` int(20) NULL DEFAULT NULL COMMENT '所属公司',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`insurance_info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '社保信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of insurance_info
-- ----------------------------
INSERT INTO `insurance_info` VALUES (1, '2020-01-07 00:00:00', '222', 'admin', 1, '2020-01-11 15:59:44');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', NULL, 'io.renren.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000016D523224E87874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672656E72656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RenrenScheduler', 'DESKTOP-IPDMG4T1578810155431', 1578810292436, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', NULL, 1578810600000, -1, 5, 'WAITING', 'CRON', 1569113147000, 0, NULL, 2, 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002E696F2E72656E72656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000016D523224E87874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B0200007870000000000000000174000672656E72656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for salary_details
-- ----------------------------
DROP TABLE IF EXISTS `salary_details`;
CREATE TABLE `salary_details`  (
  `salary_details_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '薪资详情ID',
  `salary_info_id` bigint(20) NULL DEFAULT NULL COMMENT '薪资信息ID',
  `crew_id` bigint(20) NULL DEFAULT NULL COMMENT '船员ID',
  `base_pay` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '基本工资',
  `overtime_pay` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加班工资',
  `subsidy_pay` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '补贴',
  `bonus` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖金',
  `social_insurance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '社保',
  `individual_tax` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个人所得税',
  `other_pay` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其他',
  PRIMARY KEY (`salary_details_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '薪资详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for salary_info
-- ----------------------------
DROP TABLE IF EXISTS `salary_info`;
CREATE TABLE `salary_info`  (
  `salary_info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '薪资信息ID',
  `salary_time` datetime(0) NULL DEFAULT NULL COMMENT '发放时间',
  `salary_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `salary_class` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分类',
  `create_user_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `affiliated_company_id` int(20) NULL DEFAULT NULL COMMENT '所属公司',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`salary_info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '薪资信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of salary_info
-- ----------------------------
INSERT INTO `salary_info` VALUES (1, '2020-01-08 00:00:00', '22', '111', '', NULL, NULL);
INSERT INTO `salary_info` VALUES (2, '2020-01-10 00:00:00', '2233', '22333', 'admin', 1, '2020-01-11 15:56:04');

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES (1, 'testTask', 'renren', '0 0/30 * * * ?', 0, '参数测试', '2019-09-21 13:01:21');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log`  (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `job_id`(`job_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 288 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES (1, 1, 'testTask', 'renren', 0, NULL, 3, '2019-09-22 09:30:00');
INSERT INTO `schedule_job_log` VALUES (2, 1, 'testTask', 'renren', 0, NULL, 3, '2019-09-22 10:00:00');
INSERT INTO `schedule_job_log` VALUES (3, 1, 'testTask', 'renren', 0, NULL, 1, '2019-09-22 10:30:00');
INSERT INTO `schedule_job_log` VALUES (4, 1, 'testTask', 'renren', 0, NULL, 1, '2019-09-22 16:00:00');
INSERT INTO `schedule_job_log` VALUES (5, 1, 'testTask', 'renren', 0, NULL, 1, '2019-09-24 20:30:00');
INSERT INTO `schedule_job_log` VALUES (6, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-06 13:30:00');
INSERT INTO `schedule_job_log` VALUES (7, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-15 20:30:00');
INSERT INTO `schedule_job_log` VALUES (8, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-16 21:00:00');
INSERT INTO `schedule_job_log` VALUES (9, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-16 21:30:00');
INSERT INTO `schedule_job_log` VALUES (10, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-16 22:00:00');
INSERT INTO `schedule_job_log` VALUES (11, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-17 08:00:00');
INSERT INTO `schedule_job_log` VALUES (12, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-17 21:00:00');
INSERT INTO `schedule_job_log` VALUES (13, 1, 'testTask', 'renren', 0, NULL, 8, '2019-10-17 21:30:00');
INSERT INTO `schedule_job_log` VALUES (14, 1, 'testTask', 'renren', 0, NULL, 5, '2019-10-17 22:00:00');
INSERT INTO `schedule_job_log` VALUES (15, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-18 08:00:00');
INSERT INTO `schedule_job_log` VALUES (16, 1, 'testTask', 'renren', 0, NULL, 8, '2019-10-19 08:30:00');
INSERT INTO `schedule_job_log` VALUES (17, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-19 09:00:00');
INSERT INTO `schedule_job_log` VALUES (18, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-19 10:30:00');
INSERT INTO `schedule_job_log` VALUES (19, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-19 11:00:00');
INSERT INTO `schedule_job_log` VALUES (20, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-19 11:30:00');
INSERT INTO `schedule_job_log` VALUES (21, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-19 12:00:00');
INSERT INTO `schedule_job_log` VALUES (22, 1, 'testTask', 'renren', 0, NULL, 4, '2019-10-19 12:30:00');
INSERT INTO `schedule_job_log` VALUES (23, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-19 13:00:00');
INSERT INTO `schedule_job_log` VALUES (24, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-19 13:30:00');
INSERT INTO `schedule_job_log` VALUES (25, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-19 14:00:00');
INSERT INTO `schedule_job_log` VALUES (26, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-19 14:30:00');
INSERT INTO `schedule_job_log` VALUES (27, 1, 'testTask', 'renren', 0, NULL, 4, '2019-10-19 15:00:00');
INSERT INTO `schedule_job_log` VALUES (28, 1, 'testTask', 'renren', 0, NULL, 5, '2019-10-19 17:00:00');
INSERT INTO `schedule_job_log` VALUES (29, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-19 17:30:00');
INSERT INTO `schedule_job_log` VALUES (30, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-19 18:00:00');
INSERT INTO `schedule_job_log` VALUES (31, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-19 18:30:00');
INSERT INTO `schedule_job_log` VALUES (32, 1, 'testTask', 'renren', 0, NULL, 5, '2019-10-19 19:00:00');
INSERT INTO `schedule_job_log` VALUES (33, 1, 'testTask', 'renren', 0, NULL, 4, '2019-10-19 19:30:00');
INSERT INTO `schedule_job_log` VALUES (34, 1, 'testTask', 'renren', 0, NULL, 6, '2019-10-19 20:00:00');
INSERT INTO `schedule_job_log` VALUES (35, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-19 22:00:00');
INSERT INTO `schedule_job_log` VALUES (36, 1, 'testTask', 'renren', 0, NULL, 6, '2019-10-20 09:00:00');
INSERT INTO `schedule_job_log` VALUES (37, 1, 'testTask', 'renren', 0, NULL, 4, '2019-10-20 09:30:00');
INSERT INTO `schedule_job_log` VALUES (38, 1, 'testTask', 'renren', 0, NULL, 4, '2019-10-20 10:00:00');
INSERT INTO `schedule_job_log` VALUES (39, 1, 'testTask', 'renren', 0, NULL, 2, '2019-10-20 10:30:00');
INSERT INTO `schedule_job_log` VALUES (40, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-20 11:00:00');
INSERT INTO `schedule_job_log` VALUES (41, 1, 'testTask', 'renren', 0, NULL, 3, '2019-10-20 11:30:00');
INSERT INTO `schedule_job_log` VALUES (42, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-20 14:00:00');
INSERT INTO `schedule_job_log` VALUES (43, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 14:30:00');
INSERT INTO `schedule_job_log` VALUES (44, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-20 15:30:00');
INSERT INTO `schedule_job_log` VALUES (45, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-20 16:00:00');
INSERT INTO `schedule_job_log` VALUES (46, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 17:00:00');
INSERT INTO `schedule_job_log` VALUES (47, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 17:30:00');
INSERT INTO `schedule_job_log` VALUES (48, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 18:00:00');
INSERT INTO `schedule_job_log` VALUES (49, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 18:30:00');
INSERT INTO `schedule_job_log` VALUES (50, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 19:30:00');
INSERT INTO `schedule_job_log` VALUES (51, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 20:30:00');
INSERT INTO `schedule_job_log` VALUES (52, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 21:00:00');
INSERT INTO `schedule_job_log` VALUES (53, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-20 22:00:00');
INSERT INTO `schedule_job_log` VALUES (54, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 22:30:00');
INSERT INTO `schedule_job_log` VALUES (55, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-20 23:00:00');
INSERT INTO `schedule_job_log` VALUES (56, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-20 23:30:00');
INSERT INTO `schedule_job_log` VALUES (57, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 00:00:00');
INSERT INTO `schedule_job_log` VALUES (58, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 00:30:00');
INSERT INTO `schedule_job_log` VALUES (59, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 01:00:00');
INSERT INTO `schedule_job_log` VALUES (60, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 01:30:00');
INSERT INTO `schedule_job_log` VALUES (61, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 02:00:00');
INSERT INTO `schedule_job_log` VALUES (62, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 02:30:00');
INSERT INTO `schedule_job_log` VALUES (63, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 03:00:00');
INSERT INTO `schedule_job_log` VALUES (64, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-21 03:30:00');
INSERT INTO `schedule_job_log` VALUES (65, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 04:00:00');
INSERT INTO `schedule_job_log` VALUES (66, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 04:30:00');
INSERT INTO `schedule_job_log` VALUES (67, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 05:00:00');
INSERT INTO `schedule_job_log` VALUES (68, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 05:30:00');
INSERT INTO `schedule_job_log` VALUES (69, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 06:00:00');
INSERT INTO `schedule_job_log` VALUES (70, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 06:30:00');
INSERT INTO `schedule_job_log` VALUES (71, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-21 07:00:00');
INSERT INTO `schedule_job_log` VALUES (72, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 07:30:00');
INSERT INTO `schedule_job_log` VALUES (73, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 08:00:00');
INSERT INTO `schedule_job_log` VALUES (74, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 08:30:00');
INSERT INTO `schedule_job_log` VALUES (75, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 09:00:00');
INSERT INTO `schedule_job_log` VALUES (76, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 09:30:00');
INSERT INTO `schedule_job_log` VALUES (77, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 10:00:00');
INSERT INTO `schedule_job_log` VALUES (78, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 10:30:00');
INSERT INTO `schedule_job_log` VALUES (79, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 11:00:00');
INSERT INTO `schedule_job_log` VALUES (80, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 11:30:00');
INSERT INTO `schedule_job_log` VALUES (81, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 12:00:00');
INSERT INTO `schedule_job_log` VALUES (82, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 12:30:00');
INSERT INTO `schedule_job_log` VALUES (83, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 13:00:00');
INSERT INTO `schedule_job_log` VALUES (84, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 13:30:00');
INSERT INTO `schedule_job_log` VALUES (85, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 14:00:00');
INSERT INTO `schedule_job_log` VALUES (86, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 14:30:00');
INSERT INTO `schedule_job_log` VALUES (87, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 15:00:00');
INSERT INTO `schedule_job_log` VALUES (88, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 15:30:00');
INSERT INTO `schedule_job_log` VALUES (89, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 16:00:00');
INSERT INTO `schedule_job_log` VALUES (90, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 16:30:00');
INSERT INTO `schedule_job_log` VALUES (91, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 17:00:00');
INSERT INTO `schedule_job_log` VALUES (92, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 17:30:00');
INSERT INTO `schedule_job_log` VALUES (93, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 18:00:00');
INSERT INTO `schedule_job_log` VALUES (94, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-21 18:30:00');
INSERT INTO `schedule_job_log` VALUES (95, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 19:00:00');
INSERT INTO `schedule_job_log` VALUES (96, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 19:30:00');
INSERT INTO `schedule_job_log` VALUES (97, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 20:00:00');
INSERT INTO `schedule_job_log` VALUES (98, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-21 20:30:00');
INSERT INTO `schedule_job_log` VALUES (99, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 21:00:00');
INSERT INTO `schedule_job_log` VALUES (100, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 21:30:00');
INSERT INTO `schedule_job_log` VALUES (101, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 22:00:00');
INSERT INTO `schedule_job_log` VALUES (102, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 22:30:00');
INSERT INTO `schedule_job_log` VALUES (103, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 23:00:00');
INSERT INTO `schedule_job_log` VALUES (104, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-21 23:30:00');
INSERT INTO `schedule_job_log` VALUES (105, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 00:00:00');
INSERT INTO `schedule_job_log` VALUES (106, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 00:30:00');
INSERT INTO `schedule_job_log` VALUES (107, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 01:00:00');
INSERT INTO `schedule_job_log` VALUES (108, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 01:30:00');
INSERT INTO `schedule_job_log` VALUES (109, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 02:00:00');
INSERT INTO `schedule_job_log` VALUES (110, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 02:30:00');
INSERT INTO `schedule_job_log` VALUES (111, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 03:00:00');
INSERT INTO `schedule_job_log` VALUES (112, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 03:30:00');
INSERT INTO `schedule_job_log` VALUES (113, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 04:00:00');
INSERT INTO `schedule_job_log` VALUES (114, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 04:30:00');
INSERT INTO `schedule_job_log` VALUES (115, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 05:00:00');
INSERT INTO `schedule_job_log` VALUES (116, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 05:30:00');
INSERT INTO `schedule_job_log` VALUES (117, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 06:00:00');
INSERT INTO `schedule_job_log` VALUES (118, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 06:30:00');
INSERT INTO `schedule_job_log` VALUES (119, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 07:00:00');
INSERT INTO `schedule_job_log` VALUES (120, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 07:30:00');
INSERT INTO `schedule_job_log` VALUES (121, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 08:00:00');
INSERT INTO `schedule_job_log` VALUES (122, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 08:30:00');
INSERT INTO `schedule_job_log` VALUES (123, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 09:00:00');
INSERT INTO `schedule_job_log` VALUES (124, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 09:30:00');
INSERT INTO `schedule_job_log` VALUES (125, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 10:00:00');
INSERT INTO `schedule_job_log` VALUES (126, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 10:30:00');
INSERT INTO `schedule_job_log` VALUES (127, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 11:00:00');
INSERT INTO `schedule_job_log` VALUES (128, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 11:30:00');
INSERT INTO `schedule_job_log` VALUES (129, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 12:00:00');
INSERT INTO `schedule_job_log` VALUES (130, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 12:30:00');
INSERT INTO `schedule_job_log` VALUES (131, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 13:00:00');
INSERT INTO `schedule_job_log` VALUES (132, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 13:30:00');
INSERT INTO `schedule_job_log` VALUES (133, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 14:00:00');
INSERT INTO `schedule_job_log` VALUES (134, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 14:30:00');
INSERT INTO `schedule_job_log` VALUES (135, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 15:00:00');
INSERT INTO `schedule_job_log` VALUES (136, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 15:30:00');
INSERT INTO `schedule_job_log` VALUES (137, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 16:00:00');
INSERT INTO `schedule_job_log` VALUES (138, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-22 16:30:00');
INSERT INTO `schedule_job_log` VALUES (139, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-24 14:00:00');
INSERT INTO `schedule_job_log` VALUES (140, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-24 14:30:00');
INSERT INTO `schedule_job_log` VALUES (141, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-24 15:00:00');
INSERT INTO `schedule_job_log` VALUES (142, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-24 15:30:00');
INSERT INTO `schedule_job_log` VALUES (143, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-24 18:00:01');
INSERT INTO `schedule_job_log` VALUES (144, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-25 10:30:00');
INSERT INTO `schedule_job_log` VALUES (145, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 11:30:00');
INSERT INTO `schedule_job_log` VALUES (146, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 12:00:00');
INSERT INTO `schedule_job_log` VALUES (147, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 12:30:00');
INSERT INTO `schedule_job_log` VALUES (148, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 13:00:00');
INSERT INTO `schedule_job_log` VALUES (149, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 13:30:00');
INSERT INTO `schedule_job_log` VALUES (150, 1, 'testTask', 'renren', 0, NULL, 153, '2019-10-25 14:30:02');
INSERT INTO `schedule_job_log` VALUES (151, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 15:30:00');
INSERT INTO `schedule_job_log` VALUES (152, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 16:00:00');
INSERT INTO `schedule_job_log` VALUES (153, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-25 17:30:00');
INSERT INTO `schedule_job_log` VALUES (154, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 17:00:00');
INSERT INTO `schedule_job_log` VALUES (155, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 19:00:00');
INSERT INTO `schedule_job_log` VALUES (156, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 19:30:00');
INSERT INTO `schedule_job_log` VALUES (157, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 20:00:00');
INSERT INTO `schedule_job_log` VALUES (158, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 20:30:00');
INSERT INTO `schedule_job_log` VALUES (159, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 21:00:00');
INSERT INTO `schedule_job_log` VALUES (160, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 21:30:00');
INSERT INTO `schedule_job_log` VALUES (161, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 22:30:00');
INSERT INTO `schedule_job_log` VALUES (162, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-27 23:30:00');
INSERT INTO `schedule_job_log` VALUES (163, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-28 00:30:00');
INSERT INTO `schedule_job_log` VALUES (164, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-28 22:30:00');
INSERT INTO `schedule_job_log` VALUES (165, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-29 21:00:00');
INSERT INTO `schedule_job_log` VALUES (166, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-29 21:30:00');
INSERT INTO `schedule_job_log` VALUES (167, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-29 22:00:00');
INSERT INTO `schedule_job_log` VALUES (168, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-29 22:30:00');
INSERT INTO `schedule_job_log` VALUES (169, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-29 23:00:00');
INSERT INTO `schedule_job_log` VALUES (170, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-29 23:30:00');
INSERT INTO `schedule_job_log` VALUES (171, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-30 00:00:00');
INSERT INTO `schedule_job_log` VALUES (172, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-30 08:00:00');
INSERT INTO `schedule_job_log` VALUES (173, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-30 21:00:00');
INSERT INTO `schedule_job_log` VALUES (174, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-30 21:30:00');
INSERT INTO `schedule_job_log` VALUES (175, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-30 22:00:00');
INSERT INTO `schedule_job_log` VALUES (176, 1, 'testTask', 'renren', 0, NULL, 1, '2019-10-30 22:30:00');
INSERT INTO `schedule_job_log` VALUES (177, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-30 23:00:00');
INSERT INTO `schedule_job_log` VALUES (178, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-30 23:30:00');
INSERT INTO `schedule_job_log` VALUES (179, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-31 07:30:00');
INSERT INTO `schedule_job_log` VALUES (180, 1, 'testTask', 'renren', 0, NULL, 0, '2019-10-31 08:00:00');
INSERT INTO `schedule_job_log` VALUES (181, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 10:00:00');
INSERT INTO `schedule_job_log` VALUES (182, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 12:30:11');
INSERT INTO `schedule_job_log` VALUES (183, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 13:00:00');
INSERT INTO `schedule_job_log` VALUES (184, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 14:00:00');
INSERT INTO `schedule_job_log` VALUES (185, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 14:30:00');
INSERT INTO `schedule_job_log` VALUES (186, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 15:00:00');
INSERT INTO `schedule_job_log` VALUES (187, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 21:00:00');
INSERT INTO `schedule_job_log` VALUES (188, 1, 'testTask', 'renren', 0, NULL, 1, '2019-11-02 21:30:00');
INSERT INTO `schedule_job_log` VALUES (189, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-02 22:00:00');
INSERT INTO `schedule_job_log` VALUES (190, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-03 13:00:00');
INSERT INTO `schedule_job_log` VALUES (191, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-03 13:30:00');
INSERT INTO `schedule_job_log` VALUES (192, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-03 14:00:00');
INSERT INTO `schedule_job_log` VALUES (193, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-03 14:30:00');
INSERT INTO `schedule_job_log` VALUES (194, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-03 15:00:00');
INSERT INTO `schedule_job_log` VALUES (195, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-03 15:30:00');
INSERT INTO `schedule_job_log` VALUES (196, 1, 'testTask', 'renren', 0, NULL, 1, '2019-11-03 16:00:00');
INSERT INTO `schedule_job_log` VALUES (197, 1, 'testTask', 'renren', 0, NULL, 0, '2019-11-03 16:30:00');
INSERT INTO `schedule_job_log` VALUES (198, 1, 'testTask', 'renren', 0, NULL, 2, '2019-12-01 10:30:00');
INSERT INTO `schedule_job_log` VALUES (199, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-01 11:00:00');
INSERT INTO `schedule_job_log` VALUES (200, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-01 11:30:00');
INSERT INTO `schedule_job_log` VALUES (201, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-01 12:00:00');
INSERT INTO `schedule_job_log` VALUES (202, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-01 16:30:00');
INSERT INTO `schedule_job_log` VALUES (203, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-01 17:00:00');
INSERT INTO `schedule_job_log` VALUES (204, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-01 17:30:00');
INSERT INTO `schedule_job_log` VALUES (205, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-02 21:30:00');
INSERT INTO `schedule_job_log` VALUES (206, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-02 22:00:00');
INSERT INTO `schedule_job_log` VALUES (207, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-02 22:30:00');
INSERT INTO `schedule_job_log` VALUES (208, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-02 23:00:00');
INSERT INTO `schedule_job_log` VALUES (209, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-02 23:30:00');
INSERT INTO `schedule_job_log` VALUES (210, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 00:00:00');
INSERT INTO `schedule_job_log` VALUES (211, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 08:00:00');
INSERT INTO `schedule_job_log` VALUES (212, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 08:30:00');
INSERT INTO `schedule_job_log` VALUES (213, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 20:00:00');
INSERT INTO `schedule_job_log` VALUES (214, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 21:00:00');
INSERT INTO `schedule_job_log` VALUES (215, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 21:30:00');
INSERT INTO `schedule_job_log` VALUES (216, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 22:30:00');
INSERT INTO `schedule_job_log` VALUES (217, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 23:00:00');
INSERT INTO `schedule_job_log` VALUES (218, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-03 23:30:00');
INSERT INTO `schedule_job_log` VALUES (219, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-04 00:00:00');
INSERT INTO `schedule_job_log` VALUES (220, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-04 01:00:00');
INSERT INTO `schedule_job_log` VALUES (221, 1, 'testTask', 'renren', 0, NULL, 1, '2019-12-04 08:00:00');
INSERT INTO `schedule_job_log` VALUES (222, 1, 'testTask', 'renren', 0, NULL, 1, '2019-12-04 21:00:00');
INSERT INTO `schedule_job_log` VALUES (223, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-04 22:00:00');
INSERT INTO `schedule_job_log` VALUES (224, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-04 22:30:00');
INSERT INTO `schedule_job_log` VALUES (225, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-05 08:00:00');
INSERT INTO `schedule_job_log` VALUES (226, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 10:30:00');
INSERT INTO `schedule_job_log` VALUES (227, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 11:00:00');
INSERT INTO `schedule_job_log` VALUES (228, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 11:30:00');
INSERT INTO `schedule_job_log` VALUES (229, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 12:00:32');
INSERT INTO `schedule_job_log` VALUES (230, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 13:30:00');
INSERT INTO `schedule_job_log` VALUES (231, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 14:00:00');
INSERT INTO `schedule_job_log` VALUES (232, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 16:00:00');
INSERT INTO `schedule_job_log` VALUES (233, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 16:30:00');
INSERT INTO `schedule_job_log` VALUES (234, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 18:30:12');
INSERT INTO `schedule_job_log` VALUES (235, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 19:00:00');
INSERT INTO `schedule_job_log` VALUES (236, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 21:00:00');
INSERT INTO `schedule_job_log` VALUES (237, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 21:30:00');
INSERT INTO `schedule_job_log` VALUES (238, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 22:00:00');
INSERT INTO `schedule_job_log` VALUES (239, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-08 22:30:00');
INSERT INTO `schedule_job_log` VALUES (240, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-17 21:00:00');
INSERT INTO `schedule_job_log` VALUES (241, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-18 08:00:00');
INSERT INTO `schedule_job_log` VALUES (242, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-18 20:30:00');
INSERT INTO `schedule_job_log` VALUES (243, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-18 21:00:00');
INSERT INTO `schedule_job_log` VALUES (244, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-18 21:30:00');
INSERT INTO `schedule_job_log` VALUES (245, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-18 22:00:00');
INSERT INTO `schedule_job_log` VALUES (246, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-18 22:30:00');
INSERT INTO `schedule_job_log` VALUES (247, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 12:30:00');
INSERT INTO `schedule_job_log` VALUES (248, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 13:00:00');
INSERT INTO `schedule_job_log` VALUES (249, 1, 'testTask', 'renren', 0, NULL, 9, '2019-12-22 13:30:00');
INSERT INTO `schedule_job_log` VALUES (250, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 15:00:00');
INSERT INTO `schedule_job_log` VALUES (251, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 15:30:00');
INSERT INTO `schedule_job_log` VALUES (252, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 16:00:00');
INSERT INTO `schedule_job_log` VALUES (253, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 16:30:00');
INSERT INTO `schedule_job_log` VALUES (254, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 17:00:00');
INSERT INTO `schedule_job_log` VALUES (255, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 17:30:00');
INSERT INTO `schedule_job_log` VALUES (256, 1, 'testTask', 'renren', 0, NULL, 1, '2019-12-22 18:00:00');
INSERT INTO `schedule_job_log` VALUES (257, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 18:30:00');
INSERT INTO `schedule_job_log` VALUES (258, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-22 19:00:00');
INSERT INTO `schedule_job_log` VALUES (259, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-23 20:30:00');
INSERT INTO `schedule_job_log` VALUES (260, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-23 21:00:00');
INSERT INTO `schedule_job_log` VALUES (261, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-23 22:00:00');
INSERT INTO `schedule_job_log` VALUES (262, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-23 22:30:00');
INSERT INTO `schedule_job_log` VALUES (263, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-23 23:00:00');
INSERT INTO `schedule_job_log` VALUES (264, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-24 08:30:00');
INSERT INTO `schedule_job_log` VALUES (265, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-29 10:00:00');
INSERT INTO `schedule_job_log` VALUES (266, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-29 10:30:00');
INSERT INTO `schedule_job_log` VALUES (267, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-29 11:00:00');
INSERT INTO `schedule_job_log` VALUES (268, 1, 'testTask', 'renren', 0, NULL, 0, '2019-12-30 20:30:00');
INSERT INTO `schedule_job_log` VALUES (269, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-06 22:30:00');
INSERT INTO `schedule_job_log` VALUES (270, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-07 08:00:00');
INSERT INTO `schedule_job_log` VALUES (271, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-07 21:00:00');
INSERT INTO `schedule_job_log` VALUES (272, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-07 21:30:00');
INSERT INTO `schedule_job_log` VALUES (273, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-07 22:00:00');
INSERT INTO `schedule_job_log` VALUES (274, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-07 22:30:00');
INSERT INTO `schedule_job_log` VALUES (275, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-09 21:00:00');
INSERT INTO `schedule_job_log` VALUES (276, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-09 21:30:00');
INSERT INTO `schedule_job_log` VALUES (277, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-10 08:30:00');
INSERT INTO `schedule_job_log` VALUES (278, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-11 15:30:00');
INSERT INTO `schedule_job_log` VALUES (279, 1, 'testTask', 'renren', 0, NULL, 1, '2020-01-11 16:00:00');
INSERT INTO `schedule_job_log` VALUES (280, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-11 16:30:00');
INSERT INTO `schedule_job_log` VALUES (281, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-11 17:00:00');
INSERT INTO `schedule_job_log` VALUES (282, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-11 20:30:00');
INSERT INTO `schedule_job_log` VALUES (283, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-12 09:30:00');
INSERT INTO `schedule_job_log` VALUES (284, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-12 11:00:00');
INSERT INTO `schedule_job_log` VALUES (285, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-12 12:00:00');
INSERT INTO `schedule_job_log` VALUES (286, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-12 12:30:00');
INSERT INTO `schedule_job_log` VALUES (287, 1, 'testTask', 'renren', 0, NULL, 0, '2020-01-12 13:30:00');

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha`  (
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'uuid',
  `code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '验证码',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统验证码' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------
INSERT INTO `sys_captcha` VALUES ('013bc668-9f40-44b1-8ce7-62420e315511', 'bfgpn', '2019-10-21 13:34:51');
INSERT INTO `sys_captcha` VALUES ('084c804b-3d5d-40a0-873f-75f9a27dcac8', 'gax5w', '2019-10-20 20:23:15');
INSERT INTO `sys_captcha` VALUES ('091fab5c-ce20-489b-897b-1df4d629b025', 'x67e4', '2019-10-22 09:12:24');
INSERT INTO `sys_captcha` VALUES ('0a500ade-962b-474b-89f9-0b4b34db5979', '3xfcf', '2019-10-20 21:28:04');
INSERT INTO `sys_captcha` VALUES ('17814e15-6b29-4608-8586-89cb850f914e', '5pgd2', '2019-10-21 15:38:28');
INSERT INTO `sys_captcha` VALUES ('189dd7c4-336b-4ef3-8f34-f401d8ba7dfe', 'b5a4a', '2019-12-04 08:16:03');
INSERT INTO `sys_captcha` VALUES ('18bbca50-2f95-4a27-8719-b44b2b371872', 'wb44w', '2019-10-20 20:18:14');
INSERT INTO `sys_captcha` VALUES ('1ec6470c-479d-4094-85c9-bf0110bc0098', 'gbp7d', '2019-10-21 09:05:25');
INSERT INTO `sys_captcha` VALUES ('2f0fbdb7-3161-4200-8646-4a119d9a0543', 'cy2cn', '2019-10-25 16:27:11');
INSERT INTO `sys_captcha` VALUES ('333f3966-8610-4c93-816b-2b47c2492fc6', '73g8g', '2019-10-24 14:48:56');
INSERT INTO `sys_captcha` VALUES ('344c0d97-60a6-4183-84b1-c8c00ddcb784', 'am4pa', '2019-12-04 08:17:50');
INSERT INTO `sys_captcha` VALUES ('3f4936d7-f169-498e-863f-ba91d64cf809', '667ey', '2019-10-22 09:12:22');
INSERT INTO `sys_captcha` VALUES ('436d9023-73d3-40e6-85fc-fc4d767f79ee', '333by', '2019-12-04 08:16:20');
INSERT INTO `sys_captcha` VALUES ('46097d5c-d5db-49c9-8114-21028d6f105f', 'm7wm4', '2019-11-03 16:15:31');
INSERT INTO `sys_captcha` VALUES ('4b201438-04de-4383-8b42-50889e5b4085', 'n4fx5', '2019-10-19 18:33:25');
INSERT INTO `sys_captcha` VALUES ('543bc6c0-48d1-4798-86dd-6c4f673981e2', 'm63m8', '2019-10-21 17:45:32');
INSERT INTO `sys_captcha` VALUES ('55', 'nw5an', '2019-09-24 21:23:14');
INSERT INTO `sys_captcha` VALUES ('5a398672-8ce3-4a72-88cd-d08c75081e89', '4nnee', '2019-12-17 21:46:23');
INSERT INTO `sys_captcha` VALUES ('5dfaec8b-9a90-45ac-8eb8-605d8508b34d', '2fd23', '2019-10-21 08:40:45');
INSERT INTO `sys_captcha` VALUES ('603c6952-ea78-4d86-847b-f785054b03c5', '3w25d', '2019-10-21 17:38:23');
INSERT INTO `sys_captcha` VALUES ('62cbeb90-d311-4d81-8ccd-d5cb49402a6c', '8bx3n', '2019-10-21 14:15:56');
INSERT INTO `sys_captcha` VALUES ('66557ce2-081e-46dc-8a99-c420c88a96ee', 'cyfmx', '2019-10-25 16:50:20');
INSERT INTO `sys_captcha` VALUES ('68930dfd-2d58-4b07-894e-f0adb0793186', 'm2m57', '2019-10-21 11:08:08');
INSERT INTO `sys_captcha` VALUES ('69fa4faf-8a98-49f2-8aba-892153d68d75', 'pc8mw', '2019-10-24 14:48:55');
INSERT INTO `sys_captcha` VALUES ('6ae68a0e-bae0-493c-8446-ef02f47d5494', 'dw44b', '2019-11-02 13:09:48');
INSERT INTO `sys_captcha` VALUES ('7563dd93-ab99-4614-816c-049ed74bad18', 'p5wcn', '2019-10-24 14:48:56');
INSERT INTO `sys_captcha` VALUES ('7abe49cb-8704-4225-8118-4f8565f3acee', '5cman', '2019-10-21 17:42:30');
INSERT INTO `sys_captcha` VALUES ('7ed0dd68-1a0b-4802-8706-501372cd13ed', '8bxg5', '2019-10-24 16:43:26');
INSERT INTO `sys_captcha` VALUES ('824c3134-f536-4aa1-886d-0e0fe2a46e2e', 'anyc4', '2019-10-24 14:48:55');
INSERT INTO `sys_captcha` VALUES ('98652b5f-3b9e-4fa4-8f74-689198dac5fa', 'f47wp', '2020-01-07 21:40:12');
INSERT INTO `sys_captcha` VALUES ('9a895cb1-85db-4964-82a8-b99738f877ab', 'w73w2', '2019-10-22 09:12:26');
INSERT INTO `sys_captcha` VALUES ('9a9279ee-9006-4e49-8431-8a93f7470921', '57nac', '2020-01-12 09:15:30');
INSERT INTO `sys_captcha` VALUES ('9ac9acbe-cb7e-48d2-8c9e-1304bd3b4877', 'n5d67', '2019-10-19 18:50:11');
INSERT INTO `sys_captcha` VALUES ('9b6ac536-80a8-481c-8a47-b82f526c043f', 'pf87n', '2019-12-04 08:22:57');
INSERT INTO `sys_captcha` VALUES ('9b9213a0-85b0-4358-8825-35552fd3f182', '6d2e5', '2019-10-20 21:54:50');
INSERT INTO `sys_captcha` VALUES ('9e88865e-eb0c-4170-8672-119898e88eb3', 'c5257', '2019-10-22 09:12:19');
INSERT INTO `sys_captcha` VALUES ('9faeeafa-14f0-4038-87ae-d8af7f9800cf', 'g625w', '2019-11-03 16:32:49');
INSERT INTO `sys_captcha` VALUES ('a3388c65-c103-4dad-85d3-6a082646ffd0', '3p68n', '2019-10-20 20:25:35');
INSERT INTO `sys_captcha` VALUES ('b029f793-23be-479a-8dab-4e9d6610761e', 'm48m5', '2019-10-21 13:07:01');
INSERT INTO `sys_captcha` VALUES ('b114e063-0471-4856-897d-2fcef8cef7a3', 'c5f8y', '2019-10-21 07:41:02');
INSERT INTO `sys_captcha` VALUES ('b158892f-69c9-44b4-8d40-944c0b0435ef', '8anf2', '2019-12-04 08:16:42');
INSERT INTO `sys_captcha` VALUES ('b3569c96-a2b6-4e73-847b-41c78e2ceee5', 'yc37w', '2019-10-21 13:48:16');
INSERT INTO `sys_captcha` VALUES ('b6c3846e-edff-4345-839f-ca20b9376f47', 'anned', '2019-12-30 20:53:58');
INSERT INTO `sys_captcha` VALUES ('b887e9de-5895-4adc-8f88-9c7b0a2b3d1e', 'am82w', '2019-10-21 07:37:54');
INSERT INTO `sys_captcha` VALUES ('b9ab301c-7585-49b5-8ccf-fbccbefd64af', 'fnnxp', '2019-10-21 23:58:27');
INSERT INTO `sys_captcha` VALUES ('bb122bf0-71cb-4eb9-8786-5de5df6e9201', 'p6ewm', '2019-10-21 17:31:12');
INSERT INTO `sys_captcha` VALUES ('bb124802-e650-4b36-8816-135ea68294f6', '6d35d', '2020-01-09 20:41:03');
INSERT INTO `sys_captcha` VALUES ('bde0c337-8a46-4d23-8161-afc9e5f0762f', 'gdywc', '2019-10-21 15:45:42');
INSERT INTO `sys_captcha` VALUES ('bf87e478-6570-4976-8d72-c7bf10cb11e0', 'm2ee8', '2019-10-20 19:26:13');
INSERT INTO `sys_captcha` VALUES ('c361b8ea-7ab5-43c0-8d27-f454b623b0b1', 'df34n', '2019-10-24 14:44:28');
INSERT INTO `sys_captcha` VALUES ('d2a88be5-0c7c-412c-8af1-1c427becd4b4', 'bde67', '2019-10-20 19:55:12');
INSERT INTO `sys_captcha` VALUES ('d813d72d-5f85-4cd2-8bff-2d9dbcd40d4d', '5p4ng', '2019-10-19 09:00:44');
INSERT INTO `sys_captcha` VALUES ('da7db47d-0bb3-456b-84dc-5e20a842f2e7', 'b7g6c', '2019-10-22 04:43:44');
INSERT INTO `sys_captcha` VALUES ('ddc89b22-f6f3-4242-8871-b7d99823bdcd', 'nebm6', '2019-12-08 18:22:19');
INSERT INTO `sys_captcha` VALUES ('e15be287-1ed3-4dba-8279-f75f1eee2c78', '5dmbw', '2019-10-21 11:02:03');
INSERT INTO `sys_captcha` VALUES ('e21fc9bf-1976-4aaf-8d8c-9da13858d40a', '5fned', '2019-10-20 19:55:06');
INSERT INTO `sys_captcha` VALUES ('e6289023-9c09-4825-8a1b-ec2e09d7cea2', 'c5c68', '2019-10-20 20:59:37');
INSERT INTO `sys_captcha` VALUES ('eb7c6883-26fa-4287-8a67-5001c82b1794', '5nxpc', '2019-10-20 18:49:45');
INSERT INTO `sys_captcha` VALUES ('edacdaef-5fae-4f81-88b1-e25de14b3c2c', 'pgc2g', '2019-10-25 17:01:40');
INSERT INTO `sys_captcha` VALUES ('f124f5a5-3a7d-4f7a-8aeb-0dd742a8a872', '668nb', '2019-12-30 20:53:26');
INSERT INTO `sys_captcha` VALUES ('f1bbfa8a-8361-4902-8472-f92eee2bf889', 'cxxax', '2019-10-24 14:48:55');
INSERT INTO `sys_captcha` VALUES ('fa630e3f-2f26-4432-8221-b1567bb5c5c4', 'dx2ef', '2019-10-22 09:12:21');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', 0, '云存储配置信息');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 72 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (1, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":31,\"parentId\":1,\"name\":\"商品管理\",\"url\":\"modules/generator/goods.html\",\"type\":1,\"icon\":\"job\",\"orderNum\":6}]', 5, '0:0:0:0:0:0:0:1', '2019-10-16 22:01:45');
INSERT INTO `sys_log` VALUES (2, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":31,\"parentId\":1,\"name\":\"商品管理\",\"url\":\"generator/goods\",\"type\":1,\"icon\":\"job\",\"orderNum\":6}]', 4, '0:0:0:0:0:0:0:1', '2019-10-16 22:06:21');
INSERT INTO `sys_log` VALUES (3, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":31,\"parentId\":1,\"name\":\"商品管理\",\"url\":\"generator/goods.html\",\"type\":1,\"icon\":\"job\",\"orderNum\":6}]', 4, '0:0:0:0:0:0:0:1', '2019-10-16 22:13:04');
INSERT INTO `sys_log` VALUES (4, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[36]', 3, '0:0:0:0:0:0:0:1', '2019-10-19 08:48:52');
INSERT INTO `sys_log` VALUES (5, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[36]', 1, '0:0:0:0:0:0:0:1', '2019-10-19 08:49:07');
INSERT INTO `sys_log` VALUES (6, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[31]', 0, '0:0:0:0:0:0:0:1', '2019-10-19 08:49:21');
INSERT INTO `sys_log` VALUES (7, 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":67,\"parentId\":0,\"name\":\"项目管理\",\"type\":0,\"icon\":\"dangdifill\",\"orderNum\":0}]', 10, '0:0:0:0:0:0:0:1', '2019-10-20 10:03:53');
INSERT INTO `sys_log` VALUES (8, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":2,\"username\":\"test\",\"password\":\"048c325c6cd107dc510665f6d20090b5aac5bc82301ba9b67cc72e8a084f20ca\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":\"Oct 20, 2019 11:01:44 AM\"}]', 115, '0:0:0:0:0:0:0:1', '2019-10-20 11:01:44');
INSERT INTO `sys_log` VALUES (9, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":3,\"username\":\"test01\",\"password\":\"5f82b34adcb4163aac3c3f1bd0ecdcfa62a2b5463ba67a85497290834880bea7\",\"salt\":\"pjpduaDxA4rXqIQBBj1J\",\"email\":\"tesesss@qq.com\",\"mobile\":\"13478123456\",\"status\":1,\"roleIdList\":[],\"createUserId\":1,\"createTime\":\"Oct 20, 2019 3:33:17 PM\"}]', 114, '0:0:0:0:0:0:0:1', '2019-10-20 15:33:17');
INSERT INTO `sys_log` VALUES (10, 'admin', '删除用户', 'io.renren.modules.sys.controller.SysUserController.delete()', '[[3]]', 11, '0:0:0:0:0:0:0:1', '2019-10-20 15:34:34');
INSERT INTO `sys_log` VALUES (11, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"password\":\"683be2764c47d9db696261694653fefc4e61d1cdb9ca1a21309b54d6b4b450d7\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[],\"createUserId\":1}]', 65, '0:0:0:0:0:0:0:1', '2019-10-20 15:37:27');
INSERT INTO `sys_log` VALUES (12, 'admin', '删除菜单', 'io.renren.modules.sys.controller.SysMenuController.delete()', '[67]', 9, '0:0:0:0:0:0:0:1', '2019-10-20 17:56:32');
INSERT INTO `sys_log` VALUES (13, 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":1,\"roleName\":\"企业管理员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[42,43,52,53,56,57,58,59,60,61,62,63,64,65,66,-666666,31,41,51],\"createTime\":\"Oct 20, 2019 9:54:49 PM\"}]', 262, '175.162.90.173', '2019-10-20 21:54:49');
INSERT INTO `sys_log` VALUES (14, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":3,\"username\":\"lin\",\"password\":\"0109e68dddff07887aa7db2fa7f28c40eb1150c2d721c6e9668362439b4d67b4\",\"salt\":\"iDApxUyxHQH3RMBssg2B\",\"email\":\"123@123.com\",\"mobile\":\"13010101010\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1,\"createTime\":\"Oct 20, 2019 9:55:13 PM\"}]', 99, '175.162.90.173', '2019-10-20 21:55:13');
INSERT INTO `sys_log` VALUES (15, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":3,\"username\":\"cy\",\"password\":\"1004f4230eced1913b5c548e907aee05b9aba50e022bd53c923053e55fcb2401\",\"salt\":\"iDApxUyxHQH3RMBssg2B\",\"email\":\"123@123.com\",\"mobile\":\"13010101010\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1}]', 12, '175.162.90.173', '2019-10-20 22:44:13');
INSERT INTO `sys_log` VALUES (16, 'admin', '修改密码', 'io.renren.modules.sys.controller.SysUserController.password()', '[{\"password\":\"admin\",\"newPassword\":\"adminadmin123\"}]', 8, '124.93.236.18', '2019-10-21 13:00:45');
INSERT INTO `sys_log` VALUES (17, 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":67,\"parentId\":0,\"name\":\"证书管理\",\"url\":\"123\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]', 0, '124.93.236.18', '2019-10-21 13:58:04');
INSERT INTO `sys_log` VALUES (18, 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":68,\"parentId\":0,\"name\":\"薪资管理\",\"url\":\"123\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]', 16, '124.93.236.18', '2019-10-21 13:58:23');
INSERT INTO `sys_log` VALUES (19, 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":69,\"parentId\":0,\"name\":\"社保管理\",\"url\":\"123\",\"perms\":\"\",\"type\":1,\"icon\":\"\",\"orderNum\":0}]', 16, '124.93.236.18', '2019-10-21 13:58:39');
INSERT INTO `sys_log` VALUES (20, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":67,\"parentId\":0,\"name\":\"证书管理\",\"url\":\"123\",\"perms\":\"\",\"type\":1,\"icon\":\"editor\",\"orderNum\":0}]', 0, '124.93.236.18', '2019-10-21 13:59:23');
INSERT INTO `sys_log` VALUES (21, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":68,\"parentId\":0,\"name\":\"薪资管理\",\"url\":\"123\",\"perms\":\"\",\"type\":1,\"icon\":\"log\",\"orderNum\":0}]', 0, '124.93.236.18', '2019-10-21 14:00:02');
INSERT INTO `sys_log` VALUES (22, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":69,\"parentId\":0,\"name\":\"社保管理\",\"url\":\"123\",\"perms\":\"\",\"type\":1,\"icon\":\"geren\",\"orderNum\":0}]', 0, '124.93.236.18', '2019-10-21 14:00:21');
INSERT INTO `sys_log` VALUES (23, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1}]', 297, '124.93.236.18', '2019-10-21 14:00:36');
INSERT INTO `sys_log` VALUES (24, 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"企业管理员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[42,43,52,53,56,57,58,59,60,61,62,63,64,65,66,67,68,69,-666666,31,41,51]}]', 78, '124.93.236.18', '2019-10-21 14:00:53');
INSERT INTO `sys_log` VALUES (25, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":67,\"parentId\":0,\"name\":\"证书管理\",\"url\":\"home\",\"perms\":\"\",\"type\":1,\"icon\":\"editor\",\"orderNum\":0}]', 0, '124.93.236.18', '2019-10-21 14:02:42');
INSERT INTO `sys_log` VALUES (26, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":68,\"parentId\":0,\"name\":\"薪资管理\",\"url\":\"home\",\"perms\":\"\",\"type\":1,\"icon\":\"log\",\"orderNum\":0}]', 0, '124.93.236.18', '2019-10-21 14:02:52');
INSERT INTO `sys_log` VALUES (27, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":69,\"parentId\":0,\"name\":\"社保管理\",\"url\":\"home\",\"perms\":\"\",\"type\":1,\"icon\":\"geren\",\"orderNum\":0}]', 0, '124.93.236.18', '2019-10-21 14:03:00');
INSERT INTO `sys_log` VALUES (28, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":31,\"parentId\":0,\"name\":\"船员管理\",\"type\":0,\"icon\":\"system\",\"orderNum\":1}]', 0, '124.93.236.18', '2019-10-21 14:03:21');
INSERT INTO `sys_log` VALUES (29, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":56,\"parentId\":0,\"name\":\"船员派遣管理\",\"type\":0,\"icon\":\"system\",\"orderNum\":2}]', 0, '124.93.236.18', '2019-10-21 14:04:02');
INSERT INTO `sys_log` VALUES (30, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":67,\"parentId\":0,\"name\":\"证书管理\",\"url\":\"home\",\"perms\":\"\",\"type\":0,\"icon\":\"editor\",\"orderNum\":3}]', 0, '124.93.236.18', '2019-10-21 14:04:15');
INSERT INTO `sys_log` VALUES (31, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":68,\"parentId\":0,\"name\":\"薪资管理\",\"url\":\"home\",\"perms\":\"\",\"type\":0,\"icon\":\"log\",\"orderNum\":3}]', 0, '124.93.236.18', '2019-10-21 14:04:25');
INSERT INTO `sys_log` VALUES (32, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":68,\"parentId\":0,\"name\":\"薪资管理\",\"url\":\"home\",\"perms\":\"\",\"type\":0,\"icon\":\"log\",\"orderNum\":4}]', 0, '124.93.236.18', '2019-10-21 14:04:31');
INSERT INTO `sys_log` VALUES (33, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":69,\"parentId\":0,\"name\":\"社保管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"geren\",\"orderNum\":5}]', 0, '124.93.236.18', '2019-10-21 14:04:42');
INSERT INTO `sys_log` VALUES (34, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":68,\"parentId\":0,\"name\":\"薪资管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"log\",\"orderNum\":4}]', 0, '124.93.236.18', '2019-10-21 14:04:51');
INSERT INTO `sys_log` VALUES (35, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":67,\"parentId\":0,\"name\":\"证书管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"editor\",\"orderNum\":3}]', 0, '124.93.236.18', '2019-10-21 14:04:58');
INSERT INTO `sys_log` VALUES (36, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":1,\"parentId\":0,\"name\":\"系统管理\",\"type\":0,\"icon\":\"system\",\"orderNum\":16}]', 0, '124.93.236.18', '2019-10-21 14:05:14');
INSERT INTO `sys_log` VALUES (37, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":3,\"username\":\"test1\",\"password\":\"350bcc55e18551c724d40445a0017f469eff44cf3a3361175492c793f12544d3\",\"salt\":\"iDApxUyxHQH3RMBssg2B\",\"email\":\"123@123.com\",\"mobile\":\"13010101010\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1}]', 0, '124.93.236.18', '2019-10-21 14:07:48');
INSERT INTO `sys_log` VALUES (38, 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":2,\"roleName\":\"系统管理员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[-666666],\"createTime\":\"Oct 21, 2019 5:47:53 PM\"}]', 8, '124.93.236.18', '2019-10-21 17:47:53');
INSERT INTO `sys_log` VALUES (39, 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"公司总管理员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[42,43,52,53,56,57,58,59,60,61,62,63,64,65,66,67,68,69,-666666,31,41,51]}]', 29, '124.93.236.18', '2019-10-21 17:48:10');
INSERT INTO `sys_log` VALUES (40, 'admin', '保存角色', 'io.renren.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":3,\"roleName\":\"职员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[-666666],\"createTime\":\"Oct 21, 2019 5:48:18 PM\"}]', 6, '124.93.236.18', '2019-10-21 17:48:18');
INSERT INTO `sys_log` VALUES (41, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"root@renren.io\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[3],\"createUserId\":1}]', 8, '124.93.236.18', '2019-10-21 17:49:56');
INSERT INTO `sys_log` VALUES (42, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":4,\"username\":\"5\",\"password\":\"2976369195a4f16ac922a1653a279cde5d74bba986fac90e4575463317ffca35\",\"salt\":\"i8n7plG2vNXSKeBkZFh6\",\"email\":\"1235@1235.com\",\"mobile\":\"13131313131\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1,\"createTime\":\"Oct 21, 2019 5:56:12 PM\"}]', 23, '124.93.236.18', '2019-10-21 17:56:13');
INSERT INTO `sys_log` VALUES (43, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"root@renren.io\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1}]', 786, '0:0:0:0:0:0:0:1', '2019-10-25 10:37:23');
INSERT INTO `sys_log` VALUES (44, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":3,\"username\":\"test1\",\"salt\":\"iDApxUyxHQH3RMBssg2B\",\"email\":\"123@123.com\",\"mobile\":\"13010101010\",\"status\":1,\"roleIdList\":[1,3],\"createUserId\":1}]', 873833, '0:0:0:0:0:0:0:1', '2019-10-25 11:09:58');
INSERT INTO `sys_log` VALUES (45, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":3,\"username\":\"test1\",\"salt\":\"iDApxUyxHQH3RMBssg2B\",\"email\":\"123@123.com\",\"mobile\":\"13010101010\",\"status\":1,\"roleIdList\":[3],\"createUserId\":1}]', 21589, '0:0:0:0:0:0:0:1', '2019-10-25 11:11:05');
INSERT INTO `sys_log` VALUES (46, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"root@renren.io\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[2,1],\"createUserId\":1}]', 586, '0:0:0:0:0:0:0:1', '2019-10-25 13:39:49');
INSERT INTO `sys_log` VALUES (47, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[1,2],\"createUserId\":1}]', 121, '0:0:0:0:0:0:0:1', '2019-10-25 13:40:45');
INSERT INTO `sys_log` VALUES (48, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[1,2,3],\"createUserId\":1}]', 197, '0:0:0:0:0:0:0:1', '2019-10-25 13:44:22');
INSERT INTO `sys_log` VALUES (49, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[1,2],\"createUserId\":1}]', 584999, '0:0:0:0:0:0:0:1', '2019-10-25 13:54:36');
INSERT INTO `sys_log` VALUES (50, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"password\":\"048c325c6cd107dc510665f6d20090b5aac5bc82301ba9b67cc72e8a084f20ca\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[1,2],\"createUserId\":1,\"affiliatedCompanyId\":2}]', 517, '0:0:0:0:0:0:0:1', '2019-11-02 12:27:52');
INSERT INTO `sys_log` VALUES (51, 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":2,\"roleName\":\"公司管理员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[1,2,15,16,17,18,3,19,20,21,22,4,23,24,25,26,5,6,7,8,9,10,11,12,13,14,27,29,30,-666666]}]', 90, '0:0:0:0:0:0:0:1', '2019-11-02 12:31:15');
INSERT INTO `sys_log` VALUES (52, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"password\":\"048c325c6cd107dc510665f6d20090b5aac5bc82301ba9b67cc72e8a084f20ca\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"affiliatedCompanyId\":2}]', 23, '0:0:0:0:0:0:0:1', '2019-11-02 12:38:11');
INSERT INTO `sys_log` VALUES (53, 'admin', '修改角色', 'io.renren.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":2,\"roleName\":\"公司管理员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[1,2,15,16,17,18,3,19,20,21,22,4,23,24,25,26,5,6,7,8,9,10,11,12,13,14,27,29,30,31,41,42,43,44,45,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,-666666]}]', 51, '0:0:0:0:0:0:0:1', '2019-11-02 12:38:49');
INSERT INTO `sys_log` VALUES (54, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":6,\"username\":\"test2\",\"password\":\"9aa756561b673ee44dd3e091df9251884330d0c65e9bb2c9b7882f88864a19e7\",\"salt\":\"E0WKW42mKDXwagVdPn4b\",\"email\":\"test1@qq.com\",\"mobile\":\"13478778787\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"createTime\":\"Nov 2, 2019 12:40:56 PM\",\"affiliatedCompanyId\":3}]', 9, '0:0:0:0:0:0:0:1', '2019-11-02 12:40:57');
INSERT INTO `sys_log` VALUES (55, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":7,\"username\":\"test3\",\"password\":\"b3c5aec9b78faa4d70439405ecc8a99f37c4850181e43fced894f8ad9cc7a5b6\",\"salt\":\"DgjibITYZGnVZSIwIiiY\",\"email\":\"test3@qq.com\",\"mobile\":\"12323212121\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"createTime\":\"Nov 2, 2019 1:46:49 PM\",\"affiliatedCompanyId\":2,\"company_name\":\"\"}]', 94, '0:0:0:0:0:0:0:1', '2019-11-02 13:46:49');
INSERT INTO `sys_log` VALUES (56, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":7,\"username\":\"test3\",\"password\":\"b3c5aec9b78faa4d70439405ecc8a99f37c4850181e43fced894f8ad9cc7a5b6\",\"salt\":\"DgjibITYZGnVZSIwIiiY\",\"email\":\"test3@qq.com\",\"mobile\":\"12323212121\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"affiliatedCompanyId\":3}]', 15, '0:0:0:0:0:0:0:1', '2019-11-02 13:54:51');
INSERT INTO `sys_log` VALUES (57, 'admin', '删除用户', 'io.renren.modules.sys.controller.SysUserController.delete()', '[[7]]', 15, '0:0:0:0:0:0:0:1', '2019-11-02 13:57:43');
INSERT INTO `sys_log` VALUES (58, 'admin', '删除用户', 'io.renren.modules.sys.controller.SysUserController.delete()', '[[6]]', 5, '0:0:0:0:0:0:0:1', '2019-11-02 13:57:47');
INSERT INTO `sys_log` VALUES (59, 'admin', '删除用户', 'io.renren.modules.sys.controller.SysUserController.delete()', '[[4]]', 5, '0:0:0:0:0:0:0:1', '2019-11-02 13:57:51');
INSERT INTO `sys_log` VALUES (60, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":8,\"username\":\"test2\",\"password\":\"449f54ac721ae906d758c798cb1f17e64c14aa2f5b2e5c3cb67bdd0239626b17\",\"salt\":\"CnYAmvQx6xC0pcqxYWO1\",\"email\":\"test2@qq.com\",\"mobile\":\"13423212342\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"createTime\":\"Nov 2, 2019 1:58:33 PM\",\"affiliatedCompanyId\":2,\"companyName\":\"\"}]', 140, '0:0:0:0:0:0:0:1', '2019-11-02 13:58:34');
INSERT INTO `sys_log` VALUES (61, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":9,\"username\":\"test4\",\"password\":\"e6188f623baa32fd6ff1cfaa9bac40b8eb0950a20c84193a1c94961f6acd0c3e\",\"salt\":\"M3hTBpWSCO0uPlU8BFX3\",\"email\":\"111@qq.com\",\"mobile\":\"12323121212\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"createTime\":\"Nov 2, 2019 2:02:49 PM\",\"affiliatedCompanyId\":1}]', 8, '0:0:0:0:0:0:0:1', '2019-11-02 14:02:50');
INSERT INTO `sys_log` VALUES (62, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":10,\"username\":\"test3\",\"password\":\"047f35a99df62cbbdc40a97ff54ccc523c5a5882bf0e64879d7d2a1d269cbdbe\",\"salt\":\"0f8GlEQsltCo0Fv9re0T\",\"email\":\"aaa@qq.com\",\"mobile\":\"12221111111\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"createTime\":\"Nov 2, 2019 2:06:14 PM\",\"affiliatedCompanyId\":3,\"companyName\":\"\"}]', 10, '0:0:0:0:0:0:0:1', '2019-11-02 14:06:15');
INSERT INTO `sys_log` VALUES (63, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":11,\"username\":\"test31\",\"password\":\"2828f0f04c7279b58e6e171bff92364edd8fc06f8a67212d4814a533ba967ed1\",\"salt\":\"0Jgn5JtAMxRMN6BroqHb\",\"email\":\"111@qq.com\",\"mobile\":\"12321112121\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"createTime\":\"Nov 2, 2019 2:07:19 PM\",\"affiliatedCompanyId\":3,\"companyName\":\"\"}]', 11, '0:0:0:0:0:0:0:1', '2019-11-02 14:07:20');
INSERT INTO `sys_log` VALUES (64, 'admin', '保存用户', 'io.renren.modules.sys.controller.SysUserController.save()', '[{\"userId\":12,\"username\":\"test5\",\"password\":\"cfb3e5f3701e6485e45018906c59ff9d5496d4a44456422cab2907bbcd45917a\",\"salt\":\"1I3K7p9IEPvyAYHvTkZV\",\"email\":\"111@qq.com\",\"mobile\":\"19889888888\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"createTime\":\"Nov 2, 2019 2:13:48 PM\",\"affiliatedCompanyId\":3,\"companyName\":\"3\"}]', 7, '0:0:0:0:0:0:0:1', '2019-11-02 14:13:49');
INSERT INTO `sys_log` VALUES (65, 'admin', '删除用户', 'io.renren.modules.sys.controller.SysUserController.delete()', '[[3,8,9,10,11,12]]', 9, '0:0:0:0:0:0:0:1', '2019-11-03 13:44:31');
INSERT INTO `sys_log` VALUES (66, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"password\":\"048c325c6cd107dc510665f6d20090b5aac5bc82301ba9b67cc72e8a084f20ca\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":0,\"roleIdList\":[2],\"createUserId\":1,\"affiliatedCompanyId\":2}]', 84, '0:0:0:0:0:0:0:1', '2019-11-03 15:56:29');
INSERT INTO `sys_log` VALUES (67, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"test\",\"salt\":\"mAJW1Qr4HXSxve37EQu4\",\"email\":\"test@test.com\",\"mobile\":\"15555555555\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1,\"affiliatedCompanyId\":2}]', 8, '0:0:0:0:0:0:0:1', '2019-11-03 15:56:35');
INSERT INTO `sys_log` VALUES (68, 'admin', '保存菜单', 'io.renren.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":74,\"parentId\":31,\"name\":\"工作履历表\",\"url\":\"generator/experienceinfo\",\"perms\":\"generator/experienceinfo\",\"type\":0,\"icon\":\"log\",\"orderNum\":7}]', 13, '0:0:0:0:0:0:0:1', '2019-12-04 21:58:10');
INSERT INTO `sys_log` VALUES (69, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":74,\"parentId\":31,\"name\":\"工作履历表\",\"url\":\"generator/experienceinfo\",\"perms\":\"generator/experienceinfo\",\"type\":1,\"icon\":\"menu\",\"orderNum\":7}]', 9, '0:0:0:0:0:0:0:1', '2019-12-04 22:00:16');
INSERT INTO `sys_log` VALUES (70, 'admin', '修改菜单', 'io.renren.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":74,\"parentId\":31,\"name\":\"工作履历表\",\"url\":\"generator/experienceinfo\",\"perms\":\"\",\"type\":1,\"icon\":\"menu\",\"orderNum\":7}]', 8, '0:0:0:0:0:0:0:1', '2019-12-04 22:01:27');
INSERT INTO `sys_log` VALUES (71, 'admin', '修改用户', 'io.renren.modules.sys.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"password\":\"9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"root@renren.io\",\"mobile\":\"13612345678\",\"status\":1,\"roleIdList\":[2,1],\"createUserId\":1,\"affiliatedCompanyId\":1}]', 103, '0:0:0:0:0:0:0:1', '2020-01-07 21:28:20');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 102 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'system', 16);
INSERT INTO `sys_menu` VALUES (2, 1, '管理员列表', 'sys/user', NULL, 1, 'admin', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'role', 2);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'menu', 3);
INSERT INTO `sys_menu` VALUES (5, 1, 'SQL监控', 'http://localhost:8080/renren-fast/druid/sql.html', NULL, 1, 'sql', 4);
INSERT INTO `sys_menu` VALUES (6, 1, '定时任务', 'job/schedule', NULL, 1, 'job', 5);
INSERT INTO `sys_menu` VALUES (7, 6, '查看', NULL, 'sys:schedule:list,sys:schedule:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (8, 6, '新增', NULL, 'sys:schedule:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (9, 6, '修改', NULL, 'sys:schedule:update', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (10, 6, '删除', NULL, 'sys:schedule:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (11, 6, '暂停', NULL, 'sys:schedule:pause', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (12, 6, '恢复', NULL, 'sys:schedule:resume', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (13, 6, '立即执行', NULL, 'sys:schedule:run', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (14, 6, '日志列表', NULL, 'sys:schedule:log', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 7);
INSERT INTO `sys_menu` VALUES (30, 1, '文件上传', 'oss/oss', 'sys:oss:all', 1, 'oss', 6);
INSERT INTO `sys_menu` VALUES (31, 0, '船员管理', NULL, NULL, 0, 'system', 1);
INSERT INTO `sys_menu` VALUES (41, 31, '船员基本信息表', 'generator/crewinfo', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (42, 41, '查看', NULL, 'generator:crewinfo:list,generator:crewinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (43, 41, '新增', NULL, 'generator:crewinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (44, 41, '修改', NULL, 'generator:crewinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (45, 41, '删除', NULL, 'generator:crewinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (51, 0, '所属公司信息表', 'generator/affiliatedcompanyinfo', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (52, 51, '查看', NULL, 'generator:affiliatedcompanyinfo:list,generator:affiliatedcompanyinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (53, 51, '新增', NULL, 'generator:affiliatedcompanyinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (54, 51, '修改', NULL, 'generator:affiliatedcompanyinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (55, 51, '删除', NULL, 'generator:affiliatedcompanyinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (56, 0, '船员派遣管理', NULL, NULL, 0, 'system', 2);
INSERT INTO `sys_menu` VALUES (57, 56, '派遣信息表', 'generator/dispatchinfo', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (58, 57, '查看', NULL, 'generator:dispatchinfo:list,generator:dispatchinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (59, 57, '新增', NULL, 'generator:dispatchinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (60, 57, '修改', NULL, 'generator:dispatchinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (61, 57, '删除', NULL, 'generator:dispatchinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (62, 56, '合同信息表', 'generator/contractinfo', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (63, 62, '查看', NULL, 'generator:contractinfo:list,generator:contractinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (64, 62, '新增', NULL, 'generator:contractinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (65, 62, '修改', NULL, 'generator:contractinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (66, 62, '删除', NULL, 'generator:contractinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (67, 0, '证书管理', '', '', 0, 'editor', 3);
INSERT INTO `sys_menu` VALUES (68, 0, '薪资管理', '', '', 0, 'log', 4);
INSERT INTO `sys_menu` VALUES (69, 0, '社保管理', '', '', 0, 'geren', 5);
INSERT INTO `sys_menu` VALUES (87, 67, '证书信息表', 'generator/certificatemanagement', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (88, 87, '查看', NULL, 'generator:certificatemanagement:list,generator:certificatemanagement:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (89, 87, '新增', NULL, 'generator:certificatemanagement:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (90, 87, '修改', NULL, 'generator:certificatemanagement:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (91, 87, '删除', NULL, 'generator:certificatemanagement:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (92, 69, '社保信息表', 'generator/insuranceinfo', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (93, 92, '查看', NULL, 'generator:insuranceinfo:list,generator:insuranceinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (94, 92, '新增', NULL, 'generator:insuranceinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (95, 92, '修改', NULL, 'generator:insuranceinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (96, 92, '删除', NULL, 'generator:insuranceinfo:delete', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (97, 68, '薪资信息表', 'generator/salaryinfo', NULL, 1, 'config', 6);
INSERT INTO `sys_menu` VALUES (98, 97, '查看', NULL, 'generator:salaryinfo:list,generator:salaryinfo:info', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (99, 97, '新增', NULL, 'generator:salaryinfo:save', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (100, 97, '修改', NULL, 'generator:salaryinfo:update', 2, NULL, 6);
INSERT INTO `sys_menu` VALUES (101, 97, '删除', NULL, 'generator:salaryinfo:delete', 2, NULL, 6);

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件上传' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '系统管理员', '', 1, '2019-10-20 21:54:49');
INSERT INTO `sys_role` VALUES (2, '公司管理员', '', 1, '2019-10-21 17:47:53');
INSERT INTO `sys_role` VALUES (3, '职员', '', 1, '2019-10-21 17:48:18');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 148 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (43, 1, 42);
INSERT INTO `sys_role_menu` VALUES (44, 1, 43);
INSERT INTO `sys_role_menu` VALUES (45, 1, 52);
INSERT INTO `sys_role_menu` VALUES (46, 1, 53);
INSERT INTO `sys_role_menu` VALUES (47, 1, 56);
INSERT INTO `sys_role_menu` VALUES (48, 1, 57);
INSERT INTO `sys_role_menu` VALUES (49, 1, 58);
INSERT INTO `sys_role_menu` VALUES (50, 1, 59);
INSERT INTO `sys_role_menu` VALUES (51, 1, 60);
INSERT INTO `sys_role_menu` VALUES (52, 1, 61);
INSERT INTO `sys_role_menu` VALUES (53, 1, 62);
INSERT INTO `sys_role_menu` VALUES (54, 1, 63);
INSERT INTO `sys_role_menu` VALUES (55, 1, 64);
INSERT INTO `sys_role_menu` VALUES (56, 1, 65);
INSERT INTO `sys_role_menu` VALUES (57, 1, 66);
INSERT INTO `sys_role_menu` VALUES (58, 1, 67);
INSERT INTO `sys_role_menu` VALUES (59, 1, 68);
INSERT INTO `sys_role_menu` VALUES (60, 1, 69);
INSERT INTO `sys_role_menu` VALUES (61, 1, -666666);
INSERT INTO `sys_role_menu` VALUES (62, 1, 31);
INSERT INTO `sys_role_menu` VALUES (63, 1, 41);
INSERT INTO `sys_role_menu` VALUES (64, 1, 51);
INSERT INTO `sys_role_menu` VALUES (65, 3, -666666);
INSERT INTO `sys_role_menu` VALUES (96, 2, 1);
INSERT INTO `sys_role_menu` VALUES (97, 2, 2);
INSERT INTO `sys_role_menu` VALUES (98, 2, 15);
INSERT INTO `sys_role_menu` VALUES (99, 2, 16);
INSERT INTO `sys_role_menu` VALUES (100, 2, 17);
INSERT INTO `sys_role_menu` VALUES (101, 2, 18);
INSERT INTO `sys_role_menu` VALUES (102, 2, 3);
INSERT INTO `sys_role_menu` VALUES (103, 2, 19);
INSERT INTO `sys_role_menu` VALUES (104, 2, 20);
INSERT INTO `sys_role_menu` VALUES (105, 2, 21);
INSERT INTO `sys_role_menu` VALUES (106, 2, 22);
INSERT INTO `sys_role_menu` VALUES (107, 2, 4);
INSERT INTO `sys_role_menu` VALUES (108, 2, 23);
INSERT INTO `sys_role_menu` VALUES (109, 2, 24);
INSERT INTO `sys_role_menu` VALUES (110, 2, 25);
INSERT INTO `sys_role_menu` VALUES (111, 2, 26);
INSERT INTO `sys_role_menu` VALUES (112, 2, 5);
INSERT INTO `sys_role_menu` VALUES (113, 2, 6);
INSERT INTO `sys_role_menu` VALUES (114, 2, 7);
INSERT INTO `sys_role_menu` VALUES (115, 2, 8);
INSERT INTO `sys_role_menu` VALUES (116, 2, 9);
INSERT INTO `sys_role_menu` VALUES (117, 2, 10);
INSERT INTO `sys_role_menu` VALUES (118, 2, 11);
INSERT INTO `sys_role_menu` VALUES (119, 2, 12);
INSERT INTO `sys_role_menu` VALUES (120, 2, 13);
INSERT INTO `sys_role_menu` VALUES (121, 2, 14);
INSERT INTO `sys_role_menu` VALUES (122, 2, 27);
INSERT INTO `sys_role_menu` VALUES (123, 2, 29);
INSERT INTO `sys_role_menu` VALUES (124, 2, 30);
INSERT INTO `sys_role_menu` VALUES (125, 2, 31);
INSERT INTO `sys_role_menu` VALUES (126, 2, 41);
INSERT INTO `sys_role_menu` VALUES (127, 2, 42);
INSERT INTO `sys_role_menu` VALUES (128, 2, 43);
INSERT INTO `sys_role_menu` VALUES (129, 2, 44);
INSERT INTO `sys_role_menu` VALUES (130, 2, 45);
INSERT INTO `sys_role_menu` VALUES (131, 2, 51);
INSERT INTO `sys_role_menu` VALUES (132, 2, 52);
INSERT INTO `sys_role_menu` VALUES (133, 2, 53);
INSERT INTO `sys_role_menu` VALUES (134, 2, 54);
INSERT INTO `sys_role_menu` VALUES (135, 2, 55);
INSERT INTO `sys_role_menu` VALUES (136, 2, 56);
INSERT INTO `sys_role_menu` VALUES (137, 2, 57);
INSERT INTO `sys_role_menu` VALUES (138, 2, 58);
INSERT INTO `sys_role_menu` VALUES (139, 2, 59);
INSERT INTO `sys_role_menu` VALUES (140, 2, 60);
INSERT INTO `sys_role_menu` VALUES (141, 2, 61);
INSERT INTO `sys_role_menu` VALUES (142, 2, 62);
INSERT INTO `sys_role_menu` VALUES (143, 2, 63);
INSERT INTO `sys_role_menu` VALUES (144, 2, 64);
INSERT INTO `sys_role_menu` VALUES (145, 2, 65);
INSERT INTO `sys_role_menu` VALUES (146, 2, 66);
INSERT INTO `sys_role_menu` VALUES (147, 2, -666666);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `affiliated_company_id` int(20) NULL DEFAULT NULL COMMENT '所属公司',
  `company_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司名称',
  `management_level` int(20) UNSIGNED NULL DEFAULT NULL COMMENT '管理级别',
  `create_user_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建IP',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', 'YzcmCZNvbXocrsz9dm8e', 'root@renren.io', '13612345678', 1, 1, '2016-11-11 11:11:11', NULL, 1, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (2, 'test', '048c325c6cd107dc510665f6d20090b5aac5bc82301ba9b67cc72e8a084f20ca', 'mAJW1Qr4HXSxve37EQu4', 'test@test.com', '15555555555', 1, 1, '2019-10-20 11:01:44', NULL, 2, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (6, 4, 1);
INSERT INTO `sys_user_role` VALUES (10, 3, 3);
INSERT INTO `sys_user_role` VALUES (23, 6, 2);
INSERT INTO `sys_user_role` VALUES (25, 7, 2);
INSERT INTO `sys_user_role` VALUES (26, 8, 2);
INSERT INTO `sys_user_role` VALUES (27, 9, 2);
INSERT INTO `sys_user_role` VALUES (28, 10, 2);
INSERT INTO `sys_user_role` VALUES (29, 11, 2);
INSERT INTO `sys_user_role` VALUES (30, 12, 2);
INSERT INTO `sys_user_role` VALUES (32, 2, 2);
INSERT INTO `sys_user_role` VALUES (33, 1, 2);
INSERT INTO `sys_user_role` VALUES (34, 1, 1);

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'token',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户Token' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (1, 'd2eeee1b6df86cd72d12dbfe819c91e4', '2020-01-12 22:57:05', '2020-01-12 10:57:05');
INSERT INTO `sys_user_token` VALUES (2, '9e402939a0fb48af4f0faa9a3c9558ed', '2019-12-08 21:10:47', '2019-12-08 09:10:47');
INSERT INTO `sys_user_token` VALUES (3, 'cedbe49c6a16d0cb82efc4f478cd2c89', '2019-10-22 03:41:15', '2019-10-21 15:41:15');
INSERT INTO `sys_user_token` VALUES (6, '21bdec3c6773ad3398c4031fb99ece7d', '2019-11-03 00:41:12', '2019-11-02 12:41:12');

-- ----------------------------
-- Table structure for t_student
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '学生' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tb_goods
-- ----------------------------
DROP TABLE IF EXISTS `tb_goods`;
CREATE TABLE `tb_goods`  (
  `goods_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名',
  `intro` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '介绍',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `num` int(11) NULL DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品管理' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_goods
-- ----------------------------
INSERT INTO `tb_goods` VALUES (1, '戴尔', '游匣', 123.00, 1123);
INSERT INTO `tb_goods` VALUES (2, '惠普', '暗夜精灵', 123.00, 123);
INSERT INTO `tb_goods` VALUES (3, '华硕', '飞行堡垒', 123.00, 1);
INSERT INTO `tb_goods` VALUES (4, '联想', '拯救者', 123.00, 123);
INSERT INTO `tb_goods` VALUES (7, '苹果', 'MACBOOK', 123.00, 1234);
INSERT INTO `tb_goods` VALUES (9, '商品', '笔记本', 123.00, 123);
INSERT INTO `tb_goods` VALUES (10, '商品', '笔记本', 123.00, 123);
INSERT INTO `tb_goods` VALUES (11, '商品', '笔记本', 312.00, 312);
INSERT INTO `tb_goods` VALUES (12, '商品', '笔记本', 123.00, 123);

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'mark', '13612345678', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '2017-03-23 22:37:41');

SET FOREIGN_KEY_CHECKS = 1;
